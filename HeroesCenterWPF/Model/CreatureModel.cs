using CreatureData = HeroesCenter.Data.Stats.CreatureData;

namespace HeroesCenter.Model;

public class CreatureModel : AbstractIndexedModel
{
    protected override MemoryWorkerType MemoryWorkerType => MemoryWorkerType.Creatures;

    private readonly MemoryVariable _town = CreatureData.Town;
    private readonly MemoryVariable _level = CreatureData.Level;
    private readonly MemoryVariable _flags = CreatureData.Flags;
    private readonly MemoryVariable _singleName = CreatureData.SingleName;
    private readonly MemoryVariable _pluralName = CreatureData.PluralName;
    private readonly MemoryVariable _wood = CreatureData.Wood;
    private readonly MemoryVariable _mercury = CreatureData.Mercury;
    private readonly MemoryVariable _ore = CreatureData.Ore;
    private readonly MemoryVariable _sulfur = CreatureData.Sulfur;
    private readonly MemoryVariable _crystal = CreatureData.Crystal;
    private readonly MemoryVariable _gems = CreatureData.Gems;
    private readonly MemoryVariable _gold = CreatureData.Gold;
    private readonly MemoryVariable _fightValue = CreatureData.FightValue;
    private readonly MemoryVariable _aiValue = CreatureData.AiValue;
    private readonly MemoryVariable _growth = CreatureData.Growth;
    private readonly MemoryVariable _health = CreatureData.Health;
    private readonly MemoryVariable _speed = CreatureData.Speed;
    private readonly MemoryVariable _attack = CreatureData.Attack;
    private readonly MemoryVariable _defence = CreatureData.Defence;
    private readonly MemoryVariable _minDamage = CreatureData.MinDamage;
    private readonly MemoryVariable _maxDamage = CreatureData.MaxDamage;
    private readonly MemoryVariable _shots = CreatureData.Shots;
    private readonly MemoryVariable _spells = CreatureData.Spells;

    public CreatureModel(GameVersion version, int index) : base(version, index)
    {
    }

    public int Town
    {
        get => BitConverter.ToInt32(GetData(_town, Index));
        set => SetData(value, _town, Index);
    }

    public int Level
    {
        get => BitConverter.ToInt32(GetData(_level, Index));
        set => SetData(value, _level, Index);
    }

    public uint Flags
    {
        get => BitConverter.ToUInt32(GetData(_flags, Index));
        set => SetData(value, _flags, Index);
    }

    public string SingleName
    {
        get => GetData(_singleName, Index).ByteArrayToString();
        set => SetData(value, _singleName, Index);
    }

    public string PluralName
    {
        get => GetData(_pluralName, Index).ByteArrayToString();
        set => SetData(value, _pluralName, Index);
    }

    public int Wood
    {
        get => BitConverter.ToInt32(GetData(_wood, Index));
        set => SetData(value, _wood, Index);
    }

    public int Mercury
    {
        get => BitConverter.ToInt32(GetData(_mercury, Index));
        set => SetData(value, _mercury, Index);
    }

    public int Ore
    {
        get => BitConverter.ToInt32(GetData(_ore, Index));
        set => SetData(value, _ore, Index);
    }

    public int Sulfur
    {
        get => BitConverter.ToInt32(GetData(_sulfur, Index));
        set => SetData(value, _sulfur, Index);
    }

    public int Crystal
    {
        get => BitConverter.ToInt32(GetData(_crystal, Index));
        set => SetData(value, _crystal, Index);
    }

    public int Gems
    {
        get => BitConverter.ToInt32(GetData(_gems, Index));
        set => SetData(value, _gems, Index);
    }

    public int Gold
    {
        get => BitConverter.ToInt32(GetData(_gold, Index));
        set => SetData(value, _gold, Index);
    }

    public int FightValue
    {
        get => BitConverter.ToInt32(GetData(_fightValue, Index));
        set => SetData(value, _fightValue, Index);
    }

    public int AiValue
    {
        get => BitConverter.ToInt32(GetData(_aiValue, Index));
        set => SetData(value, _aiValue, Index);
    }

    public int Growth
    {
        get => BitConverter.ToInt32(GetData(_growth, Index));
        set => SetData(value, _growth, Index);
    }

    public int Health
    {
        get => BitConverter.ToInt32(GetData(_health, Index));
        set => SetData(value, _health, Index);
    }

    public int Speed
    {
        get => BitConverter.ToInt32(GetData(_speed, Index));
        set => SetData(value, _speed, Index);
    }

    public int Attack
    {
        get => BitConverter.ToInt32(GetData(_attack, Index));
        set => SetData(value, _attack, Index);
    }

    public int Defence
    {
        get => BitConverter.ToInt32(GetData(_defence, Index));
        set => SetData(value, _defence, Index);
    }

    public int MinDamage
    {
        get => BitConverter.ToInt32(GetData(_minDamage, Index));
        set => SetData(value, _minDamage, Index);
    }

    public int MaxDamage
    {
        get => BitConverter.ToInt32(GetData(_maxDamage, Index));
        set => SetData(value, _maxDamage, Index);
    }

    public int Shots
    {
        get => BitConverter.ToInt32(GetData(_shots, Index));
        set => SetData(value, _shots, Index);
    }

    public int Spells
    {
        get => BitConverter.ToInt32(GetData(_spells, Index));
        set => SetData(value, _spells, Index);
    }
}