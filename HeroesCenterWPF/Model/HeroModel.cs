namespace HeroesCenter.Model;
/*******************Hero Data*******************/

[UsedImplicitly]
public class HeroModel : AbstractIndexedModel
{
    public HeroModel(GameVersion version, int heroIndex) : base(version, heroIndex)
    {
    }

    /*public Byte[] Spells
    {
        get { return _heroGetData(HeroStats.Spells); }
        set { _heroSetData(value, HeroStats.Spells.Offset, Index); }
    }*/

    protected override MemoryWorkerType MemoryWorkerType => MemoryWorkerType.Hero;

    public string Name
    {
        get => GetData(HeroStats.Name, Index).ByteArrayToString();
        set => SetData(value, HeroStats.Name, Index);
    }

    public byte Portrait
    {
        get => GetData(HeroStats.PortraitId, Index)[0];
        set => SetData(value, HeroStats.PortraitId.Offset, Index);
    }

    public int Class
    {
        get => GetData(HeroStats.Class, Index)[0];
        set => SetData((byte) value, HeroStats.Class.Offset, Index);
    }

    public short X
    {
        get => BitConverter.ToInt16(GetData(HeroStats.X, Index), 0);
        set => SetData(value, HeroStats.X.Offset, Index);
    }

    public short Y
    {
        get => BitConverter.ToInt16(GetData(HeroStats.Y, Index), 0);
        set => SetData(value, HeroStats.Y.Offset, Index);
    }

    public short Z
    {
        get => BitConverter.ToInt16(GetData(HeroStats.Z, Index), 0);
        set => SetData(value, HeroStats.Z, Index);
    }

    public bool Unknown
    {
        get => GetData(HeroStats.Unknown_IsAvailable, Index)[0] == 1;
        set => SetData(value, HeroStats.Unknown_IsAvailable, Index);
    }

    public byte TeleportUseCount
    {
        get => GetData(HeroStats.TeleportUseCount, Index)[0];
        set => SetData(value, HeroStats.TeleportUseCount.Offset, Index);
    }

    public byte MoraleTotal
    {
        get => GetData(HeroStats.MoraleTotal, Index)[0];
        set => SetData(value, HeroStats.MoraleTotal.Offset, Index);
    }

    public byte LuckTotal
    {
        get => GetData(HeroStats.LuckTotal, Index)[0];
        set => SetData(value, HeroStats.LuckTotal.Offset, Index);
    }

    public byte[] SpellsLearned
    {
        get => GetData(HeroStats.SpellsLearned, Index);
        set => SetData(value, HeroStats.SpellsLearned.Offset, Index);
    }

    public byte[] SpellsInBook
    {
        get => GetData(HeroStats.SpellsInBook, Index);
        set => SetData(value, HeroStats.SpellsInBook.Offset, Index);
    }

    public override string ToString()
    {
        return Name;
    }
}