namespace HeroesCenter.Model;

/*[UsedImplicitly]
public class TownModel : AbstractIndexedModel
{
    public TownModel(Int32 index) : base(index)
    {
    }

    protected override MemoryWorkerType MemoryWorkerType => MemoryWorkerType.Town;

    public Byte TownIndex
    {
        get => MemoryWorker.GetData(TownStats.Index, Index)[0];
        set => MemoryWorker.SetData(value, TownStats.Index.Offset, Index);
    }

    public Byte Owner
    {
        get => MemoryWorker.GetData(TownStats.Owner, Index)[0];
        set => MemoryWorker.SetData(value, TownStats.Owner.Offset, Index);
    }

    public Boolean CanBuild
    {
        get => MemoryWorker.GetData(TownStats.CanBuild, Index)[0] == 0;
        set => MemoryWorker.SetData(!value, TownStats.CanBuild.Offset, Index);
    }

    public Byte Type
    {
        get => MemoryWorker.GetData(TownStats.TownType, Index)[0];
        set => MemoryWorker.SetData(value, TownStats.TownType.Offset, Index);
    }
}*/