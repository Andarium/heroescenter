namespace HeroesCenter.Model;

public class SecondarySkillModel : AbstractIndexedModel
{
    public readonly int SkillIndex;
    private readonly MemoryVariable _skill;
    private readonly MemoryVariable _registry;

    public SecondarySkillModel(GameVersion version, int heroIndex, int skillIndex) : base(version, heroIndex)
    {
        SkillIndex = skillIndex;

        _skill = HeroStats.SecondarySkills.CopyPointer(skillIndex, 1);

        var registry = version == GameVersion.HornOfTheAbyss
            ? HeroStats.SecondarySkillsRegistryPointer
            : HeroStats.SecondarySkillsRegistry;

        _registry = registry.CopyPointer(skillIndex, 1);
    }

    /// <summary>
    ///     Возвращает наименование стадии развития навыка
    /// </summary>
    public string? StageName => HeroHelper.Instance.Skills.Stages.ElementAtOrDefault(Stage);

    /// <summary>
    ///     Возвращает наименование навыка
    /// </summary>
    public string? SkillName => HeroHelper.Instance.Skills.Secondary.ElementAtOrDefault(SkillIndex);

    protected override MemoryWorkerType MemoryWorkerType => MemoryWorkerType.Hero;

    /// <summary>
    ///     Возвращает/задает стадию развития навыка, допустимые значения от 0 до 3
    /// </summary>
    public byte Stage
    {
        get => GetData(_skill, Index)[0];
        set
        {
            if (value == 255)
            {
                value = 0;
            }

            var oldValue = Stage;
            if (value == oldValue)
            {
                return;
            }

            int temp = value > 3
                ? 3
                : value;

            //Изучаем навык
            SetData((byte) temp, _skill, Index);

            if (oldValue == 0 && value != 0)
            {
                // Set to max, will be reordered in SkillsModel
                StageRegistry = byte.MaxValue;
            }

            if (oldValue != 0 && value == 0)
            {
                StageRegistry = 0;
            }

            OnPropertyChanged(); //Переписываем весь блок со скиллами из-за "защиты"
        }
    }

    /// <summary>
    ///     Возвращает/задает состояние навыка в "реестре"
    /// </summary>
    public byte StageRegistry
    {
        get => GetData(_registry, Index)[0];
        set
        {
            //Заносим навык в "реестр" отображаемых
            if (value != StageRegistry)
            {
                SetData(value, _registry, Index);
            }
        }
    }
}