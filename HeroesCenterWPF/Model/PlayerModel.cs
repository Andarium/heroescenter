namespace HeroesCenter.Model;

public class PlayerModel : AbstractIndexedModel
{
    public PlayerModel(GameVersion version, int playerIndex) : base(version, playerIndex)
    {
    }

    protected override MemoryWorkerType MemoryWorkerType => MemoryWorkerType.Player;

    public byte ActiveHeroIndex => GetData(PlayerData.SelectedHero, Index)[0];
    public byte ActiveTownIndex => GetData(PlayerData.SelectedTown, Index)[0];

    public int GetHeroOnMap(int slotIndex) => BitConverter.ToInt32(GetData(4, PlayerData.HeroesOnMap.Offset + slotIndex * 4, Index));
    public void SetHeroOnMap(int slotIndex, int heroIndex) => SetData(BitConverter.GetBytes(heroIndex), PlayerData.HeroesOnMap.Offset + slotIndex * 4, Index);
}