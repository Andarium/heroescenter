namespace HeroesCenter.Model;

public class UnitModel : AbstractOffsetIndexedModel
{
    private const int Size = 4;

    public UnitModel(GameVersion version, int parentIndex, int index) : base(version, parentIndex, index)
    {
        BlockOffset = HeroStats.Army.Offset;
    }

    protected override int BlockSize => 8;

    public int Type
    {
        get => BitConverter.ToInt32(GetData(Size, BlockOffset + Index * 4, ParentIndex), 0);
        set => SetData(value, BlockOffset + Index * 4, ParentIndex);
    }

    public int Count
    {
        get => BitConverter.ToInt32(GetData(Size, BlockOffset + (Index + 7) * 4, ParentIndex), 0);
        set => SetData(value, BlockOffset + (Index + 7) * 4, ParentIndex);
    }

    protected override MemoryWorkerType MemoryWorkerType => MemoryWorkerType.Hero;
}

/*public class GarrisonUnitModel : UnitModel
{
    public GarrisonUnitModel(Int32 parentIndex, Int32 index) : base(parentIndex, index)
    {
        Offset = TownStats.GarrisonArmy.Offset;
    }

    protected override MemoryWorkerType MemoryWorkerType => MemoryWorkerType.Town;
}*/