namespace HeroesCenter.Model;

public sealed class SpellModel : AbstractOffsetIndexedModel
{
    private readonly int _learnedOffset;
    private readonly int _inBookOffset;

    public SpellModel(GameVersion version, int parentIndex, int index) : base(version, parentIndex, index)
    {
        BlockOffset = HeroStats.SpellsLearned.Offset;
        _learnedOffset = HeroStats.SpellsLearned.Offset + index;
        _inBookOffset = HeroStats.SpellsInBook.Offset + index;
    }

    protected override int BlockSize => 1;

    public byte Learned
    {
        get => GetData(BlockSize, _learnedOffset, ParentIndex)[0];
        set => SetData(value, _learnedOffset, ParentIndex);
    }

    public byte InBook
    {
        get => GetData(BlockSize, _inBookOffset, ParentIndex)[0];
        set => SetData(value, _inBookOffset, ParentIndex);
    }

    protected override MemoryWorkerType MemoryWorkerType => MemoryWorkerType.Hero;
}