namespace HeroesCenter.Model;

public sealed class ArtifactModel : AbstractOffsetIndexedModel
{
    private const int Size = 4;

    public ArtifactModel(GameVersion version, int heroIndex, int slotIndex) : base(version, heroIndex, slotIndex)
    {
        BlockOffset = slotIndex > 18
            ? HeroStats.ArtifactsInBackpack.Offset + (slotIndex - 19) * BlockSize
            : HeroStats.ArtifactsOnHero.Offset + slotIndex * BlockSize;
    }

    protected override int BlockSize => 8;

    public bool IsEmpty => Type == -1;

    protected override MemoryWorkerType MemoryWorkerType => MemoryWorkerType.Hero;

    public int Type
    {
        get => BitConverter.ToInt32(GetData(Size, BlockOffset, ParentIndex), 0);
        set => SetData(value, BlockOffset, ParentIndex);
    }

    public int Data
    {
        get => BitConverter.ToInt32(GetData(Size, BlockOffset + 4, ParentIndex), 0);
        set => SetData(value, BlockOffset + 4, ParentIndex);
    }
}