namespace HeroesCenter.Model;

public abstract class AbstractIndexedModel : AbstractModel, IIndexedModel
{
    protected AbstractIndexedModel(GameVersion version, int index) : base(version)
    {
        Index = index;
    }

    public int Index { get; }
    public override string StartAddress => GetBlockStartAddress(Index);
    public override string EndAddress => GetBlockEndAddress(Index);
    protected int BlockOffset { get; init; }

    public override byte[] DataBlock
    {
        get => GetData(BlockSize, BlockOffset, Index);
        set => SetData(value, BlockOffset, Index);
    }

    protected virtual byte[] GetData(MemoryVariable variable) => GetData(variable.Size, variable.Offset + BlockOffset, Index);
    protected virtual void SetData<T>(T input, MemoryVariable variable) where T : struct => SetData(input, variable.Offset + BlockOffset, Index);
}

public abstract class AbstractOffsetIndexedModel : AbstractIndexedModel
{
    protected AbstractOffsetIndexedModel(GameVersion version, int parentIndex, int index) : base(version, index)
    {
        ParentIndex = parentIndex;
    }

    protected int ParentIndex { get; }
    public override string StartAddress => GetBlockStartAddress(ParentIndex, BlockOffset + BlockSize * Index);
    public override string EndAddress => GetBlockEndAddress(ParentIndex, BlockOffset + BlockSize * Index);

    public override byte[] DataBlock
    {
        get => GetData(BlockSize, Index * BlockSize + BlockOffset, ParentIndex);
        set => SetData(value, Index * BlockSize + BlockOffset, ParentIndex);
    }
}