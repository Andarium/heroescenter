namespace HeroesCenter.Model;

public class MapModel : AbstractModel
{
    protected override MemoryWorkerType MemoryWorkerType => MemoryWorkerType.Map;

    public MapModel(GameVersion version) : base(version)
    {
    }

    public short CurrentDay
    {
        get => BitConverter.ToInt16(GetData(MapData.CurrentDay));
        set => SetData(value, MapData.CurrentDay);
    }

    public short CurrentWeek
    {
        get => BitConverter.ToInt16(GetData(MapData.CurrentWeek));
        set => SetData(value, MapData.CurrentWeek);
    }

    public short CurrentMonth
    {
        get => BitConverter.ToInt16(GetData(MapData.CurrentMonth));
        set => SetData(value, MapData.CurrentMonth);
    }

    public byte LevelCap
    {
        get => GetData(MapData.LevelCap)[0];
        set => SetData(value, MapData.LevelCap.Offset);
    }
}