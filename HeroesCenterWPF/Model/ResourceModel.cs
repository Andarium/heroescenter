namespace HeroesCenter.Model;

public class ResourceModel : AbstractIndexedModel
{
    public ResourceModel(GameVersion version, int playerIndex) : base(version, playerIndex)
    {
        BlockOffset = PlayerData.Wood.Offset;
    }

    protected override MemoryWorkerType MemoryWorkerType => MemoryWorkerType.Player;

    protected override int BlockSize => PlayerData.Wood.Size * 7;

    public int Wood
    {
        get => GetData(PlayerData.Wood, Index).ToInt32();
        set => SetData(value, PlayerData.Wood, Index);
    }

    public int Mercury
    {
        get => GetData(PlayerData.Mercury, Index).ToInt32();
        set => SetData(value, PlayerData.Mercury, Index);
    }

    public int Ore
    {
        get => GetData(PlayerData.Ore, Index).ToInt32();
        set => SetData(value, PlayerData.Ore, Index);
    }

    public int Sulfur
    {
        get => GetData(PlayerData.Sulfur, Index).ToInt32();
        set => SetData(value, PlayerData.Sulfur, Index);
    }

    public int Crystals
    {
        get => GetData(PlayerData.Crystals, Index).ToInt32();
        set => SetData(value, PlayerData.Crystals, Index);
    }

    public int Gems
    {
        get => GetData(PlayerData.Gems, Index).ToInt32();
        set => SetData(value, PlayerData.Gems, Index);
    }

    public int Gold
    {
        get => GetData(PlayerData.Gold, Index).ToInt32();
        set => SetData(value, PlayerData.Gold, Index);
    }
}