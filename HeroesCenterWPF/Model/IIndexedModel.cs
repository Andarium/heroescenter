namespace HeroesCenter.Model;

public interface IIndexedModel : IModel
{
    int Index { get; }
}