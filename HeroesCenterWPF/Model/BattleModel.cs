namespace HeroesCenter.Model;

public class BattleModel : AbstractModel
{
    public byte IsMagicGarrison
    {
        get => GetData(BattleData.IsMagicGarrison)[0];
        set => SetData(value, BattleData.IsMagicGarrison);
    }

    public byte IsMagicUsedHeroLeft
    {
        get => GetData(BattleData.IsMagicUsedHeroLeft)[0];
        set => SetData(value, BattleData.IsMagicUsedHeroLeft);
    }
    public byte IsMagicUsedHeroRight
    {
        get => GetData(BattleData.IsMagicUsedHeroRight)[0];
        set => SetData(value, BattleData.IsMagicUsedHeroRight);
    }

    protected override MemoryWorkerType MemoryWorkerType => MemoryWorkerType.Battle;

    public BattleModel(GameVersion version) : base(version)
    {
    }
}