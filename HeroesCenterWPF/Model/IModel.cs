namespace HeroesCenter.Model;

public interface IModel
{
    string StartAddress { get; }
    string EndAddress { get; }
    public string AddressRange => $"{StartAddress} - {EndAddress}";

    GameVersion Version { get; }

    byte[] DataBlock { get; set; }
}