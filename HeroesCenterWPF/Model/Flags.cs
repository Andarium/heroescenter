namespace HeroesCenter.Model;

public enum Flags : byte
{
    [Order(0)]  All = 200,
    [Order(1)]  FlagsOnly = 100,
    [Order(2)]  Red = 0,
    [Order(3)]  Blue = 1,
    [Order(4)]  Brown = 2,
    [Order(5)]  Green = 3,
    [Order(6)]  Orange = 4,
    [Order(7)]  Purple = 5,
    [Order(8)]  Teal = 6,
    [Order(9)]  Pink = 7,
    [Order(10)] None = 255
}