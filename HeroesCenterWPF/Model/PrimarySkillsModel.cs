namespace HeroesCenter.Model;

public class PrimarySkillsModel : AbstractIndexedModel
{
    public PrimarySkillsModel(GameVersion version, int heroIndex) : base(version, heroIndex)
    {
    }

    protected override MemoryWorkerType MemoryWorkerType => MemoryWorkerType.Hero;

    private T Getter<T>(MemoryVariable variable) where T : struct
    {
        return variable.Size switch
        {
            1 => Converter.ConvertValue<T, byte>(GetData(variable, Index)[0]),
            2 => Converter.ConvertValue<T, short>(BitConverter.ToInt16(GetData(variable, Index), 0)),
            4 => Converter.ConvertValue<T, int>(BitConverter.ToInt32(GetData(variable, Index), 0)),
            8 => Converter.ConvertValue<T, long>(BitConverter.ToInt64(GetData(variable, Index), 0)),
            _ => default
        };
    }

    private void Setter<T>(MemoryVariable variable, T value) where T : struct, IConvertible, IComparable
    {
        SetData(value, variable.Offset, Index);
    }

    private void SetterMax<T>(MemoryVariable variable, T value, T max) where T : struct, IConvertible, IComparable
    {
        Setter(variable, value.CompareTo(max) > 0 ? max : value);
    }

    #region Int32 values

    public int Experience
    {
        get => Getter<int>(HeroStats.Experience);
        set => SetterMax(HeroStats.Experience, value, 2_147_293_156);
        // 2_147_293_156 = 6424 lvl (81 if not bugged)
    }

    public int DailyMovement => Getter<int>(HeroStats.DailyMovement);

    public int MovementPoints
    {
        get => Getter<int>(HeroStats.Movement);
        set => Setter(HeroStats.Movement, value);
    }

    #endregion

    #region Int16 values

    public short ManaPoints
    {
        get => Getter<short>(HeroStats.ManaPoints);
        set => Setter(HeroStats.ManaPoints, value);
    }

    #endregion

    #region Byte values

    public byte Attack
    {
        get => Getter<byte>(HeroStats.Attack);
        set => Setter(HeroStats.Attack, value);
    }

    public byte Defence
    {
        get => Getter<byte>(HeroStats.Defence);
        set => Setter(HeroStats.Defence, value);
    }

    public byte SpellPower
    {
        get => Getter<byte>(HeroStats.SpellPower);
        set => Setter(HeroStats.SpellPower, value);
    }

    public byte Knowledge
    {
        get => Getter<byte>(HeroStats.Knowledge);
        set => Setter(HeroStats.Knowledge, value);
    }

    public byte Flag
    {
        get => Getter<byte>(HeroStats.Flag);
        set => Setter(HeroStats.Flag, value);
    }

    public short Level
    {
        get => Getter<short>(HeroStats.Level);
        set => SetterMax(HeroStats.Level, value, (short) 6424);
    }

    public byte HeroIndex
    {
        get => Getter<byte>(HeroStats.Specialization);
        set => Setter(HeroStats.Specialization, value);
        //Сеттер закрыт до лучших времен. Специализация напрямую связана с индексом героя в игре. Изменение приводит к критическим ошибкам.

        // set { _heroSetData(value > 155 ? (Byte) 155 : value, HeroStats.HeroIndex.Offset, _heroIndex); }
    }

    #endregion
}