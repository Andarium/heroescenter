namespace HeroesCenter.Model;

public class GameModel : AbstractModel
{
    public byte ActivePlayer => GetData(GameData.ActivePlayer)[0];

    protected override MemoryWorkerType MemoryWorkerType => MemoryWorkerType.Game;

    public GameModel(GameVersion version) : base(version)
    {
    }
}