namespace HeroesCenter.Model;

public class UIModel : AbstractModel
{
    protected override MemoryWorkerType MemoryWorkerType => MemoryWorkerType.UI;

    public UIModel(GameVersion version) : base(version)
    {
    }

    public byte ViewX => GetData(UIData.ViewX)[0];
    public byte ViewY => GetData(UIData.ViewY)[0];
    public byte ViewZ => GetData(UIData.ViewZ)[0];

    public byte CursorCellX => GetData(UIData.CursorCellX)[0];
    public byte CursorCellY => GetData(UIData.CursorCellY)[0];
    public byte CursorCellZ => (byte) (GetData(UIData.CursorCellZ)[0].HasBit(2) ? 1 : 0);
}