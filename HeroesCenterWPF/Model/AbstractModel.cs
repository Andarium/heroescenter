namespace HeroesCenter.Model;

public abstract class AbstractModel : IModel, INotifyPropertyChanged
{
    public GameVersion Version { get; }

    private MemoryWorker? _memoryWorker;

    protected AbstractModel(GameVersion version)
    {
        Version = version;
    }

    private MemoryWorker MemoryWorker => _memoryWorker ??= MemoryWorker.GetInstance(Version, MemoryWorkerType);

    protected abstract MemoryWorkerType MemoryWorkerType { get; }

    protected virtual int BlockSize => MemoryWorker.BlockSize;

    public virtual string StartAddress => MemoryWorker.GetBlockStartAddress(0);
    public virtual string EndAddress => MemoryWorker.GetBlockEndAddress(0);

    public event PropertyChangedEventHandler? PropertyChanged;

    [NotifyPropertyChangedInvocator]
    protected virtual void OnPropertyChanged([CallerMemberName] string? propertyName = null)
    {
        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }

    public byte[] GetData(int dataLength, int addressOffset)
    {
        return MemoryWorker.GetData(dataLength, addressOffset);
    }

    public byte[] GetData(MemoryVariable variable, int index = 0)
    {
        return MemoryWorker.GetData(variable, index);
    }

    public byte[] GetData(int size, int offset, int index)
    {
        return MemoryWorker.GetData(size, offset, index);
    }

    public void SetData(byte[] input, int addressOffset, int index = 0)
    {
        MemoryWorker.SetData(input, addressOffset, index);
    }

    public void SetData<T>(T input, int addressOffset, int index = 0) where T : struct
    {
        MemoryWorker.SetData(input, addressOffset, index);
    }

    public void SetData<T>(T input, MemoryVariable variable, int index = 0) where T : struct
    {
        MemoryWorker.SetData(input, variable, index);
    }

    protected void SetData(string input, MemoryVariable variable, int index = 0)
    {
        MemoryWorker.SetData(input, variable, index);
    }

    protected void SetData(byte[] input, MemoryVariable variable, int index = 0)
    {
        MemoryWorker.SetData(input, variable, index);
    }

    protected string GetBlockStartAddress(int index, int offset = 0)
    {
        return MemoryWorker.GetBlockStartAddress(index, offset);
    }

    protected string GetBlockEndAddress(int index, int offset = 0)
    {
        return MemoryWorker.GetBlockEndAddress(index, offset);
    }

    public virtual byte[] DataBlock
    {
        get => GetData(BlockSize, 0);
        set => SetData(value, 0);
    }
}