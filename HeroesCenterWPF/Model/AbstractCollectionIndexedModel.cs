namespace HeroesCenter.Model;

public abstract class AbstractCollectionIndexedModel<T> : AbstractIndexedModel, ICollectionModel<T>
{
    protected AbstractCollectionIndexedModel(GameVersion version, int index) : base(version, index)
    {
    }

    protected IList<T> Collection { get; } = new List<T>();

    public T this[int i]
    {
        get => Collection[i];
        set
        {
            Collection[i] = value;
            OnElementChange(value, new PropertyChangedEventArgs(null));
        }
    }

    public IEnumerator<T> GetEnumerator()
    {
        return Collection.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    protected abstract void OnElementChange(object? sender, PropertyChangedEventArgs e);
}