namespace HeroesCenter.Model;

[Flags]
public enum CreatureFlags : uint
{
    None                = 0x0,
    DoubleWide          = 0x1 << 0,
    Flight              = 0x1 << 1,
    Shooter             = 0x1 << 2,
    Breath              = 0x1 << 3,
    Living              = 0x1 << 4,
    Catapult            = 0x1 << 5,     // Cyclop and Cyclop King
    Unknown3            = 0x1 << 6,     // NONE
    KING_1              = 0x1 << 7,     // Dragons + Behemot + Hydras + Phoenix
    KING_2              = 0x1 << 8,     // Archangel, Angel, Archdevil, Devil
    KING_3              = 0x1 << 9,     // Titan, Giant
    MindImmunity        = 0x1 << 10,
    NoObstaclePenalty   = 0x1 << 11,
    NoMeleePenalty      = 0x1 << 12,
    Unknown9            = 0x1 << 13,    // NONE
    FireImmunity        = 0x1 << 14,
    DoubleStrike        = 0x1 << 15,
    NoRetaliation       = 0x1 << 16,
    NoMorale            = 0x1 << 17,
    Undead              = 0x1 << 18,
    AttackAdjacent      = 0x1 << 19,
    X05                 = 0x1 << 20, //Лич, Магог, лава+арктический снайпер = аое?

    #region Not assigned flags
    X06                 = 0x1 << 21,
    X07                 = 0x1 << 22,
    X08                 = 0x1 << 23,
    X09                 = 0x1 << 24,
    X10                 = 0x1 << 25,
    X11                 = 0x1 << 26,
    X12                 = 0x1 << 27,
    X13                 = 0x1 << 28,
    X14                 = 0x1 << 29,
    X15                 = 0x1 << 30,
    #endregion

    Dragon              = 0x1u << 31,
}