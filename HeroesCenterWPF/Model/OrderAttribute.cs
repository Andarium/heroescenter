namespace HeroesCenter.Model;

public class OrderAttribute : Attribute
{
    public int Value { get; }

    public OrderAttribute(int value)
    {
        Value = value;
    }
}