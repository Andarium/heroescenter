namespace HeroesCenter.Model;

public class SecondarySkillsModel : AbstractCollectionIndexedModel<SecondarySkillModel>
{
    public SecondarySkillsModel(GameVersion version, int heroIndex) : base(version, heroIndex)
    {
        var skillCount = version == GameVersion.HornOfTheAbyss ? 29 : 28;
        for (var i = 0; i < skillCount; i++)
        {
            var element = new SecondarySkillModel(version, heroIndex, i);
            element.PropertyChanged += OnElementChange;
            Collection.Add(element);
        }
    }

    protected override MemoryWorkerType MemoryWorkerType => MemoryWorkerType.Hero;

    /// <summary>
    ///     Возвращает количество навыков в "реестре"
    /// </summary>
    private byte SkillCount
    {
        get => GetData(HeroStats.SecondarySkillsCount)[0];
        set => SetData(value, HeroStats.SecondarySkillsCount, Index);
    }

    /// <summary>
    ///     Переписывает реестр вторичных навыков для корректной работы.
    ///     Функция внешняя, так как используются все вторичные навыки для пересчета
    /// </summary>
    protected override void OnElementChange(object? sender, PropertyChangedEventArgs e)
    {
        var skills = GetVisibleSkills();

        for (var i = 0; i < skills.Count; i++)
        {
            skills[i].StageRegistry = (byte) (i + 1);
        }

        SkillCount = (byte) Collection.Count(skill => skill.Stage > 0);
    }

    private List<SecondarySkillModel> GetVisibleSkills()
    {
        return Collection
            .Where(skill => skill.Stage > 0 && skill.StageRegistry > 0)
            .OrderBy(x => x.StageRegistry)
            .ToList();
    }

    #region Public wrappers

    public SecondarySkillModel Pathfinding
    {
        get => Collection[0];
        set => Collection[0] = value;
    }

    public SecondarySkillModel Archery
    {
        get => Collection[1];
        set => Collection[1] = value;
    }

    public SecondarySkillModel Logistics
    {
        get => Collection[2];
        set => Collection[2] = value;
    }

    public SecondarySkillModel Scouting
    {
        get => Collection[3];
        set => Collection[3] = value;
    }

    public SecondarySkillModel Diplomacy
    {
        get => Collection[4];
        set => Collection[4] = value;
    }

    public SecondarySkillModel Navigation
    {
        get => Collection[5];
        set => Collection[5] = value;
    }

    public SecondarySkillModel Leadership
    {
        get => Collection[6];
        set => Collection[6] = value;
    }

    public SecondarySkillModel Wisdom
    {
        get => Collection[7];
        set => Collection[7] = value;
    }

    public SecondarySkillModel Mysticism
    {
        get => Collection[8];
        set => Collection[8] = value;
    }

    public SecondarySkillModel Luck
    {
        get => Collection[9];
        set => Collection[9] = value;
    }

    public SecondarySkillModel Ballistics
    {
        get => Collection[10];
        set => Collection[10] = value;
    }

    public SecondarySkillModel EagleEye
    {
        get => Collection[11];
        set => Collection[11] = value;
    }

    public SecondarySkillModel Necromancy
    {
        get => Collection[12];
        set => Collection[12] = value;
    }

    public SecondarySkillModel Estates
    {
        get => Collection[13];
        set => Collection[13] = value;
    }

    public SecondarySkillModel FireMagic
    {
        get => Collection[14];
        set => Collection[14] = value;
    }

    public SecondarySkillModel AirMagic
    {
        get => Collection[15];
        set => Collection[15] = value;
    }

    public SecondarySkillModel WaterMagic
    {
        get => Collection[16];
        set => Collection[16] = value;
    }

    public SecondarySkillModel EarthMagic
    {
        get => Collection[17];
        set => Collection[17] = value;
    }

    public SecondarySkillModel Scholar
    {
        get => Collection[18];
        set => Collection[18] = value;
    }

    public SecondarySkillModel Tactics
    {
        get => Collection[19];
        set => Collection[19] = value;
    }

    public SecondarySkillModel Artillery
    {
        get => Collection[20];
        set => Collection[20] = value;
    }

    public SecondarySkillModel Learning
    {
        get => Collection[21];
        set => Collection[21] = value;
    }

    public SecondarySkillModel Offence
    {
        get => Collection[22];
        set => Collection[22] = value;
    }

    public SecondarySkillModel Armorer
    {
        get => Collection[23];
        set => Collection[23] = value;
    }

    public SecondarySkillModel Intelligence
    {
        get => Collection[24];
        set => Collection[24] = value;
    }

    public SecondarySkillModel Sorcery
    {
        get => Collection[25];
        set => Collection[25] = value;
    }

    public SecondarySkillModel Resistance
    {
        get => Collection[26];
        set => Collection[26] = value;
    }

    public SecondarySkillModel FirstAid
    {
        get => Collection[27];
        set => Collection[27] = value;
    }

    public SecondarySkillModel Interference
    {
        get => Collection[28];
        set => Collection[28] = value;
    }

    #endregion
}