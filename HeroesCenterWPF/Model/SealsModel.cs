namespace HeroesCenter.Model;

public class SealsModel : AbstractIndexedModel
{
    private readonly ArtifactsViewModel _artifacts;
    private const int SingleSize = 1;
    private const int Size = 14;
    private int _lastFrameUpdate;

    public SealsModel(GameVersion version, int heroIndex, ArtifactsViewModel model) : base(version, heroIndex)
    {
        _artifacts = model;
    }

    public int this[int i] => GetData(SingleSize, HeroStats.Seals.Offset + i, Index)[0];

    protected override MemoryWorkerType MemoryWorkerType => MemoryWorkerType.Hero;

    private byte[] SealsArray
    {
        get => GetData(Size, HeroStats.Seals.Offset, Index);
        set => SetData(value, HeroStats.Seals.Offset, Index);
    }

    public void UpdateSeals()
    {
        if (FrameCounter.Frame == _lastFrameUpdate)
        {
            return;
        }

        _lastFrameUpdate = FrameCounter.Frame;

        var sealsArray = SealsArray;

        // body seals
        for (var i = 0; i < 6; i++)
        {
            _artifacts[i].IsSealed = sealsArray[i] != 0;
        }

        // ballista, cart, tent, catapult, spell book
        for (var i = 13; i < 18; i++)
        {
            _artifacts[i].IsSealed = sealsArray[i - 4] != 0;
        }

        // rings
        switch (sealsArray[6])
        {
            case 1:
                var rightIsEmpty = _artifacts.RightRing.IsEmpty;
                _artifacts.RightRing.IsSealed = rightIsEmpty;
                _artifacts.LeftRing.IsSealed = !rightIsEmpty;
                break;

            case 2:
                _artifacts.RightRing.IsSealed = true;
                _artifacts.LeftRing.IsSealed = true;
                break;

            default:
                _artifacts.RightRing.IsSealed = false;
                _artifacts.LeftRing.IsSealed = false;
                break;
        }

        // legs
        _artifacts.Legs.IsSealed = sealsArray[7] != 0;

        // slots
        int slotSealCount = sealsArray[8];
        for (var artIndex = 18; artIndex >= 9; artIndex--) 
        {
            if (artIndex == 17)
            {
                artIndex = 12; // skip ballista, cart, tent, catapult, spell book
            }

            _artifacts[artIndex].IsSealed = _artifacts[artIndex].IsEmpty &&
                0 < slotSealCount--; //Уменьшение кол-ва печатей должно происходить только если клетка пуста
        }
    }

    public void RemoveAllSeals()
    {
        IEnumerable<byte> bytes = Enumerable.Repeat<byte>(0, Size);
        SealsArray = bytes.ToArray();
    }

    #region Wrappers

    public int Head => this[0];

    public int Shoulders => this[1];

    public int Neck => this[2];

    public int RightArm => this[3];

    public int LeftArm => this[4];

    public int Body => this[5];

    public int Rings => this[6];

    public int Legs => this[7];

    public int Slots => this[8];

    #endregion
}