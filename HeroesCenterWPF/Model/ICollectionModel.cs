namespace HeroesCenter.Model;

public interface ICollectionModel<T> : IEnumerable<T>, IModel
{
    T this[int i] { get; set; }
}