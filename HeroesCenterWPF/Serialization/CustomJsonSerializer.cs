namespace HeroesCenter.Serialization;

public static class CustomJsonSerializer
{
    private static readonly JsonSerializerSettings SerializerSettings = new()
    {
        Formatting = Formatting.Indented,
        Converters = new List<JsonConverter> { new GameVersionConverter(), new StringEnumConverter() },
        ContractResolver = ShouldSerializeContractResolver.Instance
    };

    private static readonly JsonSerializerSettings DeserializerSettings = new()
    {
        Converters = new List<JsonConverter> { new GameVersionConverter() }
    };

    public static string Serialize(object input)
    {
        return JsonConvert.SerializeObject(input, SerializerSettings).Replace("\r\n", "\n");
    }

    public static T? Deserialize<T>(string input)
    {
        return JsonConvert.DeserializeObject<T>(input, DeserializerSettings);
    }
}