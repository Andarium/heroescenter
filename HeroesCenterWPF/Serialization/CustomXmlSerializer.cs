namespace HeroesCenter.Serialization;

public static class CustomXmlSerializer
{
    public static Dictionary<int, SpellInfo> DeserializeSpells(Stream xmlStream)
    {
        XDocument doc = XDocument.Load(xmlStream);
        return doc.Descendants("Entry")
            .Select(x => new SpellInfo
            (
                IntParse(x.Attribute("Index")?.Value),
                IntParse(x.Attribute("Level")?.Value),
                x.Attribute("Name")!.Value,
                x.Attribute("School")!.Value.ToEnum<MagicSchool>()
            ))
            .ToDictionary(x => x.Type, y => y);
    }

    private static int IntParse(string? value)
    {
        return value == null ? 0 : int.Parse(value);
    }
}