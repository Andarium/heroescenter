using static HeroesCenter.Core.GameVersion;

namespace HeroesCenter.Serialization;

public class GameVersionConverter : JsonConverter<GameVersion>
{
    private static readonly Dictionary<GameVersion, string[]> Aliases = new()
    {
        { ShadowOfDeath, new[] { ShadowOfDeath.ToString(), "SOD", "Shadow of Death" } },
        { InTheWakeOfGods, new[] { InTheWakeOfGods.ToString(), "WOG", "In the wake of Gods" } },
        { HornOfTheAbyss, new[] { HornOfTheAbyss.ToString(), "HOTA", "Horn of the Abyss" } }
    };

    public override void WriteJson(JsonWriter writer, GameVersion value, JsonSerializer serializer)
    {
        writer.WriteValue(value.ToString());
    }

    public override GameVersion ReadJson(JsonReader reader, Type objectType, GameVersion existingValue, bool hasExistingValue, JsonSerializer serializer)
    {
        var value = reader.Value?.ToString();

        if (string.IsNullOrWhiteSpace(value))
        {
            return existingValue;
        }

        foreach (var pair in Aliases)
        {
            if (pair.Value.Contains(value, StringComparison.InvariantCultureIgnoreCase))
            {
                return pair.Key;
            }
        }

        throw new JsonSerializationException($"Can't parse {value} as {objectType.FullName}");
    }
}