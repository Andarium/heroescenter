namespace HeroesCenter.Serialization;

public class ShouldSerializeContractResolver : DefaultContractResolver
{
    public static readonly ShouldSerializeContractResolver Instance = new();

    protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
    {
        JsonProperty property = base.CreateProperty(member, memberSerialization);

        if (property.PropertyType is { } type && typeof(IEnumerable).IsAssignableFrom(type))
        {
            bool PropertyShouldSerialize(object instance)
            {
                return (property.ValueProvider?.GetValue(instance) as IEnumerable)?.Cast<object?>().Any() == true;
            }

            property.ShouldSerialize = PropertyShouldSerialize;
        }

        return property;
    }
}