namespace HeroesCenter.Serialization;

public sealed class HexNumberJsonConverter : JsonConverter
{
    private static readonly CultureInfo Culture = CultureInfo.InvariantCulture;

    public override bool CanRead => false;

    public override bool CanConvert(Type objectType)
    {
        return false;
    }

    public override void WriteJson(JsonWriter writer, object? value, JsonSerializer serializer)
    {
        writer.WriteRawValue(string.Format(Culture, "0x{0:X}", value));
    }

    public override object ReadJson(JsonReader reader, Type objectType, object? existingValue, JsonSerializer serializer)
    {
        throw new NotImplementedException();
    }
}