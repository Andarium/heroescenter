namespace HeroesCenter.Serialization;

public class Int32HashSetRangeConverter : JsonConverter<HashSet<int>>
{
    public override void WriteJson(JsonWriter writer, HashSet<int>? value, JsonSerializer serializer)
    {
        if (value == null)
        {
            writer.WriteNull();
            return;
        }

        var ranges = new List<Range>();
        Range? range = null;
        foreach (int current in value.OrderBy(x => x))
        {
            if (range == null)
            {
                range = new Range(current, current);
                ranges.Add(range);
                continue;
            }

            if (current - range.End > 1)
            {
                range = new Range(current, current);
                ranges.Add(range);
            }
            else
            {
                range.End = current;
            }
        }

        writer.WriteValue(string.Join(",", ranges));
    }

    public override HashSet<int> ReadJson(JsonReader reader, Type objectType, HashSet<int>? existingValue, bool hasExistingValue, JsonSerializer serializer)
    {
        var s = reader.Value?.ToString();

        if (s == null)
        {
            return new HashSet<int>();
        }

        HashSet<int> ranges = s.Split(",", StringSplitOptions.RemoveEmptyEntries)
            .Select(x => new Range(x))
            .SelectMany(x => Enumerable.Range(x.Start, x.End - x.Start + 1))
            .OrderBy(x => x)
            .ToHashSet();
        return ranges;
    }

    private sealed class Range
    {
        public readonly int Start;
        public int End;

        public Range(int start, int end)
        {
            Start = start;
            End = end;
        }

        public Range(string rangeString)
        {
            var parts = rangeString.Split("-", StringSplitOptions.RemoveEmptyEntries);
            Start = int.Parse(parts[0]);
            End = parts.Length > 0
                ? int.Parse(parts[^1])
                : Start;
        }

        public override string ToString()
        {
            return Start == End
                ? Start.ToString()
                : $"{Start}-{End}";
        }
    }
}