namespace HeroesCenter.Serialization;

public class SpecializationDataConverter : JsonConverter<SpecializationData>
{
    public override void WriteJson(JsonWriter writer, SpecializationData? value, JsonSerializer serializer)
    {
        if (value == null)
        {
            writer.WriteNull();
            return;
        }

        writer.WriteValue($"{value.Index:D3}:{value.Key:D3}:{value.Name}");
    }

    public override SpecializationData ReadJson(JsonReader reader, Type objectType, SpecializationData? existingValue, bool hasExistingValue, JsonSerializer serializer)
    {
        var s = reader.Value?.ToString();

        if (s == null)
        {
            throw new NullReferenceException();
        }

        var parts = s.Split(":").ToArray();

        var index = int.Parse(parts[0]);
        var key = int.Parse(parts[1]);
        var name= parts[2];

        return new SpecializationData(index, key, name);
    }
}