using static HeroesCenter.Core.GameVersion;

namespace HeroesCenter.Serialization;

public class UnitSettingsConverter : JsonConverter<UnitSettings>
{
    public override void WriteJson(JsonWriter writer, UnitSettings? value, JsonSerializer serializer)
    {
        if (value == null)
        {
            writer.WriteNull();
            return;
        }

        string spriteVersion = value.SpriteVersion switch
        {
            ShadowOfDeath => "S",
            InTheWakeOfGods => "W",
            HornOfTheAbyss => "H",
            _ => throw new ArgumentOutOfRangeException(nameof(value.SpriteVersion), value.SpriteVersion.ToString())
        };

        var ut = value.UnitType;
        var sk = value.SpriteKey;
        var uts = ut.ToString("D3");
        var sks = sk == uts ? "" : sk;

        writer.WriteValue($"{uts}:{sks}:{spriteVersion}:{(value.IsAvailable ? 1 : 0)}");
    }

    public override UnitSettings ReadJson(JsonReader reader, Type type, UnitSettings? existing, bool hasExisting, JsonSerializer serializer)
    {
        var s = reader.Value?.ToString();

        if (s == null)
        {
            throw new NullReferenceException();
        }

        var parts = s.Split(":").ToArray();

        var unitType = int.Parse(parts[0]);
        var spriteKey = string.IsNullOrWhiteSpace(parts[1]) ? parts[0] : parts[1];

        var version = parts[2];
        var isAvailable = parts[3] == "1";

        GameVersion spriteVersion = version switch
        {
            "S" => ShadowOfDeath,
            "W" => InTheWakeOfGods,
            "H" => HornOfTheAbyss,
            _ => throw new ArgumentOutOfRangeException(nameof(version), version)
        };

        return new UnitSettings(unitType, spriteKey, spriteVersion, isAvailable);
    }
}