namespace HeroesCenter.Converters;

[ValueConversion(typeof(object), typeof(string))]
public class StringFormatConverter : IValueConverter
{
    public object? Convert(object? value, Type targetType, object parameter, CultureInfo culture)
    {
        if (parameter is string param && value != null)
        {
            return string.Format(param, value);
        }

        return null;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
        throw new NotImplementedException();
    }
}