namespace HeroesCenter.Converters;

[ValueConversion(typeof(object), typeof(string))]
public class NumericToStringConverter : IValueConverter
{
    public object Convert(object? value, Type targetType, object parameter, CultureInfo culture)
    {
        return value?.ToString() ?? string.Empty;
    }

    public object ConvertBack(object? value, Type targetType, object parameter, CultureInfo culture)
    {
        var str = value?.ToString();

        if (string.IsNullOrWhiteSpace(str))
        {
            return 0;
        }

        // Remove non digits symbols
        string number = CommonRegex.EverythingButDigitsAndMinus.Replace(str, string.Empty);

        if (!decimal.TryParse(number, out decimal temp))
        {
            return 0;
        }

        switch (targetType.Name)
        {
            case "Decimal":
                return System.Convert.ToDecimal(temp);
            case "Int32":
                return temp > int.MaxValue ? int.MaxValue : System.Convert.ToInt32(temp);
            case "Int16":
                return temp > short.MaxValue ? short.MaxValue : System.Convert.ToInt16(temp);
            case "Byte":
                return temp > byte.MaxValue ? byte.MaxValue : System.Convert.ToByte(temp);
            case "SByte":
                return temp > sbyte.MaxValue ? sbyte.MaxValue : System.Convert.ToSByte(temp);
            case "Boolean":
                return temp > 0;
            case "String":
                return str;
            default:
                return temp;
        }
    }
}