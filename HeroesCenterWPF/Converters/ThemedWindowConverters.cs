namespace HeroesCenter.Converters;

[ValueConversion(typeof(ResizeMode), typeof(Visibility))]
internal class MaximizeVisibilityConverter : IValueConverter
{
    #region IValueConverter Members

    public object? Convert(object? value, Type targetType, object parameter, CultureInfo culture)
    {
        if (value is ResizeMode mode)
        {
            return mode.EqualsAnyOf(ResizeMode.NoResize, ResizeMode.CanMinimize)
                ? Visibility.Collapsed
                : Visibility.Visible;
        }

        return null;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
        throw new NotImplementedException();
    }

    #endregion
}

[ValueConversion(typeof(ResizeMode), typeof(Visibility))]
internal class MinimizeVisibilityConverter : IValueConverter
{
    #region IValueConverter Members

    public object? Convert(object? value, Type targetType, object parameter, CultureInfo culture)
    {
        if (value is ResizeMode mode)
        {
            return mode == ResizeMode.NoResize
                ? Visibility.Collapsed
                : Visibility.Visible;
        }

        return null;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
        throw new NotImplementedException();
    }

    #endregion
}