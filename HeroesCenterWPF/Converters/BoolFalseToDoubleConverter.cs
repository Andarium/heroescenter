namespace HeroesCenter.Converters;

[ValueConversion(typeof(bool), typeof(double))]
public class BoolFalseToDoubleConverter : IValueConverter
{
    public object Convert(object? value, Type targetType, object? parameter, CultureInfo culture)
    {
        if (value is true or null)
        {
            return 1.0;
        }

        return parameter is not double opacity ? 0.0 : opacity;
    }

    public object ConvertBack(object? value, Type targetType, object? parameter, CultureInfo culture)
    {
        throw new NotImplementedException();
    }
}