namespace HeroesCenter.Converters;

[ValueConversion(typeof(bool), typeof(Visibility))]
public class BoolToCollapsedConverter : IValueConverter
{
    public object Convert(object? value, Type targetType, object? parameter, CultureInfo culture)
    {
        if (value is bool val)
        {
            if (parameter != null)
            {
                return val ? Visibility.Visible : Visibility.Collapsed;
            }

            return val ? Visibility.Collapsed : Visibility.Visible;
        }

        return default(Visibility);
    }

    public object ConvertBack(object? value, Type targetType, object? parameter, CultureInfo culture)
    {
        if (value is Visibility val)
        {
            if (parameter == null)
            {
                return val != Visibility.Collapsed;
            }

            return val == Visibility.Collapsed;
        }

        return default(bool);
    }
}