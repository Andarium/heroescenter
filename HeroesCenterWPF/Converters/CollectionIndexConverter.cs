namespace HeroesCenter.Converters;

public class CollectionIndexConverter : IMultiValueConverter
{
    public object? Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
    {
        if (values.Length < 2)
        {
            throw new ArgumentException();
        }

        if (values[0] is IEnumerable enumerable)
        {
            return !int.TryParse(values[1].ToString(), out int index)
                ? enumerable.Cast<object?>().LastOrDefault()
                : enumerable.Cast<object?>().ElementAtOrLastOrDefault(index);
        }

        return null;
    }

    public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
    {
        throw new NotImplementedException();
    }
}