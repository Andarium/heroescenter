namespace HeroesCenter.View;

/// <summary>
///     Логика взаимодействия для CreatureDialog.xaml
/// </summary>
public partial class CreatureDialog
{
    private readonly Dictionary<PropertyInfo, object?> _initialValues;
    private readonly CreatureViewModel _model;
    private bool _restoreValues = true;

    public CreatureDialog(CreatureViewModel model)
    {
        _model = model;
        DataContext = _model;

        InitializeComponent();
        _initialValues = _model
            .GetType()
            .GetProperties(BindingFlags.Public | BindingFlags.Instance)
            .Where(x => x.CanRead && x.CanWrite)
            .ToDictionary(x => x, y => y.GetValue(_model));
    }

    private void ButtonOk_OnMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
    {
        _restoreValues = false;
        Close();
    }

    private void ButtonCancel_OnMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
    {
        _restoreValues = true;
        Close();
    }

    protected override void OnClosing(CancelEventArgs e)
    {
        base.OnClosing(e);

        if (_restoreValues && MemoryWorker.IsGameRunning())
        {
            foreach ((PropertyInfo key, object? value) in _initialValues)
            {
                key.SetValue(_model, value);
            }
        }
    }
}