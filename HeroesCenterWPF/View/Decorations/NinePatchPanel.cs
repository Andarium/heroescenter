using Rect = System.Windows.Rect;

namespace HeroesCenter.View.Decorations;

public class NinePatchPanel : ContentControl
{
    public static readonly DependencyProperty CenterAreaProperty = Register<Int32Rect, NinePatchPanel>("CenterArea", Int32Rect.Empty, SetArea);
    public static readonly DependencyProperty BackgroundImageProperty = Register<ImageSource, NinePatchPanel>("BackgroundImage", null, SetImg);


    private ImageSource[]? _patchs;

    [Category("Background")]
    [Description("Set nine patch background image")]
    public ImageSource BackgroundImage
    {
        get => (ImageSource) GetValue(BackgroundImageProperty);
        set => SetValue(BackgroundImageProperty, value);
    }

    [Category("Background")]
    [Description("Set the center split area")]
    public Int32Rect CenterArea
    {
        get => (Int32Rect) GetValue(CenterAreaProperty);
        set => SetValue(CenterAreaProperty, value);
    }

    protected override void OnRender(DrawingContext dc)
    {
        if (_patchs != null)
        {
            var x1 = _patchs[0].Width;
            var x2 = Math.Max(ActualWidth - _patchs[2].Width, 0);
            var y1 = _patchs[0].Height;
            var y2 = Math.Max(ActualHeight - _patchs[6].Height, 0);
            var w1 = _patchs[0].Width;
            var w2 = Math.Max(x2 - x1, 0);
            var w3 = _patchs[2].Width;
            var h1 = _patchs[0].Height;
            var h2 = Math.Max(y2 - y1, 0);
            var h3 = _patchs[6].Height;
            var rects = new[]
            {
                new Rect(0, 0, w1, h1),
                new Rect(x1, 0, w2, h1),
                new Rect(x2, 0, w3, h1),
                new Rect(0, y1, w1, h2),
                new Rect(x1, y1, w2, h2),
                new Rect(x2, y1, w3, h2),
                new Rect(0, y2, w1, h3),
                new Rect(x1, y2, w2, h3),
                new Rect(x2, y2, w3, h3)
            };
            for (var i = 0; i < 9; i++)
            {
                dc.DrawImage(_patchs[i], rects[i]);
            }
        }

        base.OnRender(dc);
    }

    private static void SetArea(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
        var np = d as NinePatchPanel;

        if (np?.BackgroundImage is BitmapSource bm)
        {
            SetPatchs(np, bm);
        }
    }

    private static void SetImg(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
        if (d is NinePatchPanel np)
        {
            var bm = (BitmapSource) np.BackgroundImage;
            if (np.CenterArea == Int32Rect.Empty)
            {
                var w = bm.PixelWidth / 3;
                var h = bm.PixelHeight / 3;
                np.CenterArea = new Int32Rect(w, h, w, h);
            }
            else
            {
                SetPatchs(np, bm);
            }
        }
    }

    private static void SetPatchs(NinePatchPanel np, BitmapSource bm)
    {
        var x1 = np.CenterArea.X;
        var x2 = np.CenterArea.X + np.CenterArea.Width;
        var y1 = np.CenterArea.Y;
        var y2 = np.CenterArea.Y + np.CenterArea.Height;
        var w1 = np.CenterArea.X;
        var w2 = np.CenterArea.Width;
        var w3 = bm.PixelWidth - np.CenterArea.X - np.CenterArea.Width;
        var h1 = np.CenterArea.Y;
        var h2 = np.CenterArea.Height;
        var h3 = bm.PixelHeight - np.CenterArea.Y - np.CenterArea.Height;
        np._patchs = new ImageSource[]
        {
            new CroppedBitmap(bm, new Int32Rect(0, 0, w1, h1)),
            new CroppedBitmap(bm, new Int32Rect(x1, 0, w2, h1)),
            new CroppedBitmap(bm, new Int32Rect(x2, 0, w3, h1)),
            new CroppedBitmap(bm, new Int32Rect(0, y1, w1, h2)),
            new CroppedBitmap(bm, new Int32Rect(x1, y1, w2, h2)),
            new CroppedBitmap(bm, new Int32Rect(x2, y1, w3, h2)),
            new CroppedBitmap(bm, new Int32Rect(0, y2, w1, h3)),
            new CroppedBitmap(bm, new Int32Rect(x1, y2, w2, h3)),
            new CroppedBitmap(bm, new Int32Rect(x2, y2, w3, h3))
        };
    }
}