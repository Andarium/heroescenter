using static System.Windows.Input.Key;

namespace HeroesCenter.View;

public sealed partial class MainWindow : IDisposable
{
    private readonly GlobalKeyboardHook _globalHook;

    public MainWindow()
    {
        InitializeComponent();
        Title = AssemblyInfoHelper.FullName;
        _globalHook = new GlobalKeyboardHook(new[] { OemTilde, F1, F2, F3, F5, F6, F7, F8, F9, F10, LeftShift, LeftAlt });
        _globalHook.KeyboardPressed += OnKeyboardPressedGlobal;
    }

    private static GameViewModel? Game => GameViewModel.Instance;
    private static HeroManager? HeroManager => Game?.HeroManager;
    private static HeroViewModel? ActiveHero => Game?.ActiveHero;
    private static HeroViewModel? CurrentHero => Game?.CurrentHero;
    private static PlayerViewModel? ActivePlayer => Game?.ActivePlayer;
    private static BattleViewModel? Battle => Game?.Battle;
    private static UIViewModel? UI => Game?.UI;

    private static byte[]? _tempResources;
    private static Dictionary<int, byte>? _tempMapHero;
    private static Dictionary<int, byte>? _tempMapPlayer;

    private static HeroViewModel? GetHero() => GlobalKeyboardHook.IsKeyDown(LeftShift) ? CurrentHero : ActiveHero;

    public void Dispose()
    {
        _globalHook.Dispose();
    }

    private void OnKeyboardPressedGlobal(object? sender, GlobalKeyboardHookEventArgs e)
    {
        if (e.KeyboardState != KeyboardState.KeyPress)
        {
            return;
        }

        if (MemoryWorker.IsGameRunning(out bool isGameForeground, out bool isProgramForeground))
        {
            if (isGameForeground || isProgramForeground)
            {
                bool cheatApplied = false;
                switch (e.KeyboardData.KeyCode)
                {
                    case OemTilde:
                        ListHeroes.SelectedItem = GameViewModel.Instance!.GetInitialHero();
                        break;
                    case F1:
                        if (GlobalKeyboardHook.IsKeyDown(LeftShift))
                        {
                            DiffHelper.RememberHero();
                        }
                        else
                        {
                            DiffHelper.DiffHero();
                        }

                        break;
                    case F2 when GetHero() is { } hero:
                        hero.RestoreMana(GlobalKeyboardHook.IsKeyDown(LeftAlt));
                        cheatApplied = true;
                        break;
                    case F3 when GetHero() is { } hero:
                        var value = GlobalKeyboardHook.IsKeyDown(LeftAlt)
                            ? 15_000
                            : hero.PrimarySkills.DailyMovement;

                        hero.SetMovementPoints(value);
                        hero.ResetTeleportUses();
                        cheatApplied = true;

                        break;
                    case F5 when Battle is { } battle:
                        battle.IsMagicGarrison = (byte) (battle.IsMagicGarrison == 0 ? 1 : 0);
                        cheatApplied = true;
                        break;
                    case F6 when Battle is { } battle:

                        if (GlobalKeyboardHook.IsKeyDown(LeftAlt))
                        {
                            battle.IsMagicUsedHeroRight = 0;
                        }
                        else
                        {
                            battle.IsMagicUsedHeroLeft = 0;
                        }

                        cheatApplied = true;
                        break;
                    case F7 when ActivePlayer is { } player:
                    {
                        if (GlobalKeyboardHook.IsKeyDown(LeftShift))
                        {
                            _tempResources = player.Resources.DataBlock;
                        }
                        else if (_tempResources != null)
                        {
                            player.Resources.DataBlock = _tempResources;
                        }

                        break;
                    }
                    case F8 when ActiveHero is { } hero && UI is { } ui:
                    {
                        hero.X = ui.CursorCellX;
                        hero.Y = ui.CursorCellY;
                        hero.Z = ui.CursorCellZ;
                        cheatApplied = true;
                        break;
                    }
                    case F9 when Game is { } && GetHero() is { } hero:
                    {
                        if (GlobalKeyboardHook.IsKeyDown(LeftShift))
                        {
                            _tempMapHero = hero.GetDataBlockMap();
                            _tempMapPlayer = Game.PlayerManager[1].GetDataBlockMap();
                        }
                        else if (_tempMapHero != null && _tempMapPlayer != null)
                        {
                            var str = new StringBuilder();
                            var newMap = hero.GetDataBlockMap();
                            var diffKeys = _tempMapHero.GetKeysOfDiff(newMap);

                            str.AppendLine("Start Hero:");

                            foreach (var i in diffKeys)
                            {
                                str.AppendLine($"[{i:X3} | {i:D4}]: {_tempMapHero[i]:X2} => {newMap[i]:X2}");
                            }

                            newMap = Game.PlayerManager[1].GetDataBlockMap();
                            diffKeys = _tempMapPlayer.GetKeysOfDiff(newMap);
                            str.AppendLine();
                            str.AppendLine();
                            str.AppendLine("Start Player:");

                            foreach (var i in diffKeys)
                            {
                                str.AppendLine($"[{i:X3} | {i:D4}]: {_tempMapHero[i]:X2} => {newMap[i]:X2}");
                            }

                            Clipboard.SetText(str.ToString());
                        }

                        break;
                    }
                    case F10 when GlobalKeyboardHook.IsKeyDown(LeftShift) && GetHero() is { } hero:
                    {
                        foreach (var skill in hero.SecondarySkills)
                        {
                            skill.Stage = 3;
                        }

                        hero.MoraleTotal = 40;
                        hero.LuckTotal = 40;

                        break;
                    }
                }

                if (cheatApplied)
                {
                    e.Handled = true;
                }
            }
        }
        else
        {
            if (e.KeyboardData.KeyCode == OemTilde && AttachToProcess())
            {
                e.Handled = true;
            }
        }
    }

    private bool AttachToProcess()
    {
        if (!MemoryWorker.IsGameRunning())
        {
            var processes = ProcessHelper.GetProcesses();
            ProcessList.Collection = processes;
            ProcessBox.SelectedIndex = processes.Length == 0 ? 0 : 1;
        }

        return MemoryWorker.IsGameRunning();
    }

    // private static List<String> _names = new();

    private void InventoryButton_Click(object sender, RoutedEventArgs e)
    {
        BackpackWindow.ShowDialog(this, Game);
    }

    private void SpellBookButton_Click(object sender, RoutedEventArgs e)
    {
        SpellBookWindow.ShowDialog(this, Game);
    }

    private void DownloadButton_Click(object sender, RoutedEventArgs e)
    {
        // ActiveHero.PrimarySkills.SpecializationImage
        // MemoryInjector.main(Game!.CurrentHero!.PrimarySkills.HeroIndex);

        if (Game is not { CurrentHero: { } hero } game)
        {
            return;
        }

        if (!hero.Unknown && game.ActivePlayer is { } player)
        {
            player.AddHeroOnMap(hero);
        }
        /*if (Keyboard.IsKeyDown(LeftShift))
            {
                var hm = game.HeroManager;

                for (int i = 0; i < _names.Count; i++)
                {
                    // hm[i].DisplayName = $"Hero_{i}";
                    hm[i].DisplayName = _names[i];
                }
            }
            else
            {
                _names = game.HeroManager.Select(x => x.DisplayName).ToList();
            }*/

        /*if (game.CurrentHero is { } hero)
            {
                int newArt = hero.Artifacts[0].Type == 140 ? -1 : 140;
                foreach (var artifact in hero.Artifacts)
                {
                    artifact.Type = newArt;
                }

                Refresh();
            }*/
        // new DebugWindow().ShowDialog();

        // var fullBp = hero.Artifacts.Skip(19).Select(x=>x.DisplayName).ToList();
        // Console.WriteLine(fullBp.Count);
    }

    private void ProcessBox_OnDropDownOpened(object sender, EventArgs e)
    {
        var processes = ProcessHelper.GetProcesses();
        if (processes.Length == 0)
        {
            ProcessBox.IsDropDownOpen = false;
        }
        else
        {
            ProcessList.Collection = processes;
        }
    }

    private void ProcessBox_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
    {
        if (ProcessBox.SelectedIndex < 1)
        {
            return;
        }

        if (ProcessBox.SelectedItem is Process p)
        {
            SetProcess(p.Id);
        }
    }

    private void MainWindow_OnMouseDown(object sender, MouseButtonEventArgs e)
    {
        MainGrid.Focus();
    }

    private void UseNewImages_OnChecked(object sender, RoutedEventArgs e)
    {
        HeroHelper.Instance.UseNewImages = true;
    }

    private void UseNewImages_OnUnchecked(object sender, RoutedEventArgs e)
    {
        HeroHelper.Instance.UseNewImages = false;
    }

    private void MainWindow_Activated(object sender, EventArgs e)
    {
        if (GameViewModel.Instance == null)
        {
            return;
        }

        if (MemoryWorker.IsGameRunning())
        {
            this.ReloadDataContext();
            HeroManager?.CollectionView.Refresh();
        }
    }

    private void SetProcess(int processId)
    {
        if (MemoryWorker.SetProcess(processId, out GameVersion? version, Unload))
        {
            var viewModel = GameViewModel.CreateInstance(Settings.Instance, version.Value);
            MemoryWorker.RaiseRecalculate();
            ListHeroes.ItemsSource = HeroManager?.CollectionView;
            ListHeroes.SelectedItem = viewModel.GetInitialHero();
            HeroManager?.ChangeVisibility((Flags) FlagBox.SelectedValue, true);
            DataContext = viewModel;
        }
    }

    private void Unload()
    {
        if (GameViewModel.Instance != null)
        {
            DataContext = null;
            ListHeroes.ItemsSource = null;
            ProcessBox.SelectedIndex = 0;
        }

        GameViewModel.Instance = null;
    }

    private void FlagBox_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
    {
        HeroManager?.ChangeVisibility((Flags) FlagBox.SelectedValue);
    }
}