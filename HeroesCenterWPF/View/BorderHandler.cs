namespace HeroesCenter.View;

public class BorderHandler
{
    private readonly Thickness _resizeFrame;
    private readonly Window _window;
    private readonly HandleRef _handle;
    private WindowBorderEdge _edge = WindowBorderEdge.None;

    private double _botMin;
    private double _leftMax;
    private double _rightMin;
    private double _topMax;

    private Point _startCursor;
    private Point _startPos;
    private Size _startSize;

    public BorderHandler(Window window, Thickness resizeFrame)
    {
        _resizeFrame = resizeFrame;
        _window = window;

        _window.MouseMove += OnMouseMove;
        _window.MouseLeftButtonDown += OnMouseLeftButtonDown;
        _window.MouseLeftButtonUp += (_, _) => _window.ReleaseMouseCapture();
        _window.SizeChanged += OnSizeChanged;
        RecalculateBorders();

        _handle = new HandleRef(_window, new WindowInteropHelper(_window).Handle);
    }

    private WindowState WindowState => _window.WindowState;
    private ResizeMode ResizeMode => _window.ResizeMode;

    private double Width
    {
        get => _window.Width;
        set => _window.Width = value;
    }

    private double MinWidth
    {
        get => _window.MinWidth;
        set => _window.MinWidth = value;
    }

    private double Height
    {
        get => _window.Height;
        set => _window.Height = value;
    }

    private double MinHeight
    {
        get => _window.MinHeight;
        set => _window.MinHeight = value;
    }

    private double Left
    {
        get => _window.Left;
        set => _window.Left = value;
    }

    private double Top
    {
        get => _window.Top;
        set => _window.Top = value;
    }

    private Point Position => new(Left, Top);

    private bool IsMouseCaptured => _window.IsMouseCaptured;

    private void OnSizeChanged(object sender, SizeChangedEventArgs e)
    {
        RecalculateBorders();
    }

    private void RecalculateBorders()
    {
        var size = _window.RenderSize;

        _leftMax = _resizeFrame.Left;
        _rightMin = size.Width - _resizeFrame.Right;
        _topMax = _resizeFrame.Top;
        _botMin = size.Height - _resizeFrame.Bottom;
    }

    private void CheckCursorResize(Point position)
    {
        var x = position.X;
        var y = position.Y;
        Cursor newCursor;
        WindowBorderEdge newEdge;

        var l = x <= _leftMax;
        var r = x >= _rightMin;
        var t = y <= _topMax;
        var b = y >= _botMin;

        if (l)
        {
            if (t)
            {
                newCursor = Cursors.SizeNWSE;
                newEdge = WindowBorderEdge.TopLeft;
            }
            else if (b)
            {
                newCursor = Cursors.SizeNESW;
                newEdge = WindowBorderEdge.BottomLeft;
            }
            else
            {
                newCursor = Cursors.SizeWE;
                newEdge = WindowBorderEdge.Left;
            }
        }
        else if (r)
        {
            if (t)
            {
                newCursor = Cursors.SizeNESW;
                newEdge = WindowBorderEdge.TopRight;
            }
            else if (b)
            {
                newCursor = Cursors.SizeNWSE;
                newEdge = WindowBorderEdge.BottomRight;
            }
            else
            {
                newCursor = Cursors.SizeWE;
                newEdge = WindowBorderEdge.Right;
            }
        }
        else
        {
            if (t)
            {
                newCursor = Cursors.SizeNS;
                newEdge = WindowBorderEdge.Top;
            }
            else if (b)
            {
                newCursor = Cursors.SizeNS;
                newEdge = WindowBorderEdge.Bottom;
            }
            else
            {
                newCursor = Cursors.Arrow;
                newEdge = WindowBorderEdge.None;
            }
        }

        _window.Cursor = newCursor;
        _edge = newEdge;
    }

    private void OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
    {
        if (WindowState == WindowState.Maximized || ResizeMode != ResizeMode.CanResize || _edge == WindowBorderEdge.None)
        {
            return;
        }

        _startCursor = e.GetPosition(_window);
        _startPos = new Point(Left, Top);
        _startCursor.X += _startPos.X;
        _startCursor.Y += _startPos.Y;
        _startSize = new Size(Width, Height);
        _window.CaptureMouse();
    }

    private void OnMouseMove(object sender, MouseEventArgs e)
    {
        if (WindowState == WindowState.Maximized || ResizeMode != ResizeMode.CanResize)
        {
            return;
        }

        var cursorLocation = e.GetPosition(_window);
        if (!IsMouseCaptured)
        {
            CheckCursorResize(cursorLocation);
            return;
        }

        var newLeft = Left;
        var newTop = Top;
        cursorLocation.X += newLeft;
        cursorLocation.Y += newTop;

        var xDiff = cursorLocation.X - _startCursor.X;
        var yDiff = cursorLocation.Y - _startCursor.Y;

        if (!_edge.IntersectsFlag(WindowBorderEdge.Horizontal))
        {
            xDiff = 0;
        }
        else if (_edge.HasEnumFlag(WindowBorderEdge.Left))
        {
            xDiff *= -1;
        }

        if (!_edge.IntersectsFlag(WindowBorderEdge.Vertical))
        {
            yDiff = 0;
        }
        else if (_edge.HasEnumFlag(WindowBorderEdge.Top))
        {
            yDiff *= -1;
        }

        var newWidth = Math.Max(_startSize.Width + xDiff, MinWidth);
        xDiff = newWidth - _startSize.Width;

        var newHeight = Math.Max(_startSize.Height + yDiff, MinHeight);
        yDiff = newHeight - _startSize.Height;

        if (_edge.HasEnumFlag(WindowBorderEdge.Left))
        {
            newLeft = _startPos.X - xDiff;
        }

        if (_edge.HasEnumFlag(WindowBorderEdge.Top))
        {
            newTop = _startPos.Y - yDiff;
        }

        NativeMethods.SetWindowPos(_handle, default, (int) newLeft, (int) newTop, (int) newWidth, (int) newHeight);
    }

    [Flags]
    private enum WindowBorderEdge
    {
        None = 0,
        Left = 1,
        Top = 2,
        Right = 4,
        Bottom = 8,
        TopLeft = Top | Left,
        TopRight = Top | Right,
        BottomLeft = Bottom | Left,
        BottomRight = Bottom | Right,
        Horizontal = Left | Right,
        Vertical = Top | Bottom
    }
}