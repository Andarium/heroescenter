namespace HeroesCenter.View.Extensions;

public sealed class Spacing
{
    public static readonly DependencyProperty VerticalProperty =
        DependencyProperty.RegisterAttached("Vertical", typeof(double), typeof(Spacing), new UIPropertyMetadata(0d, ChangedCallback));

    public static readonly DependencyProperty HorizontalProperty =
        DependencyProperty.RegisterAttached("Horizontal", typeof(double), typeof(Spacing), new UIPropertyMetadata(0d, ChangedCallback));

    private static void ChangedCallback(object sender, DependencyPropertyChangedEventArgs e)
    {
        if (sender is Panel panel)
        {
            panel.Loaded -= OnPanelLoaded;
            panel.Loaded += OnPanelLoaded;

            panel.LayoutUpdated -= OnLayoutUpdated;
            panel.LayoutUpdated += OnLayoutUpdated;

            panel.SizeChanged -= OnSizeChanged;
            panel.SizeChanged += OnSizeChanged;

            if (panel.IsLoaded)
            {
                OnPanelLoaded(panel);
            }
        }
    }

    private static void OnSizeChanged(object sender, SizeChangedEventArgs e)
    {
        if (sender is Panel panel)
        {
            OnPanelLoaded(panel);
        }
    }

    private static void OnLayoutUpdated(object? sender, EventArgs e)
    {
        if (sender is Panel panel)
        {
            OnPanelLoaded(panel);
        }
    }

    [UsedImplicitly]
    public static double GetHorizontal(DependencyObject obj)
    {
        return (double) obj.GetValue(HorizontalProperty);
    }

    [UsedImplicitly]
    public static void SetHorizontal(DependencyObject obj, double space)
    {
        obj.SetValue(HorizontalProperty, space);
    }

    [UsedImplicitly]
    public static double GetVertical(DependencyObject obj)
    {
        return (double) obj.GetValue(VerticalProperty);
    }

    [UsedImplicitly]
    public static void SetVertical(DependencyObject obj, double value)
    {
        obj.SetValue(VerticalProperty, value);
    }

    private static void OnPanelLoaded(object sender, RoutedEventArgs? e = default)
    {
        if(sender is Panel panel)
        {
            List<FrameworkElement> elements = panel
                .Children
                .Cast<object>()
                .Where(x => x is FrameworkElement)
                .Cast<FrameworkElement>()
                .Where(x => x.Visibility == Visibility.Visible)
                .Where(x => x.ActualWidth > 1)
                .ToList();

            if (elements.Count > 1)
            {
                for (var i = 0; i < elements.Count; i++)
                {
                    bool isLast = i == elements.Count - 1;

                    if (isLast)
                    {
                        SetSpacing(elements[i]);
                    }
                    else
                    {
                        SetSpacing(elements[i], GetHorizontal(panel), GetVertical(panel));
                    }
                }
            }
            else if (elements.Count == 1)
            {
                SetSpacing(elements[0]);
            }
        }
    }

    private static void SetSpacing(FrameworkElement input, double x = 0, double y = 0)
    {
        Thickness margin = input.Margin;
        margin.Right = x;
        margin.Bottom = y;
        input.Margin = margin;
    }
}