namespace HeroesCenter.View.Controls;

public partial class ArtifactsPanel
{
    public ArtifactsPanel()
    {
        InitializeComponent();
        CreateArtBoxes();
    }

    private void CreateArtBoxes()
    {
        var margins = new[]
        {
            new Thickness(0, 7, 109, 0),
            new Thickness(0, 219, 50, 0),
            new Thickness(0, 57, 109, 0),
            new Thickness(0, 46, 235, 0),
            new Thickness(0, 161, 56, 0),
            new Thickness(0, 108, 109, 0),
            new Thickness(0, 46, 187, 0),
            new Thickness(0, 161, 7, 0),
            new Thickness(0, 272, 103, 0),
            new Thickness(0, 120, 235, 0),
            new Thickness(0, 170, 219, 0),
            new Thickness(0, 221, 203, 0),
            new Thickness(0, 272, 185, 0),
            new Thickness(0, 7, 54, 0),
            new Thickness(0, 7, 8, 0),
            new Thickness(0, 53, 8, 0),
            new Thickness(0, 99, 8, 0),
            new Thickness(0, 288, 8, 0),
            new Thickness(0, 272, 235, 0),
            new Thickness(0, 342, 215, 0),
            new Thickness(0, 342, 169, 0),
            new Thickness(0, 342, 123, 0),
            new Thickness(0, 342, 77, 0),
            new Thickness(0, 342, 31, 0)
        };

        for (var i = 0; i < 24; i++)
        {
            var box = new ArtifactBox
            {
                Name = "ArtBox" + i,
                Margin = margins[i],
            };

            var path = $"{nameof(GameViewModel.CurrentHero)}.{nameof(HeroViewModel.Artifacts)}[{i}]";
            var binding = new Binding(path);
            BindingOperations.SetBinding(box, DataContextProperty, binding);
            GridPanel.Children.Add(box);
        }
    }
}