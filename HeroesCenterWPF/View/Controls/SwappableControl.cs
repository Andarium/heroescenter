namespace HeroesCenter.View.Controls;

public class SwappableControl : ImageControl
{
    protected const double DragThreshold = 5;

    protected Point StartPoint;

    protected virtual void OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
    {
        StartPoint = e.GetPosition(null);
    }

    protected virtual void OnPreviewMouseMove(object sender, MouseEventArgs e)
    {
        Point mousePos = e.GetPosition(null);
        Vector diff = StartPoint - mousePos;

        if (e.LeftButton == MouseButtonState.Released)
        {
            return;
        }

        if (Math.Abs(diff.X) > DragThreshold || Math.Abs(diff.Y) > DragThreshold)
        {
            if (Convert.ChangeType(sender, GetType()) is DependencyObject senderControl)
            {
                var dragData = new DataObject(GetType().Name, senderControl);
                DragDrop.DoDragDrop(senderControl, dragData, DragDropEffects.Move);
            }
        }
    }

    protected virtual void OnDragEnter(object sender, DragEventArgs e)
    {
        if (!e.Data.GetDataPresent(GetType().Name) || sender == e.Source)
        {
            e.Effects = DragDropEffects.None;
        }
    }

    protected virtual void OnDrop(object sender, DragEventArgs e)
    {
        if (e.Data.GetDataPresent(GetType().Name))
        {
            object converted = Convert.ChangeType(e.Data.GetData(GetType().Name), GetType());
            if (converted is SwappableControl other && !ReferenceEquals(other, this))
            {
                Swap(this, other);
            }
        }
    }

    protected virtual void Swap(SwappableControl first, SwappableControl second)
    {
    }
}