namespace HeroesCenter.View.Controls;

public class BaseCustomToggle : ToggleButton, INotifyPropertyChanged, IPointerClickHandler
{
    public static readonly DependencyProperty ContentTypeProperty = Register<SlicedButtonContentType, BaseCustomToggle>("ContentType");
    public static readonly DependencyProperty ImageSourceProperty = Register<ImageSource, BaseCustomToggle>("ImageSource");
    public static readonly DependencyProperty ImageTransformProperty = Register<Transform, BaseCustomToggle>("ImageTransform");
    public static readonly DependencyProperty HoverImageSourceProperty = Register<ImageSource, BaseCustomToggle>("HoverImageSource");
    public static readonly DependencyProperty ClickCommandProperty = Register<ICommand, BaseCustomToggle>("ClickCommand");
    public static readonly DependencyProperty StretchProperty = Register<Stretch, BaseCustomToggle>("Stretch");

    public BaseCustomToggle()
    {
        ClickCommand = new ClickCommand<IPointerClickHandler>(this);
        IsEnabledChanged += CheckClick;
        IsVisibleChanged += CheckClick;
    }

    public Stretch Stretch
    {
        get => (Stretch) GetValue(StretchProperty);
        set => SetValueDp(StretchProperty, value);
    }

    public ICommand ClickCommand
    {
        get => (ICommand) GetValue(ClickCommandProperty);
        set => SetValueDp(ClickCommandProperty, value);
    }

    public SlicedButtonContentType ContentType
    {
        get => (SlicedButtonContentType) GetValue(ContentTypeProperty);
        set => SetValueDp(ContentTypeProperty, value);
    }

    public ImageSource ImageSource
    {
        get => (ImageSource) GetValue(ImageSourceProperty);
        set => SetValueDp(ImageSourceProperty, value);
    }

    public Transform ImageTransform
    {
        get => (Transform) GetValue(ImageTransformProperty);
        set => SetValueDp(ImageTransformProperty, value);
    }

    public ImageSource HoverImageSource
    {
        get => (ImageSource) GetValue(HoverImageSourceProperty);
        set => SetValueDp(HoverImageSourceProperty, value);
    }

    public event PropertyChangedEventHandler? PropertyChanged;

    public void OnPointerClick()
    {
        OnClick();
    }

    public bool CanClick()
    {
        return IsEnabled && Visibility == Visibility.Visible;
    }

    public event EventHandler? CanClickChanged;

    private void CheckClick(object sender, DependencyPropertyChangedEventArgs e)
    {
        CanClickChanged?.Invoke(this, EventArgs.Empty);
    }

    protected void SetValueDp(DependencyProperty property, object value, [CallerMemberName] string? p = null)
    {
        if (value == GetValue(property))
        {
            return;
        }

        SetValue(property, value);
        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(p));
    }
}