namespace HeroesCenter.View.Controls;

public class BaseCustomButton : Button, INotifyPropertyChanged, IPointerClickHandler
{
    public static readonly DependencyProperty ContentTypeProperty = DHelper.Register<SlicedButtonContentType, BaseCustomButton>("ContentType");
    public static readonly DependencyProperty ImageSourceProperty = DHelper.Register<ImageSource, BaseCustomButton>("ImageSource");
    public static readonly DependencyProperty ImageTransformProperty = DHelper.Register<Transform, BaseCustomButton>("ImageTransform");
    public static readonly DependencyProperty HoverImageSourceProperty = DHelper.Register<ImageSource, BaseCustomButton>("HoverImageSource");
    public static readonly DependencyProperty ClickCommandProperty = DHelper.Register<ICommand, BaseCustomButton>("ClickCommand");
    public static readonly DependencyProperty StretchProperty = DHelper.Register<Stretch, BaseCustomButton>("Stretch");

    public BaseCustomButton()
    {
        IsEnabledChanged += CheckClick;
        IsVisibleChanged += CheckClick;
        ClickCommand = new ClickCommand<IPointerClickHandler>(this);
    }

    public Stretch Stretch
    {
        get => (Stretch) GetValue(StretchProperty);
        set => SetValueDp(StretchProperty, value);
    }

    public ICommand ClickCommand
    {
        get => (ICommand) GetValue(ClickCommandProperty);
        set => SetValueDp(ClickCommandProperty, value);
    }

    public SlicedButtonContentType ContentType
    {
        get => (SlicedButtonContentType) GetValue(ContentTypeProperty);
        set => SetValueDp(ContentTypeProperty, value);
    }

    public ImageSource ImageSource
    {
        get => (ImageSource) GetValue(ImageSourceProperty);
        set => SetValueDp(ImageSourceProperty, value);
    }

    public Transform ImageTransform
    {
        get => (Transform) GetValue(ImageTransformProperty);
        set => SetValueDp(ImageTransformProperty, value);
    }

    public ImageSource HoverImageSource
    {
        get => (ImageSource) GetValue(HoverImageSourceProperty);
        set => SetValueDp(HoverImageSourceProperty, value);
    }

    public event PropertyChangedEventHandler? PropertyChanged;

    public void OnPointerClick()
    {
        OnClick();
    }

    public bool CanClick()
    {
        return IsEnabled && Visibility == Visibility.Visible;
    }

    public event EventHandler? CanClickChanged;

    private void CheckClick(object sender, DependencyPropertyChangedEventArgs e)
    {
        CanClickChanged?.Invoke(this, EventArgs.Empty);
    }

    protected void SetValueDp(DependencyProperty property, object value, [CallerMemberName] string? p = null)
    {
        if (value == GetValue(property))
        {
            return;
        }

        SetValue(property, value);
        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(p));
    }
}