namespace HeroesCenter.View.Controls;

public partial class ResourceBox
{
    public static readonly DependencyProperty ResourceCountProperty = DependencyProperty
        .Register(nameof(ResourceCount), typeof(int), typeof(ResourceBox));

    public ResourceBox()
    {
        InitializeComponent();
    }

    public int ResourceCount
    {
        get => (int) GetValue(ResourceCountProperty);
        set
        {
            if (value != ResourceCount)
            {
                SetValue(ResourceCountProperty, value);
            }
        }
    }
}