namespace HeroesCenter.View.Controls;

/// <summary>
/// Interaction logic for UnitBox.xaml
/// </summary>
public partial class UnitBox
{
    public UnitBox()
    {
        InitializeComponent();
    }

    public static readonly DependencyProperty UnitProperty = Register<UnitViewModel, UnitBox>("Unit", null, OnUnitTypeChanged);
    public static readonly DependencyProperty UnitCountProperty = Register<int, UnitBox>("UnitCount", -1);
    public static readonly DependencyProperty UnitTypeProperty = Register<int, UnitBox>("UnitType", -1, OnUnitTypeChanged);

    public int UnitCount
    {
        get => (int) GetValue(UnitCountProperty);
        set
        {
            if (value == (int) GetValue(UnitCountProperty)) return;
            SetValue(UnitCountProperty, value);
        }
    }

    public int UnitType
    {
        get => (int) GetValue(UnitTypeProperty);
        set
        {
            if (value == (int) GetValue(UnitTypeProperty)) return;
            SetValue(UnitTypeProperty, value);
        }
    }

    public UnitViewModel Unit
    {
        get => (UnitViewModel) GetValue(UnitProperty);
        set
        {
            if (value == (UnitViewModel) GetValue(UnitProperty)) return;
            SetValue(UnitProperty, value);
        }
    }

    private static void OnUnitTypeChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
    {
        if (sender is UnitBox box)
        {
            box.LabelBorder.Visibility = box.UnitType < 0
                ? Visibility.Collapsed
                : Visibility.Visible;
        }
    }

    private void UnitBox_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
    {
        if (e.ChangedButton != MouseButton.Left) return;
        if (EditableLabel.IsMouseOver) return;

        DialogResult result = new UnitDialog(Unit).ShowDialog(Window.GetWindow(this));
        switch (result.Response)
        {
            case DialogResponse.Ok:
            {
                if (UnitType < 0) //Если раньше существа не было, выставляем кол-во 1
                {
                    UnitCount = 1;
                }

                UnitType = result.Type;
                break;
            }
            case DialogResponse.Delete:
            {
                UnitType = -1;
                UnitCount = 0; //После удаления существа выставляем его количество на 1
                break;
            }
            case DialogResponse.Cancel:
            {
                return;
            }
        }

        BindingExpression bindingExpression = GetBindingExpression(ImageSourceProperty);
        bindingExpression?.UpdateTarget();
    }

    private void UnitBox_OnMouseClick(object sender, MouseButtonEventArgs e)
    {
        if (EditableLabel.IsMouseOver) return;

        if (UnitType != -1)
        {
            var gameViewModel = GameViewModel.Instance;

            if (gameViewModel != null)
            {
                var viewModel = gameViewModel.CreatureManager[UnitType];
                new CreatureDialog(viewModel).ShowDialog(Window.GetWindow(this));
            }
        }
    }
}