namespace HeroesCenter.View.Controls;

/// <summary>
///     Interaction logic for BaseSkillBox.xaml
/// </summary>
public partial class SecondaryBox
{
    public static readonly DependencyProperty DescriptionProperty =
        DependencyProperty.Register(nameof(Description),
            typeof(string), typeof(SecondaryBox));

    public static readonly DependencyProperty DisplayValueProperty =
        DependencyProperty.Register(nameof(DisplayValue),
            typeof(string), typeof(SecondaryBox));

    public static readonly DependencyProperty StageProperty =
        DependencyProperty.Register(nameof(Stage),
            typeof(byte), typeof(SecondaryBox));

    public static readonly DependencyProperty BottomTextAlignmentProperty =
        DependencyProperty.Register(nameof(BottomTextAlignment),
            typeof(TextAlignment), typeof(SecondaryBox));

    public SecondaryBox()
    {
        InitializeComponent();
    }

    public string Description
    {
        get => (string) GetValue(DescriptionProperty);
        set => SetValueDp(DescriptionProperty, value);
    }

    public string DisplayValue
    {
        get => (string) GetValue(DisplayValueProperty);
        set => SetValueDp(DisplayValueProperty, value);
    }

    [UsedImplicitly]
    public byte Stage
    {
        get => (byte) GetValue(StageProperty);
        set
        {
            if (Stage != value)
            {
                SetValueDp(StageProperty, value);
            }
        }
    }

    public TextAlignment BottomTextAlignment
    {
        get => (TextAlignment) GetValue(BottomTextAlignmentProperty);
        set
        {
            if (value == (TextAlignment) GetValue(BottomTextAlignmentProperty))
            {
                return;
            }

            SetValue(BottomTextAlignmentProperty, value);
        }
    }

    private void SecondaryBox_OnMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
    {
        Stage++;
    }

    private void SecondaryBox_OnMouseRightButtonUp(object sender, MouseButtonEventArgs e)
    {
        Stage--;
    }
}