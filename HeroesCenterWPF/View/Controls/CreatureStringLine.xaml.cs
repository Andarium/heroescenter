namespace HeroesCenter.View.Controls;

/// <summary>
///     Логика взаимодействия для CreatureStatLine.xaml
/// </summary>
public partial class CreatureStringLine
{
    public static readonly DependencyProperty ValueProperty = Register<string, CreatureStringLine>("Value", "");
    public static readonly DependencyProperty DescriptionProperty = Register<string, CreatureStringLine>("Description", "");
    public static readonly DependencyProperty IsReadOnlyProperty = Register<bool, CreatureStringLine>("IsReadOnly");

    public CreatureStringLine()
    {
        InitializeComponent();
    }

    public string Value
    {
        get => (string) GetValue(ValueProperty);
        set => SetValue(ValueProperty, value);
    }

    public string Description
    {
        get => (string) GetValue(DescriptionProperty);
        set => SetValue(DescriptionProperty, value);
    }

    public bool IsReadOnly
    {
        get => (bool) GetValue(IsReadOnlyProperty);
        set
        {
            if (value != (bool) GetValue(IsReadOnlyProperty))
            {
                SetValue(IsReadOnlyProperty, value);
            }
        }
    }
}