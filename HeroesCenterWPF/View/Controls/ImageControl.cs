namespace HeroesCenter.View.Controls;

public class ImageControl : UserControl, INotifyPropertyChanged
{
    public static readonly DependencyProperty ImageSourceProperty = Register<ImageSource, ImageControl>("ImageSource");

    public ImageSource ImageSource
    {
        get => (ImageSource) GetValue(ImageSourceProperty);
        set => SetValueDp(ImageSourceProperty, value);
    }

    public event PropertyChangedEventHandler? PropertyChanged;

    protected void SetValueDp(DependencyProperty property, object value, [CallerMemberName] string? p = null)
    {
        if (value == GetValue(property))
        {
            return;
        }

        SetValue(property, value);
        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(p));
    }
}