namespace HeroesCenter.View.Controls;

public partial class ArmyPanel
{
    public ArmyPanel()
    {
        InitializeComponent();
        CreateUnitBoxes();
    }

    private void CreateUnitBoxes()
    {
        for (var i = 0; i < 7; i++)
        {
            var box = new UnitBox
            {
                Name = "UnitBox" + i,
                Margin = new Thickness(8 + 70 * i, 0, 0, 5)
            };

            String path = $"CurrentHero.Army[{i}]";

            var myBinding = new Binding(path);
            BindingOperations.SetBinding(box, UnitBox.UnitProperty, myBinding);

            myBinding = new Binding(path + ".Image");
            BindingOperations.SetBinding(box, ImageControl.ImageSourceProperty, myBinding);

            myBinding = new Binding(path + ".Type")
            {
                Mode = BindingMode.TwoWay
            };
            BindingOperations.SetBinding(box, UnitBox.UnitTypeProperty, myBinding);

            myBinding = new Binding(path + ".Count")
            {
                Mode = BindingMode.TwoWay
            };
            BindingOperations.SetBinding(box, UnitBox.UnitCountProperty, myBinding);

            GridPanel.Children.Add(box);
        }
    }
}