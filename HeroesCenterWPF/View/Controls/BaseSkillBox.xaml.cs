namespace HeroesCenter.View.Controls;

/// <summary>
/// Interaction logic for BaseSkillBox.xaml
/// </summary>
public partial class BaseSkillBox
{
    public BaseSkillBox()
    {
        InitializeComponent();
    }

    public static readonly DependencyProperty DescriptionProperty = Register<string, BaseSkillBox>("Description");

    public string Description
    {
        get => (string) GetValue(DescriptionProperty);
        set => SetValueDp(DescriptionProperty, value);
    }

    public static readonly DependencyProperty DisplayValueProperty = Register<string, BaseSkillBox>("DisplayValue");

    public string DisplayValue
    {
        get => (string) GetValue(DisplayValueProperty);
        set => SetValueDp(DisplayValueProperty, value);
    }

    public static readonly DependencyProperty ValueProperty = Register<string, BaseSkillBox>("Value", OnValueChanged);

    public string Value
    {
        get => (string) GetValue(ValueProperty);
        set => SetValueDp(ValueProperty, value);
    }

    public static readonly DependencyProperty IsReadOnlyProperty = Register<bool, BaseSkillBox>("IsReadOnly");

    public bool IsReadOnly
    {
        get => (bool) GetValue(IsReadOnlyProperty);
        set => SetValue(IsReadOnlyProperty, value);
    }

    public static readonly DependencyProperty BottomTextAlignmentProperty = Register<TextAlignment, BaseSkillBox>("BottomTextAlignment");

    public TextAlignment BottomTextAlignment
    {
        get => (TextAlignment) GetValue(BottomTextAlignmentProperty);
        set
        {
            if (value != (TextAlignment) GetValue(BottomTextAlignmentProperty))
            {
                SetValue(BottomTextAlignmentProperty, value);
            }
        }
    }

    private static void OnValueChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
    {
        if (sender is BaseSkillBox dObject)
        {
            dObject.GetBindingExpression(DisplayValueProperty)?.UpdateTarget();
        }
    }

    private void BaseSkillBox_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
    {
        if (e.ChangedButton == MouseButton.Left)
        {
            e.Handled = true;
            EditableLabel.StartEditing();
        }
    }
}