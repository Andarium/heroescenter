namespace HeroesCenter.View.Controls;

/// <summary>
/// Interaction logic for UnitBox.xaml
/// </summary>
public partial class SpellBox
{
    public SpellBox()
    {
        InitializeComponent();
    }

    // public static readonly DependencyProperty UnitProperty = Register<UnitViewModel, SpellBox>("Unit", null);
    public static readonly DependencyProperty IsLearnedProperty = Register<int, SpellBox>("IsLearned", -1);
    // public static readonly DependencyProperty SpellTypeProperty = Register<int, SpellBox>("SpellType", -1);

    public int IsLearned
    {
        get => (int) GetValue(IsLearnedProperty);
        set
        {
            if (value == (int) GetValue(IsLearnedProperty)) return;
            SetValue(IsLearnedProperty, value);
        }
    }

    /*public int SpellType
    {
        get => (int) GetValue(SpellTypeProperty);
        set
        {
            if (value == (int) GetValue(SpellTypeProperty)) return;
            SetValue(SpellTypeProperty, value);
        }
    }*/

    /*public UnitViewModel Unit
    {
        get => (UnitViewModel) GetValue(UnitProperty);
        set
        {
            if (value == (UnitViewModel) GetValue(UnitProperty)) return;
            SetValue(UnitProperty, value);
        }
    }*/

    /*private static void OnUnitTypeChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
    {
        if (sender is UnitBox box)
        {
            box.LabelBorder.Visibility = box.UnitType < 0
                ? Visibility.Collapsed
                : Visibility.Visible;
        }
    }*/

    /*private void UnitBox_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
    {
        if (e.ChangedButton != MouseButton.Left) return;
        if (EditableLabel.IsMouseOver) return;

        DialogResult result = new UnitDialog(Unit).ShowDialog(Window.GetWindow(this));
        switch (result.Response)
        {
            case DialogResponse.Ok:
            {
                if (SpellType < 0) //Если раньше существа не было, выставляем кол-во 1
                {
                    UnitCount = 1;
                }

                SpellType = result.Type;
                break;
            }
            case DialogResponse.Delete:
            {
                SpellType = -1;
                UnitCount = 0; //После удаления существа выставляем его количество на 1
                break;
            }
            case DialogResponse.Cancel:
            {
                return;
            }
        }

        BindingExpression bindingExpression = GetBindingExpression(ImageSourceProperty);
        bindingExpression?.UpdateTarget();
    }*/

    /*private void UnitBox_OnMouseClick(object sender, MouseButtonEventArgs e)
    {
        if (EditableLabel.IsMouseOver) return;

        if (SpellType != -1)
        {
            var gameViewModel = GameViewModel.Instance;

            if (gameViewModel != null)
            {
                var viewModel = gameViewModel.CreatureManager[SpellType];
                new CreatureDialog(viewModel).ShowDialog(Window.GetWindow(this));
            }
        }
    }*/
}