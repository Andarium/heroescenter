namespace HeroesCenter.View.Controls;

/// <summary>
///     Interaction logic for EditableLabel.xaml
/// </summary>
public partial class EditableLabel
{
    public static readonly DependencyProperty DisplayValueProperty = Register<string, EditableLabel>("DisplayValue");

    public static readonly DependencyProperty ValueProperty = Register<string, EditableLabel>("Value", OnValueChanged);

    public static readonly DependencyProperty TextAlignmentProperty = Register<TextAlignment, EditableLabel>("TextAlignment");

    public static readonly DependencyProperty IsReadOnlyProperty = Register<bool, EditableLabel>("IsReadOnly");

    private string _backupValue = "";

    public EditableLabel()
    {
        InitializeComponent();
    }

    public string DisplayValue
    {
        get => (string) GetValue(DisplayValueProperty);
        set
        {
            if (value != (string) GetValue(DisplayValueProperty))
            {
                SetValue(DisplayValueProperty, value);
            }
        }
    }

    public string Value
    {
        get => (string) GetValue(ValueProperty);
        set
        {
            if (value != (string) GetValue(ValueProperty))
            {
                SetValue(ValueProperty, value);
            }
        }
    }

    public TextAlignment TextAlignment
    {
        get => (TextAlignment) GetValue(TextAlignmentProperty);
        set
        {
            if (value != (TextAlignment) GetValue(TextAlignmentProperty))
            {
                SetValue(TextAlignmentProperty, value);
            }
        }
    }

    public bool IsReadOnly
    {
        get => (bool) GetValue(IsReadOnlyProperty);
        set
        {
            if (value != (bool) GetValue(IsReadOnlyProperty))
            {
                SetValue(IsReadOnlyProperty, value);
            }
        }
    }

    private static void OnValueChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
    {
        if (sender is EditableLabel dObject)
        {
            dObject.GetBindingExpression(DisplayValueProperty)?.UpdateTarget();
        }
    }

    private void SendCaretToEnd()
    {
        TextBox.CaretIndex = int.MaxValue;
    }

    private void EditableLabel_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
    {
        if (e.ChangedButton != MouseButton.Left)
        {
            return;
        }

        if (IsReadOnly)
        {
            return;
        }

        e.Handled = true; //Событие не передастся родителю, добавлено для решение проблемы потери фокуса, если у родителя не выставлен параметр Focusable=false
        StartEditing();
    }

    private void TextBox_OnKeyUp(object sender, KeyEventArgs e)
    {
        if (e.Key != Key.Enter && e.Key != Key.Escape)
        {
            return;
        }

        ExitEditing(e.Key == Key.Escape);
    }

    public void ExitEditing(bool revertChanges = false)
    {
        TextBox.Opacity = 0; //Делаем текстовое поле прозрачным
        TextBox.IsEnabled = false; //Отключаем его для предотвращения изменений
        Keyboard.ClearFocus();
        TextBlock.Opacity = 1;
        if (revertChanges)
        {
            Value = _backupValue; //Отменяем последние изменения
        }
    }

    public void StartEditing()
    {
        if (IsReadOnly)
        {
            return;
        }

        _backupValue = Value; //Запоминаем значение, что восстановить его в случае отмены изменений
        TextBlock.Opacity = 0;
        TextBox.Opacity = 1; //Делаем текстовое поле видимым
        TextBox.IsEnabled = true; //Включаем для возможности редактирования
        TextBox.Focus(); //Фокус, чтобы сразу можно было вводить текст
        TextBox.CaretIndex = int.MaxValue;
    }

    private void TextBox_OnLostFocus(object sender, RoutedEventArgs e)
    {
        ExitEditing();
    }

    private void TextBox_OnTextChanged(object sender, TextChangedEventArgs e)
    {
        if (TextBox.Text == "0")
        {
            SendCaretToEnd();
        }
    }
}