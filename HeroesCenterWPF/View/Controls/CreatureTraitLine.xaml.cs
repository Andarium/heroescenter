namespace HeroesCenter.View.Controls;

/// <summary>
///     Логика взаимодействия для CreatureStatLine.xaml
/// </summary>
public partial class CreatureTraitLine
{
    public static readonly DependencyProperty ValueProperty = Register<bool, CreatureTraitLine>("Value");
    public static readonly DependencyProperty DescriptionProperty = Register<string, CreatureTraitLine>("Description", "");

    public CreatureTraitLine()
    {
        InitializeComponent();
    }

    public bool Value
    {
        get => (bool) GetValue(ValueProperty);
        set => SetValue(ValueProperty, value);
    }

    public string Description
    {
        get => (string) GetValue(DescriptionProperty);
        set => SetValue(DescriptionProperty, value);
    }
}