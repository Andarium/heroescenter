namespace HeroesCenter.View.Controls;

public interface IPointerClickHandler
{
    void OnPointerClick();
    bool CanClick();
    event EventHandler CanClickChanged;
}