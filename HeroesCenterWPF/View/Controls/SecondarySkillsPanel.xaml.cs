namespace HeroesCenter.View.Controls;

public partial class SecondarySkillsPanel
{
    public SecondarySkillsPanel()
    {
        InitializeComponent();
        CreateSecondaryBoxes();
    }

    private void CreateSecondaryBoxes()
    {
        for (var i = 0; i < 29; i++)
        {
            bool interference = i == 28;

            string path = interference
                ? "CurrentHero.SecondarySkills.InterferenceSafe"
                : $"CurrentHero.SecondarySkills[{i}]";

            var box = new SecondaryBox();
            SetBinding(SecondaryBox.StageProperty, ".Stage", BindingMode.TwoWay);
            SetBinding(SecondaryBox.DescriptionProperty, ".SkillName");
            SetBinding(SecondaryBox.DisplayValueProperty, ".StageName");
            SetBinding(ImageControl.ImageSourceProperty, ".Image");

            if (interference)
            {
                var binding = new Binding(path) { Converter = new NullToVisibilityConverter() };
                BindingOperations.SetBinding(box, VisibilityProperty, binding);
            }

            SecondarySkillsGrid.Children.Add(box);

            void SetBinding(DependencyProperty property, string subPath, BindingMode mode = BindingMode.OneWay)
            {
                var binding = new Binding(path + subPath) { Mode = mode };
                BindingOperations.SetBinding(box, property, binding);
            }
        }
    }
}