namespace HeroesCenter.View.Controls;

public partial class HeroPanel
{
    private Vector3? _savedPos;

    private GameViewModel? GameViewModel => DataContext as GameViewModel;

    public HeroPanel()
    {
        InitializeComponent();
    }

    private void OnSavePosClick(object sender, RoutedEventArgs e)
    {
        if (GameViewModel is not { CurrentHero: { } hero })
        {
            return;
        }

        if (_savedPos == null)
        {
            _savedPos = new Vector3(hero.X, hero.Y, hero.Z);
        }
        else
        {
            _savedPos.X = hero.X;
            _savedPos.Y = hero.Y;
            _savedPos.Z = hero.Z;
        }
    }

    private void OnLoadPosClick(object sender, RoutedEventArgs e)
    {
        if (GameViewModel is not { CurrentHero: { } hero })
        {
            return;
        }

        if (_savedPos != null)
        {
            hero.X = _savedPos.X;
            hero.Y = _savedPos.Y;
            hero.Z = _savedPos.Z;
        }
    }

    private void ButtonRemoveAllSeals_OnClick(object sender, RoutedEventArgs e)
    {
        if (GameViewModel is not { CurrentHero: { } hero })
        {
            return;
        }

        hero.Artifacts.RemoveAllSeals();
    }

    private class Vector3
    {
        public short X;
        public short Y;
        public short Z;

        public Vector3(short x, short y, short z)
        {
            X = x;
            Y = y;
            Z = z;
        }
    }
}