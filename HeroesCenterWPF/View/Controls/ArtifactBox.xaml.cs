namespace HeroesCenter.View.Controls;

public partial class ArtifactBox
{
    public ArtifactBox()
    {
        InitializeComponent();
    }

    private ArtifactViewModel? ViewModel => DataContext as ArtifactViewModel;

    private void ArtifactBox_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
    {
        if (e.ChangedButton != MouseButton.Left)
        {
            return;
        }

        if (ViewModel is not { } viewModel)
        {
            return;
        }

        var dialog = new ArtifactDialog(viewModel);
        dialog.ShowDialog(Window.GetWindow(this)!);

        DialogResult result = dialog.Result;

        if (result.Response == DialogResponse.Cancel)
        {
            return;
        }

        if (result.Response == DialogResponse.Delete)
        {
            viewModel.SetValues(-1, -1);
        }
        else
        {
            viewModel.SetValues(result.Type, result.Data);
        }
    }

    protected override void Swap(SwappableControl first, SwappableControl second)
    {
        if (first is not ArtifactBox fBox || second is not ArtifactBox sBox)
        {
            return;
        }

        if (fBox.ViewModel is not {} f || sBox.ViewModel is not {} s)
        {
            return;
        }

        f.Swap(s);
    }
}