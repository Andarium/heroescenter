namespace HeroesCenter.View.Controls;

public partial class PrimaryBox
{
    public PrimaryBox()
    {
        InitializeComponent();
    }

    public static readonly DependencyProperty DescriptionProperty =
        DependencyProperty.Register(nameof(Description),
            typeof(string), typeof(PrimaryBox));

    public string Description
    {
        get => (string) GetValue(DescriptionProperty);
        set => SetValueDp(DescriptionProperty, value);
    }

    public static readonly DependencyProperty ValueProperty =
        DependencyProperty.Register(nameof(Value),
            typeof(string), typeof(PrimaryBox));

    public string Value
    {
        get => (string) GetValue(ValueProperty);
        set => SetValueDp(ValueProperty, value);
    }

    public static readonly DependencyProperty DisplayValueProperty =
        DependencyProperty.Register(nameof(DisplayValue),
            typeof(string), typeof(PrimaryBox));

    public string DisplayValue
    {
        get => (string) GetValue(DisplayValueProperty);
        set => SetValueDp(DisplayValueProperty, value);
    }
}