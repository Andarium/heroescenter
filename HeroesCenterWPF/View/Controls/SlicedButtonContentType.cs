namespace HeroesCenter.View.Controls;

public enum SlicedButtonContentType
{
    Image,
    Text
}