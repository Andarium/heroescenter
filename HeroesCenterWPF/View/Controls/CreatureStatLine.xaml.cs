namespace HeroesCenter.View.Controls;

/// <summary>
///     Логика взаимодействия для CreatureStatLine.xaml
/// </summary>
public partial class CreatureStatLine
{
    public static readonly DependencyProperty ValueProperty = Register<int, CreatureStatLine>("Value", -1);
    public static readonly DependencyProperty DescriptionProperty = Register<string, CreatureStatLine>("Description", "");

    public CreatureStatLine()
    {
        InitializeComponent();
    }

    public int Value
    {
        get => (int) GetValue(ValueProperty);
        set => SetValue(ValueProperty, value);
    }

    public string Description
    {
        get => (string) GetValue(DescriptionProperty);
        set => SetValue(DescriptionProperty, value);
    }
}