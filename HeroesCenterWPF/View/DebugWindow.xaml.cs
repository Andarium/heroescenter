namespace HeroesCenter.View;

/// <summary>
///     Interaction logic for DebugWindow.xaml
/// </summary>
public partial class DebugWindow : Window
{
    private readonly GameVersion _version;

    public DebugWindow()
    {
        InitializeComponent();
        var game = GameViewModel.Instance!;
        _version = game.Version;

        AddMap(typeof(CreatureData), MemoryWorkerType.Creatures);
        AddLine();
        AddMap(typeof(GameData), MemoryWorkerType.Game);
        AddLine();
        AddMap(typeof(UIData), MemoryWorkerType.UI);
        AddLine();
        AddMap(typeof(MapData), MemoryWorkerType.Map);
        AddLine();
        AddMap(typeof(PlayerData), MemoryWorkerType.Player);
        AddLine();
        AddMap(typeof(HeroStats), MemoryWorkerType.Hero);
        AddLine();
        AddMap(typeof(BattleData), MemoryWorkerType.Battle);
    }

    private void AddMap(Type c, MemoryWorkerType type)
    {
        AddLine(c.Name);
        var map = GetAddressMap(c, MemoryWorker.GetInstance(_version, type));

        foreach (var pair in map)
        {
            AddLine(pair.Key, pair.Value);
        }
    }

    private void AddLine(string name = "", string content = "")
    {
        var line = new StackPanel
        {
            Orientation = Orientation.Horizontal
        };
        line.Children.Add(new TextBlock { Text = name, Width = 200 });
        line.Children.Add(new TextBox { Text = content, IsReadOnly = true, BorderBrush = null });

        Panel.Children.Add(line);
    }

    private static Dictionary<string, string> GetAddressMap(Type type, MemoryWorker worker)
    {
        var varType = typeof(MemoryVariable);
        var fields = type
            .GetFields()
            .Where(x => x.FieldType == varType)
            .ToList();

        var map = fields.ToDictionary(x => x.Name, y => y.GetValue(null) as MemoryVariable);
        return map
            .ToDictionary(x => x.Key, y => worker.GetBlockStartAddress(0, y.Value!.Offset));
    }

    /*public static Int32 SummarySize
    {
        get
        {
            return typeof(HeroStats)
                .GetFields()
                .Select(field => field.GetValue(typeof(HeroStats)))
                .Cast<MemoryVariable>()
                .Select(x => x.Size)
                .Sum();
        }
    }*/
}