namespace HeroesCenter.View.Dialogs;

public enum DialogResponse
{
    Ok,
    Cancel,
    Delete
}