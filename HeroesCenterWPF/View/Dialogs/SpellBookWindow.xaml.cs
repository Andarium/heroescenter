namespace HeroesCenter.View.Dialogs;

public sealed partial class SpellBookWindow : BaseThemedWindow
{
    private SpellBookWindow()
    {
        InitializeComponent();
    }

    private void OnOkClick(object _, RoutedEventArgs __)
    {
        Close();
    }

    public static SpellBookWindow ShowDialog(Window parent, GameViewModel? model)
    {
        var newWindow = new SpellBookWindow
        {
            Owner = parent,
            DataContext = model
        };
        newWindow.ShowDialog();
        return newWindow;
    }

    private void SpellBookWindow_OnActivated(object? sender, EventArgs e)
    {
        this.ReloadDataContext();
    }
}