namespace HeroesCenter.View.Dialogs;

public sealed partial class BackpackWindow : BaseThemedWindow
{
    private BackpackWindow()
    {
        InitializeComponent();
    }

    private void OnOkClick(object _, RoutedEventArgs __)
    {
        Close();
    }

    private void OnSortClick(object _, RoutedEventArgs __)
    {
        if (DataContext is GameViewModel { CurrentHero: { } hero } game)
        {
            hero.Artifacts.SortBackpack();
        }
    }

    public static BackpackWindow ShowDialog(Window parent, GameViewModel? model)
    {
        var newWindow = new BackpackWindow
        {
            Owner = parent,
            DataContext = model
        };
        newWindow.ShowDialog();
        return newWindow;
    }

    private void BackpackWindow_OnActivated(object? sender, EventArgs e)
    {
        this.ReloadDataContext();
    }
}