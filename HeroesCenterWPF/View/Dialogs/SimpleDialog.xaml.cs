namespace HeroesCenter.View.Dialogs;

public sealed partial class SimpleDialog
{
    private SimpleDialog(string message, DialogButtons buttons = DialogButtons.Ok)
    {
        InitializeComponent();

        Message.Text = message;

        switch (buttons)
        {
            case DialogButtons.Ok:
            {
                CancelButton.IsCancel = false;
                CancelButton.Visibility = Visibility.Collapsed;
                break;
            }
            case DialogButtons.OkCancel:
            case DialogButtons.OkCancelDelete:
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(buttons), buttons, null);
        }

        Response = DialogResponse.Cancel;
    }

    public static DialogResponse Show(Window? owner, string message, DialogButtons buttons = DialogButtons.Ok)
    {
        var dialog = new SimpleDialog(message, buttons);
        dialog.ShowDialog(owner);
        return dialog.Response;
    }

    private void OnOkClick(object sender, RoutedEventArgs routedEventArgs)
    {
        Response = DialogResponse.Ok;
        Close();
    }

    private void OnCancelClick(object sender, RoutedEventArgs e)
    {
        Response = DialogResponse.Cancel;
        Close();
    }

    protected override void OnCloseButtonClick(object sender, RoutedEventArgs e)
    {
        OnCancelClick(sender, e);
    }
}