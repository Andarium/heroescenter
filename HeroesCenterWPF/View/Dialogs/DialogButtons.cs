namespace HeroesCenter.View.Dialogs;

public enum DialogButtons
{
    Ok,
    OkCancel,
    OkCancelDelete
}