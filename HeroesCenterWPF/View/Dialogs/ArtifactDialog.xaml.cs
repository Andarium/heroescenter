using static HeroesCenter.Data.ArtifactCategory;

namespace HeroesCenter.View.Dialogs;

public sealed partial class ArtifactDialog
{
    private readonly GameVersion _gameVersion;
    private readonly ObservableCollection<ArtifactInfo> _artifacts;
    private int _categoryIndex;

    private readonly List<ArtifactCategory> _categories = new()
    {
        Head,
        Neck,
        RightArm,
        LeftArm,
        Body,
        Rings,
        Shoulders,
        Legs,
        Slots,
        Other,
        Scroll
    };

    public ArtifactDialog(ArtifactViewModel artifact) : this(
        artifact.Version,
        artifact.Type,
        artifact.Data,
        artifact.Category
    )
    {
    }

    public ArtifactDialog(GameVersion gameVersion, int artifactIndex, int artifactData, ArtifactCategory fallBack)
    {
        InitializeComponent();
        _gameVersion = gameVersion;
        _artifacts = new ObservableCollection<ArtifactInfo>();
        Response = DialogResponse.Cancel;
        ListBox.ItemsSource = _artifacts;

        ArtifactCategory category;
        if (artifactIndex < 0)
        {
            ButtonDelete.IsEnabled = false;
            category = fallBack;
            Select(-1, -1);
        }
        else
        {
            category = ArtifactHelper.GetArtifactInfo(artifactIndex, gameVersion).Category;
            Select(artifactIndex, artifactData);
        }

        CurrentCategoryIndex = _categories.FindIndex(x => x.HasFlag(category));
    }

    private void Select(int type, int data)
    {
        SetResult(type, data);
        OnPropertyChanged(string.Empty);
    }

    private ArtifactInfo Info => ArtifactHelper.GetArtifactInfo(ResultType, Data, _gameVersion);

    public BitmapSource SelectedArtifactImage => Info.Image;

    public string SelectedArtifactName => Info.DisplayName;

    private int CurrentCategoryIndex
    {
        get => _categoryIndex;
        set
        {
            if (value < 0 || value >= _categories.Count) return;
            _categoryIndex = value;
            ButtonLeft.IsEnabled = value > 0;
            ButtonRight.IsEnabled = value < _categories.Count - 1;
            FillList(_categories[_categoryIndex]);
        }
    }

    public void FillList(ArtifactCategory category)
    {
        _artifacts.Clear();

        IList<ArtifactInfo> artifacts = ArtifactHelper.GetArtifactsByCategory(category, _gameVersion);

        foreach (ArtifactInfo artifact in artifacts)
        {
            _artifacts.Add(artifact);
        }

        ListBox.SelectedItem = FindSelectedItem();
    }

    private ArtifactInfo? FindSelectedItem()
    {
        return ListBox.Items
            .Cast<ArtifactInfo>()
            .FirstOrDefault(x => x.Type == ResultType && x.Data == Data);
    }

    private void ButtonLeft_OnMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
    {
        if (Keyboard.Modifiers.HasFlag(ModifierKeys.Shift))
        {
            CurrentCategoryIndex = 0;
        }
        else
        {
            CurrentCategoryIndex--;
        }
    }

    private void ButtonRight_OnMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
    {
        if (Keyboard.Modifiers.HasFlag(ModifierKeys.Shift))
        {
            CurrentCategoryIndex = _categories.Count - 1;
        }
        else
        {
            CurrentCategoryIndex++;
        }
    }

    private void ListBox_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
    {
        if (ListBox.SelectedIndex != -1)
        {
            var selected = (ArtifactInfo) ListBox.SelectedItem;

            ListBox.ScrollIntoView(selected);
            Select(selected.Type, selected.Data);
            Response = DialogResponse.Ok;
        }
    }

    private void OnDeleteClick(object sender, RoutedEventArgs routedEventArgs)
    {
        if (SimpleDialog.Show(this, $"Удалить артефакт{Environment.NewLine}'{SelectedArtifactName}'?", DialogButtons.OkCancel) == DialogResponse.Ok)
        {
            Response = DialogResponse.Delete;
            Close();
        }
    }

    private void OnOkClick(object sender, RoutedEventArgs routedEventArgs)
    {
        Response = DialogResponse.Ok;
        Close();
    }

    private void OnCancelClick(object sender, RoutedEventArgs routedEventArgs)
    {
        Response = DialogResponse.Cancel;
        Close();
    }

    protected override void OnCloseButtonClick(object sender, RoutedEventArgs e)
    {
        OnCancelClick(sender, e);
    }
}