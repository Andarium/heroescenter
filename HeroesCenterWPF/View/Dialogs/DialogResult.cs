namespace HeroesCenter.View.Dialogs;

public struct DialogResult
{
    public DialogResponse Response { get; set; }
    public int Type { get; set; }
    public int Data { get; set; }
}