namespace HeroesCenter.View.Dialogs;

public class DialogWindow : BaseThemedWindow
{
    private DialogResult _result;

    public DialogResult Result => _result;

    protected int ResultType => _result.Type;

    protected int Data => _result.Data;

    protected void SetResult(int resultType, int data = -1)
    {
        _result.Type = resultType;
        _result.Data = data;
    }

    protected void ResetResult()
    {
        SetResult(-1);
    }

    protected DialogResponse Response
    {
        get => Result.Response;
        set
        {
            if (_result.Response != value)
            {
                _result.Response = value;
                OnPropertyChanged(string.Empty);
            }
        }
    }

    public DialogResult ShowDialog(Window? owner)
    {
        Owner = owner;
        ShowDialog();
        return Result;
    }
}