namespace HeroesCenter.View.Dialogs;

public sealed partial class UnitDialog
{
    private readonly ObservableCollection<CreatureViewModel> _creatures;
    private int _townIndex;

    private readonly GameVersion _gameVersion;
    private readonly List<TownType> _availableTowns;
    private readonly CreatureManager _creatureManager;

    public UnitDialog(UnitViewModel unit)
    {
        var gameViewModel = GameViewModel.Instance ?? throw new InvalidOperationException("GameViewModel instance is null");
        _gameVersion = gameViewModel.Version;
        _creatureManager = gameViewModel.CreatureManager;
        _creatures = new ObservableCollection<CreatureViewModel>();
        _availableTowns = Enum.GetValues(typeof(TownType))
            .Cast<TownType>()
            .Where(x => _gameVersion == GameVersion.HornOfTheAbyss || x != TownType.Cove)
            .OrderBy(x => x == TownType.None)
            .ThenBy(x => x)
            .ToList();

        InitializeComponent();

        var creature = _creatureManager.GetOrDefault(unit.Type);
        var town = creature?.Town ?? TownType.Castle;
        SetResult(unit.Type);
        Response = DialogResponse.Cancel;
        ListBox.ItemsSource = _creatures;
        CurrentTownIndex = _availableTowns.IndexOf(town);
    }

    public ImageSource SelectedUnitImage => ImageHelper.GetImage(_gameVersion, ImageType.Unit, ResultType);

    public string SelectedUnitName => _creatureManager.GetOrDefault(ResultType)?.SingleName ?? string.Empty;

    private int CurrentTownIndex
    {
        get => _townIndex;
        set
        {
            if (value >= 0 && value < _availableTowns.Count)
            {
                _townIndex = value;
                ButtonLeft.IsEnabled = value > 0;
                ButtonRight.IsEnabled = value < _availableTowns.Count - 1;
                FillList(_availableTowns[value]);
            }
        }
    }

    public void FillList(TownType town)
    {
        _creatures.Clear();
        TownBrush.ImageSource = ImageHelper.GetTownImage(town);

        foreach (var creature in _creatureManager.GetTownUnits(town, true))
        {
            _creatures.Add(creature);
        }

        var selected = _creatures.FirstOrDefault(x => x.UnitType == ResultType);
        ListBox.SelectedItem = selected;
    }

    private void ButtonLeft_OnMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
    {
        if (Keyboard.Modifiers.HasFlag(ModifierKeys.Shift))
        {
            CurrentTownIndex = 0;
        }
        else
        {
            CurrentTownIndex--;
        }
    }

    private void ButtonRight_OnMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
    {
        if (Keyboard.Modifiers.HasFlag(ModifierKeys.Shift))
        {
            CurrentTownIndex = _availableTowns.Count - 1;
        }
        else
        {
            CurrentTownIndex++;
        }
    }

    private void ListBox_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
    {
        if (ListBox.SelectedIndex != -1)
        {
            Response = DialogResponse.Ok;
            var item = (CreatureViewModel) ListBox.SelectedItem;
            SetResult(item.UnitType);
            ListBox.ScrollIntoView(ListBox.SelectedItem);
        }
    }

    private void OnDeleteClick(object sender, RoutedEventArgs routedEventArgs)
    {
        if (SimpleDialog.Show(this, $"Remove unit{Environment.NewLine}'{SelectedUnitName}'?", DialogButtons.OkCancel) == DialogResponse.Ok)
        {
            Response = DialogResponse.Delete;
            ResetResult();
            ListBox.SelectedIndex = -1;
            Close();
        }
    }

    private void OnOkClick(object sender, RoutedEventArgs routedEventArgs)
    {
        Response = DialogResponse.Ok;
        Close();
    }

    private void OnCancelClick(object sender, RoutedEventArgs e)
    {
        Response = DialogResponse.Cancel;
        Close();
    }

    protected override void OnCloseButtonClick(object sender, RoutedEventArgs e)
    {
        OnCancelClick(sender, e);
    }
}