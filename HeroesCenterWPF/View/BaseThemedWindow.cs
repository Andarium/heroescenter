namespace HeroesCenter.View;

public abstract class BaseThemedWindow : Window, INotifyPropertyChanged
{
    public static readonly DependencyProperty ShowMaximizeButtonProperty = Register<bool, BaseThemedWindow>(nameof(ShowMaximizeButton));
    public static readonly DependencyProperty ShowMinimizeButtonProperty = Register<bool, BaseThemedWindow>(nameof(ShowMinimizeButton));
    public static readonly DependencyProperty TitleHeightProperty = Register<GridLength, BaseThemedWindow>(nameof(TitleHeight), new GridLength(30));

    private FrameworkElement? _caption;
    private ButtonBase? _minimizeButton;
    private ButtonBase? _maximizeButton;
    private ButtonBase? _closeButton;
    private BorderHandler? _borderHandler;

    protected BaseThemedWindow()
    {
        SourceInitialized += (_, _) =>
        {
            var handle = new WindowInteropHelper(this).Handle;
            var hwndSource = HwndSource.FromHwnd(handle);
            hwndSource?.AddHook(WndProc);
        };

        this.Loaded += OnLoaded;
    }

    private void OnLoaded(object sender, RoutedEventArgs e)
    {
        RegisterBorders();
        RegisterCaption();
        RegisterMinimizeButton();
        RegisterMaximizeButton();
        RegisterCloseButton();
    }

    public new Window? Owner
    {
        get => base.Owner;
        set
        {
            if (Equals(value, base.Owner)) return;
            base.Owner = value;
            OnPropertyChanged();
            OnPropertyChanged(nameof(ThemedOwner));
        }
    }

    public BaseThemedWindow? ThemedOwner => Owner as BaseThemedWindow;

    public bool ShowMaximizeButton
    {
        get => (bool) GetValue(ShowMaximizeButtonProperty);
        set => SetValue(ShowMaximizeButtonProperty, value);
    }

    public bool ShowMinimizeButton
    {
        get => (bool) GetValue(ShowMinimizeButtonProperty);
        set => SetValue(ShowMinimizeButtonProperty, value);
    }

    public GridLength TitleHeight
    {
        get => (GridLength) GetValue(TitleHeightProperty);
        set => SetValue(TitleHeightProperty, value);
    }

    private T GetTemplateChild<T>(string name) where T : class
    {
        return (GetTemplateChild(name) as T)!;
    }

    private void RegisterCloseButton()
    {
        _closeButton = GetTemplateChild<ButtonBase>("PART_WindowCaptionCloseButton");

        if (_closeButton != null)
        {
            _closeButton.Click += OnCloseButtonClick;
        }
    }

    protected virtual void OnCloseButtonClick(object sender, RoutedEventArgs e)
    {
        Close();
    }

    private void RegisterMaximizeButton()
    {
        _maximizeButton = GetTemplateChild<ButtonBase>("PART_WindowCaptionMaximizeButton");

        if (_maximizeButton != null)
        {
            _maximizeButton.Click += OnMaximizeButtonClick;
        }
    }

    protected virtual void OnMaximizeButtonClick(object sender, RoutedEventArgs e)
    {
        WindowState = WindowState == WindowState.Normal ? WindowState.Maximized : WindowState.Normal;
    }

    private void RegisterMinimizeButton()
    {
        _minimizeButton = GetTemplateChild<ButtonBase>("PART_WindowCaptionMinimizeButton");

        if (_minimizeButton != null)
        {
            _minimizeButton.Click += OnMinimizeButtonClick;
        }
    }

    protected virtual void OnMinimizeButtonClick(object sender, RoutedEventArgs e)
    {
        WindowState = WindowState.Minimized;
    }

    private void RegisterBorders()
    {
        _borderHandler = new BorderHandler(this, new Thickness(15));
    }

    private void RegisterCaption()
    {
        _caption = GetTemplateChild<FrameworkElement>("PART_WindowCaption");

        if (_caption == null)
        {
            return;
        }

        _caption.MouseLeftButtonDown += (sender, e) =>
        {
            //_cursorRestoreTop = e.GetPosition(this).Y;
            //_cursorRestoreLeft = e.GetPosition(this).X; //Added
            if (e.ClickCount == 2 && e.ChangedButton == MouseButton.Left && ResizeMode == ResizeMode.CanResize)
                //(ResizeMode != ResizeMode.CanMinimize && ResizeMode != ResizeMode.NoResize)
            {
                WindowState = WindowState == WindowState.Maximized ? WindowState.Normal : WindowState.Maximized;
                return;
            }

            //DragMove();
            MouseHelper.DragWindow(this, new WindowInteropHelper(this).Handle, e);
        };

        //Нужно для того, чтобы "сдергивать" окно из maximized режима мышкой (за PART_WindowCaption)
        /*_caption.MouseMove += (sender, e) =>
        {
            if (e.LeftButton != MouseButtonState.Pressed || !_caption.IsMouseOver) return;
            if (WindowState != WindowState.Maximized) return;
            /*WindowState = WindowState.Normal;
            Top = _cursorRestoreTop - 10;
            //if(_cursorRestoreLeft > Width/2)
            Left = _cursorRestoreLeft - Width/2;
                    //Math.Abs(_cursorRestoreLeft - (_cursorRestoreLeft / SystemParameters.WorkArea.Width) * Width);
            DragMove();
            //MouseHelper.DragWindow(this, new WindowInteropHelper(this).Handle, e);
        };*/
    }

    private static void WmGetMinMaxInfo(IntPtr hwnd, IntPtr lParam)
    {
        object? ptr = Marshal.PtrToStructure(lParam, typeof(MinMaxInfo));

        if (ptr == null)
        {
            throw new NullReferenceException($"Pointer to {lParam} is null!");
        }

        var minMaxInfo = (MinMaxInfo) ptr;

        const int monitorDefaultToNearest = 0x00000002;

        IntPtr monitor = NativeMethods.MonitorFromWindow(hwnd, monitorDefaultToNearest);

        if (monitor != IntPtr.Zero)
        {
            var monitorInfo = new MonitorInfo();
            NativeMethods.GetMonitorInfo(monitor, monitorInfo);

            var rcWorkArea = monitorInfo.rcWork;
            var rcMonitorArea = monitorInfo.rcMonitor;

            int x = Math.Abs(rcWorkArea.left - rcMonitorArea.left);
            int y = Math.Abs(rcWorkArea.top - rcMonitorArea.top);
            minMaxInfo.ptMaxPosition = new Native.Point(x, y);

            x = Math.Abs(rcWorkArea.right - rcWorkArea.left);
            y = Math.Abs(rcWorkArea.bottom - rcWorkArea.top);
            var maxSize = new Native.Point(x, y);
            minMaxInfo.ptMaxSize = maxSize;
        }

        Marshal.StructureToPtr(minMaxInfo, lParam, true);
    }

    private static IntPtr WndProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
    {
        switch (msg)
        {
            case 0x0024:
                WmGetMinMaxInfo(hwnd, lParam);
                handled = true;
                break;
        }

        return IntPtr.Zero;
    }

    public event PropertyChangedEventHandler? PropertyChanged;

    [NotifyPropertyChangedInvocator]
    protected virtual void OnPropertyChanged([CallerMemberName] string? propertyName = null)
    {
        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }
}