using System.Diagnostics.CodeAnalysis;

namespace HeroesCenter.Core;

public class ConcurrentDictionary<TKey1, TKey2, TValue> : ConcurrentDictionary<TKey1, ConcurrentDictionary<TKey2, TValue>>
    where TKey1 : notnull
    where TKey2 : notnull
{
    public TValue this[TKey1 key1, TKey2 key2]
    {
        get => this[key1][key2];
        set
        {
            if (!TryGetValue(key1, out var subDict))
            {
                subDict = new ConcurrentDictionary<TKey2, TValue>();
                this[key1] = subDict;
            }

            subDict[key2] = value;
        }
    }

    public bool TryGetValue(TKey1 key1, TKey2 key2, [MaybeNullWhen(false)] out TValue value)
    {
        if (TryGetValue(key1, out var subDict))
        {
            if (subDict.TryGetValue(key2, out var val))
            {
                value = val;
                return true;
            }
        }

        value = default;
        return false;
    }

    public bool TryRemove(TKey1 key1, TKey2 key2, [MaybeNullWhen(false)] out TValue value)
    {
        if (TryGetValue(key1, out var subDict))
        {
            if (subDict.TryRemove(key2, out var val))
            {
                value = val;
                return true;
            }
        }

        value = default;
        return false;
    }

    public ConcurrentDictionary()
    {
    }

    public ConcurrentDictionary(IEnumerable<KeyValuePair<TKey1, IEnumerable<KeyValuePair<TKey2, TValue>>>> collection)
        : base(collection.ToDictionary(
            x => x.Key,
            y => new ConcurrentDictionary<TKey2, TValue>(y.Value)
        ))
    {
    }

    public ConcurrentDictionary(IEnumerable<KeyValuePair<TKey1, IDictionary<TKey2, TValue>>> collection)
        : base(collection.ToDictionary(
            x => x.Key,
            y => new ConcurrentDictionary<TKey2, TValue>(y.Value)
        ))
    {
    }

    public ConcurrentDictionary(IEnumerable<KeyValuePair<TKey1, Dictionary<TKey2, TValue>>> collection)
        : base(collection.ToDictionary(
            x => x.Key,
            y => new ConcurrentDictionary<TKey2, TValue>(y.Value)
        ))
    {
    }
}