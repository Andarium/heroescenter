namespace HeroesCenter.Core;

public class SpecializationFileMap : IVersion
{
    [JsonProperty("Version", Order = 0, Required = Required.Always)]
    public GameVersion Version { get; }

    [JsonProperty("BasedOn", Order = 1, DefaultValueHandling = DefaultValueHandling.Ignore, NullValueHandling = NullValueHandling.Ignore)]
    public readonly GameVersion? BasedOn;

    [JsonProperty("Map", Order = 4, ItemConverterType = typeof(SpecializationDataConverter))]
    private readonly List<SpecializationData> _map;

    private readonly Dictionary<int, SpecializationData> _resolved = new();

    [JsonIgnore]
    private bool IsResolved { get; set; }

    public SpecializationFileMap(GameVersion version, List<SpecializationData>? idList, GameVersion? basedOn = null)
    {
        Version = version;
        BasedOn = basedOn;
        _map = idList ?? new List<SpecializationData>();
    }

    public SpecializationData this[int index]
    {
        get
        {
            if (_resolved.TryGetValue(index, out SpecializationData? value))
            {
                return value;
            }

            return _resolved[index] = new SpecializationData(index, 179, "N/A");
        }
    }

    public void Resolve(ConcurrentDictionary<GameVersion, SpecializationFileMap> fileMap)
    {
        if (fileMap == null)
        {
            throw new ArgumentNullException(nameof(fileMap));
        }

        if (IsResolved)
        {
            return;
        }

        if (BasedOn != null)
        {
            if (BasedOn == Version)
            {
                throw new InvalidOperationException($"Circular dependency {Version}");
            }

            var baseSettings = fileMap[BasedOn.Value];
            if (baseSettings.BasedOn != null)
            {
                throw new InvalidOperationException($"Multilevel dependency not supported {Version}->{BasedOn.Value}->{baseSettings.BasedOn.Value}");
            }

            baseSettings.Resolve(fileMap);

            foreach ((int index, SpecializationData data) in baseSettings._resolved)
            {
                _resolved[index] = data;
            }
        }

        foreach (var data in _map)
        {
            _resolved[data.Index] = data;
        }

        IsResolved = true;
    }
}