namespace HeroesCenter.Core;

public enum GameVersion
{
    ShadowOfDeath,
    InTheWakeOfGods,
    HornOfTheAbyss
}

public static class GameVersionExtensions
{
    public static int GetAllHeroesCount(this GameVersion input)
    {
        return input == GameVersion.HornOfTheAbyss ? 179 : 156;
    }
}