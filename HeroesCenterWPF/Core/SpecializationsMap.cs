namespace HeroesCenter.Core;

public class SpecializationsMap : ConcurrentDictionary<GameVersion, SpecializationFileMap>
{
    [OnDeserialized]
    [UsedImplicitly]
    private void Resolve(StreamingContext _)
    {
        foreach (var value in Values)
        {
            value.Resolve(this);
        }
    }

    public SpecializationData this[GameVersion version, int id] => this[version][id];
}