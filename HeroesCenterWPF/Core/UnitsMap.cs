namespace HeroesCenter.Core;

public class UnitsMap : ConcurrentDictionary<GameVersion, UnitSettingsMap>
{
    [OnDeserialized]
    [UsedImplicitly]
    private void Resolve(StreamingContext _)
    {
        foreach (var value in Values)
        {
            value.Resolve(this);
        }
    }

    public UnitSettings this[GameVersion version, int unitType] => this[version][unitType];
}