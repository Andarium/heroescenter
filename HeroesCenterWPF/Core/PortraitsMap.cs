namespace HeroesCenter.Core;

public class PortraitsMap : ConcurrentDictionary<GameVersion, VersionFileSet>
{
    [OnDeserialized]
    [UsedImplicitly]
    private void Resolve(StreamingContext _)
    {
        foreach (VersionFileSet value in Values)
        {
            value.Resolve(this);
        }
    }

    public string this[GameVersion version, int id] => this[version][id];
}