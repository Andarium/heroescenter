using System.Diagnostics.CodeAnalysis;

namespace HeroesCenter.Core;

public class ConcurrentDictionary<TKey1, TKey2, TKey3, TValue> : ConcurrentDictionary<TKey1, TKey2, ConcurrentDictionary<TKey3, TValue>>
    where TKey1 : notnull
    where TKey2 : notnull
    where TKey3 : notnull
{
    public TValue this[TKey1 key1, TKey2 key2, TKey3 key3]
    {
        get => this[key1, key2][key3];
        set
        {
            if (!TryGetValue(key1, key2, out var subDict))
            {
                subDict = new ConcurrentDictionary<TKey3, TValue>();
                this[key1, key2] = subDict;
            }

            subDict[key3] = value;
        }
    }

    public bool TryGetValue(TKey1 key1, TKey2 key2, TKey3 key3, [MaybeNullWhen(false)] out TValue value)
    {
        if (TryGetValue(key1, key2, out var subDict))
        {
            if (subDict.TryGetValue(key3, out var val))
            {
                value = val;
                return true;
            }
        }

        value = default;
        return false;
    }

    public bool TryRemove(TKey1 key1, TKey2 key2, TKey3 key3, [MaybeNullWhen(false)] out TValue value)
    {
        if (TryGetValue(key1, key2, out var subDict))
        {
            if (subDict.TryRemove(key3, out var val))
            {
                value = val;
                return true;
            }
        }

        value = default;
        return false;
    }

    public ConcurrentDictionary()
    {
    }

    public ConcurrentDictionary(IEnumerable<KeyValuePair<TKey1, IEnumerable<KeyValuePair<TKey2, IEnumerable<KeyValuePair<TKey3, TValue>>>>>> collection)
        : base(collection.ToDictionary(
            x => x.Key,
            y => y.Value.ToDictionary(
                zk => zk.Key,
                zv => new ConcurrentDictionary<TKey3, TValue>(zv.Value)).AsEnumerable()
        ))
    {
    }

    public ConcurrentDictionary(IEnumerable<KeyValuePair<TKey1, IEnumerable<KeyValuePair<TKey2, IDictionary<TKey3, TValue>>>>> collection)
        : base(collection.ToDictionary(
            x => x.Key,
            y => y.Value.ToDictionary(
                zk => zk.Key,
                zv => new ConcurrentDictionary<TKey3, TValue>(zv.Value)).AsEnumerable()
        ))
    {
    }

    public ConcurrentDictionary(IEnumerable<KeyValuePair<TKey1, IEnumerable<KeyValuePair<TKey2, Dictionary<TKey3, TValue>>>>> collection)
        : base(collection.ToDictionary(
            x => x.Key,
            y => y.Value.ToDictionary(
                zk => zk.Key,
                zv => new ConcurrentDictionary<TKey3, TValue>(zv.Value)).AsEnumerable()
        ))
    {
    }

    public ConcurrentDictionary(IEnumerable<KeyValuePair<TKey1, IDictionary<TKey2, IDictionary<TKey3, TValue>>>> collection)
        : base(collection.ToDictionary(
            x => x.Key,
            y => y.Value.ToDictionary(
                zk => zk.Key,
                zv => new ConcurrentDictionary<TKey3, TValue>(zv.Value)).AsEnumerable()
        ))
    {
    }

    public ConcurrentDictionary(IEnumerable<KeyValuePair<TKey1, IDictionary<TKey2, Dictionary<TKey3, TValue>>>> collection)
        : base(collection.ToDictionary(
            x => x.Key,
            y => y.Value.ToDictionary(
                zk => zk.Key,
                zv => new ConcurrentDictionary<TKey3, TValue>(zv.Value)).AsEnumerable()
        ))
    {
    }

    public ConcurrentDictionary(IEnumerable<KeyValuePair<TKey1, Dictionary<TKey2, IDictionary<TKey3, TValue>>>> collection)
        : base(collection.ToDictionary(
            x => x.Key,
            y => y.Value.ToDictionary(
                zk => zk.Key,
                zv => new ConcurrentDictionary<TKey3, TValue>(zv.Value)).AsEnumerable()
        ))
    {
    }

    public ConcurrentDictionary(IEnumerable<KeyValuePair<TKey1, Dictionary<TKey2, Dictionary<TKey3, TValue>>>> collection)
        : base(collection.ToDictionary(
            x => x.Key,
            y => y.Value.ToDictionary(
                zk => zk.Key,
                zv => new ConcurrentDictionary<TKey3, TValue>(zv.Value)).AsEnumerable()
        ))
    {
    }
}