namespace HeroesCenter.Core;

public class UnitSettingsMap : IVersion
{
    [JsonProperty("Version", Order = 0, Required = Required.Always)]
    public GameVersion Version { get; }

    [JsonProperty("BasedOn", Order = 1, DefaultValueHandling = DefaultValueHandling.Ignore, NullValueHandling = NullValueHandling.Ignore)]
    public readonly GameVersion? BasedOn;

    [JsonProperty("Map", Order = 4, ItemConverterType = typeof(UnitSettingsConverter))]
    private readonly List<UnitSettings> _map;

    private readonly Dictionary<int, UnitSettings> _resolved = new();

    [JsonIgnore]
    private bool IsResolved { get; set; }

    private readonly UnitSettings _absent;

    public UnitSettingsMap(GameVersion version, List<UnitSettings>? idList, GameVersion? basedOn = null)
    {
        Version = version;
        BasedOn = basedOn;
        _map = idList ?? new List<UnitSettings>();
        _absent = new UnitSettings(-1, "-1", Version, false);
    }

    public UnitSettings this[int unitType]
    {
        get
        {
            if (_resolved.TryGetValue(unitType, out UnitSettings? value))
            {
                return value;
            }

            return unitType switch
            {
                -1 => _absent,
                < 0 => throw new ArgumentOutOfRangeException($"Invalid unit type: {unitType}"),
                _ => _resolved[unitType] = new UnitSettings(unitType, unitType.ToString(), Version, false)
            };
        }
    }

    public List<UnitSettings> GetAllUnits() => _resolved.Values.ToList();

    public void Resolve(ConcurrentDictionary<GameVersion, UnitSettingsMap> versionMap)
    {
        if (versionMap == null)
        {
            throw new ArgumentNullException(nameof(versionMap));
        }

        if (IsResolved)
        {
            return;
        }

        if (BasedOn != null)
        {
            if (BasedOn == Version)
            {
                throw new InvalidOperationException($"Circular dependency {Version}");
            }

            var baseSettings = versionMap[BasedOn.Value];
            if (baseSettings.BasedOn != null)
            {
                throw new InvalidOperationException($"Multilevel dependency not supported {Version}->{BasedOn.Value}->{baseSettings.BasedOn.Value}");
            }

            baseSettings.Resolve(versionMap);

            foreach ((int unitType, UnitSettings data) in baseSettings._resolved)
            {
                _resolved[unitType] = data;
            }
        }

        foreach (var data in _map)
        {
            _resolved[data.UnitType] = data;
        }

        IsResolved = true;
    }
}