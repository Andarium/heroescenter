namespace HeroesCenter.Core;

public interface IVersion
{
    GameVersion Version { get; }
}