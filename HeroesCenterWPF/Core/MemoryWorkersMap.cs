namespace HeroesCenter.Core;

[UsedImplicitly]
public sealed class MemoryWorkersMap : ConcurrentDictionary<GameVersion, MemoryWorkerType, MemoryWorkerSettings>
{
}