namespace HeroesCenter.Core;

public static class FrameCounter
{
    public static int Frame { get; private set; }

    public static void Init()
    {
        CompositionTarget.Rendering += OnRendering;
    }

    public static void Dispose()
    {
        CompositionTarget.Rendering -= OnRendering;
    }

    private static void OnRendering(object? sender, EventArgs e)
    {
        Frame++;
    }
}