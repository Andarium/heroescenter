namespace HeroesCenter.Core;

public class VersionFileSet : IVersion
{
    [JsonProperty("Version", Order = 0, Required = Required.Always)]
    public GameVersion Version { get; }

    [JsonProperty("BasedOn", Order = 1, DefaultValueHandling = DefaultValueHandling.Ignore, NullValueHandling = NullValueHandling.Ignore)]
    public readonly GameVersion? BasedOn;

    [JsonProperty("SubFolder", Order = 2, DefaultValueHandling = DefaultValueHandling.Ignore, NullValueHandling = NullValueHandling.Ignore)]
    private readonly string? _subFolder;

    [JsonProperty("Format", Order = 3)]
    private readonly string _format;

    [JsonConverter(typeof(Int32HashSetRangeConverter))]
    [JsonProperty("IdSet", Order = 4)]
    private readonly HashSet<int> _idSet;

    private readonly Dictionary<int, string> _resolvedNames = new();

    [JsonIgnore]
    private bool IsResolved { get; set; }

    public VersionFileSet(GameVersion version, string format, HashSet<int>? idList, GameVersion? basedOn = null, string? subFolder = null)
    {
        Version = version;
        _subFolder = string.IsNullOrWhiteSpace(subFolder) ? string.Empty : subFolder;
        BasedOn = basedOn;
        _format = format;
        _idSet = idList ?? new HashSet<int>();
    }

    public string this[int index] => _resolvedNames[index];

    public void Resolve(ConcurrentDictionary<GameVersion, VersionFileSet> fileMap)
    {
        if (fileMap == null)
        {
            throw new ArgumentNullException(nameof(fileMap));
        }

        if (IsResolved)
        {
            return;
        }

        if (BasedOn != null)
        {
            if (BasedOn == Version)
            {
                throw new InvalidOperationException($"Circular dependency {Version}");
            }

            var baseSettings = fileMap[BasedOn.Value];
            if (baseSettings.BasedOn != null)
            {
                throw new InvalidOperationException($"Multilevel dependency not supported {Version}->{BasedOn.Value}->{baseSettings.BasedOn.Value}");
            }

            baseSettings.Resolve(fileMap);

            foreach ((int id, string path) in baseSettings._resolvedNames)
            {
                _resolvedNames[id] = path;
            }
        }

        foreach (int id in _idSet)
        {
            var subFolder = string.IsNullOrWhiteSpace(_subFolder)
                ? string.Empty
                : _subFolder + Path.DirectorySeparatorChar;
            _resolvedNames[id] = subFolder + string.Format(_format, id);
        }

        IsResolved = true;
    }
}