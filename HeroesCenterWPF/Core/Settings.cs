namespace HeroesCenter.Core;

public sealed class Settings
{
    private static Settings? _instance;

    public static Settings Instance => _instance ??= new Settings();

    public readonly PortraitsMap Portraits;
    public readonly MemoryWorkersMap MemoryWorkers;
    public readonly SpecializationsMap Specializations;
    public readonly UnitsMap Units;

    public static void Initialize()
    {
        _instance ??= new Settings();
    }

    private Settings()
    {
        Portraits = JsonLoader.LoadSettings<PortraitsMap>(ResourceHelper.Settings.PortraitMap);
        MemoryWorkers = JsonLoader.LoadSettings<MemoryWorkersMap>(ResourceHelper.Settings.MemoryWorkerMap);
        Specializations = JsonLoader.LoadSettings<SpecializationsMap>(ResourceHelper.Settings.Specializations);
        Units = JsonLoader.LoadSettings<UnitsMap>(ResourceHelper.Settings.Units);
    }
}