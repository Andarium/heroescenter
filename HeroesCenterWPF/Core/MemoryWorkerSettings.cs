namespace HeroesCenter.Core;

public class MemoryWorkerSettings
{
    [JsonProperty("Type", Order = 0)]
    public MemoryWorkerType Type { get; }

    [JsonProperty("BlockSize", Order = 1, DefaultValueHandling = DefaultValueHandling.Ignore)]
    public readonly int BlockSize;

    [JsonProperty("Module", Order = 2)]
    public readonly string ModuleName;

    [JsonProperty("ModuleOffset", Order = 3)]
    [JsonConverter(typeof(HexNumberJsonConverter))]
    public readonly int ModuleOffset;

    [JsonProperty("Pointers", Order = 4, ItemConverterType = typeof(HexNumberJsonConverter))]
    public int[] Pointers;

    public MemoryWorkerSettings(MemoryWorkerType type, int blockSize, string moduleName, int moduleOffset = 0, int[]? pointers = default)
    {
        Type = type;
        BlockSize = blockSize;
        ModuleName = moduleName;
        ModuleOffset = moduleOffset;
        Pointers = pointers ?? Array.Empty<int>();
    }
}