namespace HeroesCenter;

public partial class App
{
    protected override void OnStartup(StartupEventArgs e)
    {
        FrameCounter.Init();
        NativeMethods.TimeBeginPeriod(1);
        base.OnStartup(e);
        Settings.Initialize();
    }

    protected override void OnExit(ExitEventArgs e)
    {
        base.OnExit(e);
        FrameCounter.Dispose();
    }
}