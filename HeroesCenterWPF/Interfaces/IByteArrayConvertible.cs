namespace HeroesCenter.Interfaces;

public interface IByteArrayConvertible
{
    byte[] ToByteArray();
}