namespace HeroesCenter.ViewModel;

public class ResourceViewModel : AbstractIndexedViewModel<ResourceModel>
{
    public ResourceViewModel(GameVersion version, int playerIndex) : base(new ResourceModel(version, playerIndex))
    {
    }

    public int Wood
    {
        get => Model.Wood;
        set => Model.Wood = value;
    }

    public int Mercury
    {
        get => Model.Mercury;
        set => Model.Mercury = value;
    }

    public int Ore
    {
        get => Model.Ore;
        set => Model.Ore = value;
    }

    public int Sulfur
    {
        get => Model.Sulfur;
        set => Model.Sulfur = value;
    }

    public int Crystals
    {
        get => Model.Crystals;
        set => Model.Crystals = value;
    }

    public int Gems
    {
        get => Model.Gems;
        set => Model.Gems = value;
    }

    public int Gold
    {
        get => Model.Gold;
        set => Model.Gold = value;
    }
}