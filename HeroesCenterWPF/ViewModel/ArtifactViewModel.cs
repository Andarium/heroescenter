namespace HeroesCenter.ViewModel;

[UsedImplicitly]
public class ArtifactViewModel : AbstractIndexedViewModel<ArtifactModel>
{
    public ArtifactViewModel(GameVersion version, int heroIndex, int slotIndex) : base(new ArtifactModel(version, heroIndex, slotIndex))
    {
    }

    public int SlotIndex => Model.Index;

    public int Type
    {
        get => Model.Type;
        set
        {
            Model.Type = value;
            OnPropertyChanged();
        }
    }

    public int Data
    {
        get => Model.Data;
        set
        {
            Model.Data = value;
            OnPropertyChanged();
        }
    }

    private bool _isSealed;

    public bool IsSealed
    {
        get
        {
            if (Index > 12 && Index != 18)
            {
                return false;
            }

            Seals.UpdateSeals();
            return _isSealed;
        }
        set => _isSealed = value;
    }

    public SealsModel Seals { get; set; } = default!;

    public bool IsEmpty => Type == -1;

    [DependsOn(nameof(Type))]
    [DependsOn(nameof(Data))]
    public ImageSource Image => IsSealed ? ImageHelper.LockImage : Info.Image;

    public ArtifactInfo Info => ArtifactHelper.GetArtifactInfo(Type, Data, Version);

    [DependsOn(nameof(Type))]
    [DependsOn(nameof(Data))]
    public string DisplayName => Info.DisplayName;

    [DependsOn(nameof(Type))]
    [DependsOn(nameof(Data))]
    public string Name => Info.Name;

    /// <summary>
    ///     Возвращает категорию текущего артефакта или слота
    /// </summary>
    public ArtifactCategory Category => Type == -1 ? SlotHelper.GetSlotCategory(SlotIndex) : Info.Category;

    /// <summary>
    ///     Возвращает название слота, к которому привязана данная модель
    /// </summary>
    public string SlotName => SlotHelper.GetSlotName(SlotIndex);

    public override string ToString()
    {
        return $"{Name} {{{SlotName}}}";
    }

    public void Swap(ArtifactViewModel other)
    {
        var temp = (Type, Data);
        SetValues(other.Type, other.Data);
        other.SetValues(temp.Type, temp.Data);
    }

    public void SetValues(int type, int data)
    {
        Model.Type = type;
        Model.Data = data;
        OnPropertyChanged(nameof(Type));
        OnPropertyChanged(nameof(data));
    }
}