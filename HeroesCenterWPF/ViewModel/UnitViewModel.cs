namespace HeroesCenter.ViewModel;

public abstract class AbstractUnitViewModel<T> : AbstractIndexedViewModel<T> where T : UnitModel
{
    protected AbstractUnitViewModel(T model) : base(model)
    {
    }

    [UsedImplicitly]
    public ImageSource Image => ImageHelper.GetImage(Version, ImageType.Unit, Type, false);

    public int Type
    {
        get => Model.Type;
        set => Model.Type = value;
    }

    [UsedImplicitly]
    public int Count
    {
        get => Model.Count;
        set => Model.Count = value;
    }
}

[UsedImplicitly]
public class UnitViewModel : AbstractUnitViewModel<UnitModel>
{
    public UnitViewModel(GameVersion version, int heroIndex, int index) : base(new UnitModel(version, heroIndex, index))
    {
    }
}

/*[UsedImplicitly]
public class GarrisonUnitViewModel : AbstractUnitViewModel<GarrisonUnitModel>
{
    public GarrisonUnitViewModel(Int32 townIndex, Int32 index) : base(new GarrisonUnitModel(townIndex, index))
    {
    }
}*/