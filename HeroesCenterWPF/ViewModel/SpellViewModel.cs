namespace HeroesCenter.ViewModel;

public sealed class SpellViewModel : AbstractIndexedViewModel<SpellModel>
{
    public SpellViewModel(GameVersion version, int parentIndex, int index) : base(new SpellModel(version, parentIndex, index))
    {
    }

    [UsedImplicitly]
    public ImageSource Image => ImageHelper.GetImage(Version, ImageType.Spell, Index);

    public bool? IsLearned
    {
        get
        {
            var learned = Model.Learned == 1;
            var inBook = Model.InBook == 1;

            if (learned)
            {
                // ignore not in book case
                return true;
            }

            return inBook ? null : false;
        }
        set
        {
            Model.Learned = (byte) (value == true ? 1 : 0);
            Model.InBook = (byte) (value == false ? 0 : 1);
            OnPropertyChanged();
        }
    }

    public string DisplayName => $"Spell #{Index}";
}