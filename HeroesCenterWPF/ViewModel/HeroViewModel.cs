namespace HeroesCenter.ViewModel;

[UsedImplicitly]
public class HeroViewModel : AbstractIndexedViewModel<HeroModel>
{
    private const byte Alpha = 0;
    private const byte Colorful = 180;

    private static readonly IReadOnlyDictionary<GameVersion, decimal[]> IntelligenceMultiplier = new Dictionary<GameVersion, decimal[]>
    {
        { GameVersion.ShadowOfDeath, new[] { 0m, 0.25m, 0.5m, 1m } },
        { GameVersion.InTheWakeOfGods, new[] { 0m, 0.25m, 0.5m, 1m } },
        { GameVersion.HornOfTheAbyss, new[] { 0m, 0.2m, 0.35m, 0.5m } }
    };

    public static readonly IReadOnlyList<SolidColorBrush> FlagBrushes = new[]
    {
        new SolidColorBrush(Color.FromArgb(Alpha, Colorful, 0, 0)),
        new SolidColorBrush(Color.FromArgb(Alpha, 0, 0, Colorful)),
        new SolidColorBrush(Color.FromArgb(Alpha, Colorful, Colorful / 4 * 3, Colorful / 2)),
        new SolidColorBrush(Color.FromArgb(Alpha, 0, Colorful, 0)),
        new SolidColorBrush(Color.FromArgb(Alpha, Colorful, Colorful / 2, 0)),
        new SolidColorBrush(Color.FromArgb(Alpha, Colorful / 4 * 3, 0, Colorful / 4 * 3)),
        new SolidColorBrush(Color.FromArgb(Alpha, 0, Colorful, Colorful)),
        new SolidColorBrush(Color.FromArgb(Alpha, Colorful, Colorful / 2, Colorful / 4 * 3)),
        new SolidColorBrush(Color.FromArgb(0, 255, 255, 255))
    };

    public HeroViewModel(GameVersion version, int index) : base(new HeroModel(version, index))
    {
        PrimarySkills = new PrimarySkillsViewModel(version, index);
        SecondarySkills = new SecondarySkillsViewModel(version, index);
        Army = new ArmyViewModel(version, index);
        Artifacts = new ArtifactsViewModel(version, index);
        Spells = new SpellBookViewModel(version, index);

        PrimarySkills.PropertyChanged += RaisePropertyChanged;
        SecondarySkills.IntelligenceChanged += () => RaisePropertyChanged(string.Empty);
    }

    public PrimarySkillsViewModel PrimarySkills { get; }
    public SecondarySkillsViewModel SecondarySkills { get; }
    public ArmyViewModel Army { get; }
    public ArtifactsViewModel Artifacts { get; }
    public SpellBookViewModel Spells { get; }

    public string ClassName => HeroHelper.Instance.Classes.ElementAt(Class);

    public string? FullClass => string.IsNullOrEmpty(ClassName)
        ? null
        : $"Уровень {PrimarySkills.Level}, {ClassName}";

    public ImageSource SmallPortrait => ImageHelper.GetImage(Version, ImageType.PortraitSmall, Portrait);
    public ImageSource BigPortrait => ImageHelper.GetImage(Version, ImageType.PortraitBig, Portrait);

    public ImageSource Flag => ImageHelper.GetFlagImage(PrimarySkills.Flag);

    public SolidColorBrush? FlagBrush => FlagBrushes.ElementAtOrLastOrDefault(PrimarySkills.Flag);

    [DependsOn(nameof(PrimarySkillsViewModel.Flag))]
    public ImageSource ColoredBackgroundTile => ImageHelper.GetBackgroundTile(PrimarySkills.Flag);

    [DependsOn(nameof(PrimarySkillsViewModel.Flag))]
    public ImageSource ColoredInventory => ImageHelper.GetHeroInventory(PrimarySkills.Flag);

    public string DisplayName
    {
        get => Model.Name.Replace(StringExtensions.EndString, string.Empty);
        set => Model.Name = value.EndsWith(StringExtensions.EndString) ? value : $"{value}{StringExtensions.EndString}";
    }

    public byte Portrait
    {
        get => Model.Portrait;
        set => Model.Portrait = value;
    }

    public int Class
    {
        get => Model.Class;
        set => Model.Class = value;
    }

    public short X
    {
        get => Model.X;
        set
        {
            Model.X = value;
            OnPropertyChanged();
        }
    }

    public short Y
    {
        get => Model.Y;
        set
        {
            Model.Y = value;
            OnPropertyChanged();
        }
    }

    public short Z
    {
        get => Model.Z;
        set
        {
            Model.Z = value.Clamp(0, 1);
            OnPropertyChanged();
        }
    }

    public bool Unknown
    {
        get => Model.Unknown;
        set => Model.Unknown = value;
    }

    public byte TeleportUseCount
    {
        get => Model.TeleportUseCount;
        set => Model.TeleportUseCount = value;
    }

    public byte MoraleTotal
    {
        get => Model.MoraleTotal;
        set => Model.MoraleTotal = value;
    }

    public byte LuckTotal
    {
        get => Model.LuckTotal;
        set => Model.LuckTotal = value;
    }

    public override string ToString()
    {
        return DisplayName;
    }

    public short MaximumMana
    {
        get
        {
            var stage = SecondarySkills.Intelligence.Stage;

            // check if hero specialization is Intelligence
            var specMulti = PrimarySkills.HeroIndex is 26 or 56 or 126
                ? 1 + PrimarySkills.Level * 0.05m
                : 1m;

            var multi = 1 + IntelligenceMultiplier[Version][stage] * specMulti;
            var knowledge = PrimarySkills.KnowledgeClamped;
            var value = Math.Round(knowledge * 10 * multi, MidpointRounding.ToZero);
            return Convert.ToInt16(value);
        }
    }

    [DependsOn(nameof(PrimarySkillsViewModel.ManaPoints))]
    public string ManaText => $"{PrimarySkills.ManaPoints} / {MaximumMana}";

    public void RestoreMana(bool doubleMana)
    {
        PrimarySkills.ManaPoints = (short) (MaximumMana * (doubleMana ? 2 : 1));
    }

    public void MultiplyMovement(decimal multiplier)
    {
        PrimarySkills.MovementPoints = (int) (PrimarySkills.MovementPoints * multiplier);
    }

    public void AddMovement(int value)
    {
        PrimarySkills.MovementPoints += value;
    }

    public void SetMovementPoints(int value)
    {
        PrimarySkills.MovementPoints = value;
    }

    public void ResetTeleportUses()
    {
        TeleportUseCount = 0;
    }
}