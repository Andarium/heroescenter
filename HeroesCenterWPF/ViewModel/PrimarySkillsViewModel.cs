namespace HeroesCenter.ViewModel;

public class PrimarySkillsViewModel : AbstractIndexedViewModel<PrimarySkillsModel>
{
    private readonly byte _max;

    public PrimarySkillsViewModel(GameVersion version, int heroIndex) : base(new PrimarySkillsModel(version, heroIndex))
    {
        _max = version == GameVersion.InTheWakeOfGods ? (byte) 127 : (byte) 99;
    }

    public byte HeroIndex
    {
        get => Model.HeroIndex;
        set => Model.HeroIndex = value;
    }

    private SpecializationData Specialization => Settings.Instance.Specializations[Version, HeroIndex];

    public string SpecializationName => Specialization.Name;

    public ImageSource SpecializationImage => ImageHelper.GetSpecializationImage(Specialization.Key);

    public byte Attack
    {
        get => Model.Attack;
        set
        {
            Model.Attack = value;
            OnPropertyChanged();
        }
    }

    [DependsOn(nameof(Attack))]
    public byte AttackClamped => GetClampedValue(Model.Attack);

    [DependsOn(nameof(Attack))]
    public string AttackDisplay => GetDisplayValue(Attack, AttackClamped);

    public byte Defence
    {
        get => Model.Defence;
        set
        {
            Model.Defence = value;
            OnPropertyChanged();
        }
    }

    [DependsOn(nameof(Defence))]
    public byte DefenceClamped => GetClampedValue(Model.Defence);

    [DependsOn(nameof(Defence))]
    public string DefenceDisplay => GetDisplayValue(Defence, DefenceClamped);

    public byte SpellPower
    {
        get => Model.SpellPower;
        set
        {
            Model.SpellPower = value;
            OnPropertyChanged();
        }
    }

    [DependsOn(nameof(SpellPower))]
    public byte SpellPowerClamped => GetClampedValueWithMin(Model.SpellPower);

    [DependsOn(nameof(SpellPower))]
    public string SpellPowerDisplay => GetDisplayValue(SpellPower, SpellPowerClamped);

    public byte Knowledge
    {
        get => Model.Knowledge;
        set
        {
            Model.Knowledge = value;
            OnPropertyChanged();
            OnPropertyChanged(nameof(ManaPoints));
        }
    }

    [DependsOn(nameof(Knowledge))]
    public byte KnowledgeClamped => GetClampedValueWithMin(Model.Knowledge);

    [DependsOn(nameof(Knowledge))]
    public string KnowledgeDisplay => GetDisplayValue(Knowledge, KnowledgeClamped);

    public int Experience
    {
        get => Model.Experience;
        set => Model.Experience = value;
    }

    public int MovementPoints
    {
        get => Model.MovementPoints;
        set
        {
            Model.MovementPoints = value;
            OnPropertyChanged();
        }
    }

    public int DailyMovement => Model.DailyMovement;

    public byte Flag
    {
        get => Model.Flag;
        set
        {
            Model.Flag = value;
            OnPropertyChanged();
        }
    }

    public short Level
    {
        get => Model.Level;
        set => Model.Level = value;
    }

    [DependsOn(nameof(Knowledge))]
    public short ManaPoints
    {
        get => Model.ManaPoints;
        set
        {
            Model.ManaPoints = value;
            OnPropertyChanged();
        }
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    private byte GetClampedValueWithMin(byte value)
    {
        return value == 0 ? (byte) 1 : GetClampedValue(value);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    private byte GetClampedValue(byte value)
    {
        if (value > 127)
        {
            return 1;
        }

        return value > _max ? _max : value;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    private string GetDisplayValue(byte value, byte clampedValue)
    {
        if (value == clampedValue)
        {
            return value.ToString();
        }

        return $"{clampedValue} ({value})";
    }
}