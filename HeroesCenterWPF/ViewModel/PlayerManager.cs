namespace HeroesCenter.ViewModel;

public class PlayerManager : AbstractSimpleCollectionViewModel<PlayerViewModel>
{
    private int _currentPlayerIndex;

    public PlayerManager(GameVersion version) : base(version, CreatePlayers)
    {
    }

    public PlayerViewModel CurrentPlayer => this[CurrentPlayerIndex];

    public int CurrentPlayerIndex
    {
        get => _currentPlayerIndex;
        set
        {
            if (value == _currentPlayerIndex)
            {
                return;
            }

            _currentPlayerIndex = value;
            OnPropertyChanged();
            OnPropertyChanged(nameof(CurrentPlayer));
        }
    }

    private static IEnumerable<PlayerViewModel> CreatePlayers(GameVersion version)
    {
        return Enumerable.Range(0, 8).Select(x => new PlayerViewModel(version, x));
    }
}