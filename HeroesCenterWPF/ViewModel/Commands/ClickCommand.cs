namespace HeroesCenter.ViewModel.Commands;

public class ClickCommand<T> : ICommand where T: IPointerClickHandler
{
    private readonly T _source;

    public ClickCommand(T source)
    {
        _source = source;
    }

    public bool CanExecute(object? parameter)
    {
        return _source.CanClick();
    }

    public void Execute(object? parameter)
    {
        if (CanExecute(parameter))
        {
            _source.OnPointerClick();
        }
    }

    public event EventHandler? CanExecuteChanged
    {
        add => _source.CanClickChanged += value;
        remove => _source.CanClickChanged -= value;
    }
}