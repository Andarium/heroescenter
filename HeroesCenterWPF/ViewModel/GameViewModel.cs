namespace HeroesCenter.ViewModel;

public sealed class GameViewModel : AbstractViewModel<GameModel>
{
    private GameViewModel(Settings settings, GameVersion version) : base(new GameModel(version))
    {
        HeroManager = new HeroManager(version);
        PlayerManager = new PlayerManager(version);
        // TownManager = new TownManager();
        CreatureManager = new CreatureManager(settings, version);
        Map = new MapViewModel(version);
        Battle = new BattleViewModel(version);
        UI = new UIViewModel(version);
        HeroManager.PropertyChanged += RaisePropertyChanged;
        HeroManager.CurrentHeroPropertyChanged += OnPropertyChanged;
    }

    public static GameViewModel? Instance { get; set; }

    public bool IsWog => Version == GameVersion.InTheWakeOfGods;

    /// <summary>
    ///     Возвращает индекс активного (текущего) игрока в игре
    /// </summary>
    public int ActivePlayerIndex => Model.ActivePlayer;

    /// <summary>
    ///     Возвращает активного (текущего) игрока в игре
    /// </summary>
    public PlayerViewModel ActivePlayer => PlayerManager.Get(ActivePlayerIndex);

    /// <summary>
    ///     Возвращает индекс активного (выбранного) в игре героя для активного игрока
    /// </summary>
    public int ActiveHeroIndex => ActivePlayer.ActiveHeroIndex;

    /// <summary>
    ///     Возвращает индекс активного (выбранного) в игре города для активного игрока
    /// </summary>
    public int ActiveTownIndex => ActivePlayer.ActiveTownIndex;

    /// <summary>
    ///     Возвращает активного (выбранного) в игре героя (null, если он не выбран)
    /// </summary>
    public HeroViewModel? ActiveHero => HeroManager.GetOrDefault(ActiveHeroIndex);

    /// <summary>
    ///     Возвращает выбранного в программе героя
    /// </summary>
    public HeroViewModel? CurrentHero => HeroManager.CurrentHero;

    /// <summary>
    ///     Возвращает игрока текущего выбранного героя в программе
    /// </summary>
    [DependsOn(nameof(CurrentHero))]
    [DependsOn(nameof(HeroViewModel.Flag))]
    public PlayerViewModel? CurrentPlayer => PlayerManager.ElementAtOrDefault(CurrentHero?.PrimarySkills.Flag);

    /// <summary>
    ///     Возвращает индекс игрока текущего выбранного героя в программе
    /// </summary>
    public int CurrentPlayerIndex => CurrentPlayer?.Index ?? -1;

    public HeroManager HeroManager { get; }

    public PlayerManager PlayerManager { get; }

    // public TownManager TownManager { get; }
    public CreatureManager CreatureManager { get; }
    public MapViewModel Map { get; }
    public BattleViewModel Battle { get; }
    public UIViewModel UI { get; }

    /// <summary>
    ///     Возвращает внутриигровой индекс выбранного в программе героя
    /// </summary>
    [DependsOn(nameof(CurrentHero))]
    public int CurrentHeroIndex => HeroManager.CurrentHeroIndex;

    /// <summary>
    ///     Возвращает внутриигровой индекс портрета выбранного в программе героя
    /// </summary>
    [DependsOn(nameof(CurrentHero))]
    public int CurrentHeroPortraitIndex => HeroManager.CurrentHeroPortraitIndex;

    public static GameViewModel CreateInstance(Settings settings, GameVersion version)
    {
        return Instance = new GameViewModel(settings, version);
    }

    public HeroViewModel? GetInitialHero()
    {
        PlayerViewModel player = ActivePlayer;
        return ActiveHero ??
               HeroManager.FirstOrDefault(x => x.PrimarySkills.Flag == player.Flag);
    }
}