namespace HeroesCenter.ViewModel;

public class SecondarySkillsViewModel : AbstractCollectionViewModel<SecondarySkillModel, SecondarySkillsModel, SecondarySkillViewModel>
{
    public event Action? IntelligenceChanged;

    public SecondarySkillsViewModel(GameVersion version, int heroIndex) : base(new SecondarySkillsModel(version, heroIndex), m => new SecondarySkillViewModel(m))
    {
        Intelligence.PropertyChanged += (_, _) => IntelligenceChanged?.Invoke();
    }

    #region Public wrappers

    public SecondarySkillViewModel Pathfinding => this[0];

    public SecondarySkillViewModel Archery => this[1];

    public SecondarySkillViewModel Logistics => this[2];

    public SecondarySkillViewModel Scouting => this[3];

    public SecondarySkillViewModel Diplomacy => this[4];

    public SecondarySkillViewModel Navigation => this[5];

    public SecondarySkillViewModel Leadership => this[6];

    public SecondarySkillViewModel Wisdom => this[7];

    public SecondarySkillViewModel Mysticism => this[8];

    public SecondarySkillViewModel Luck => this[9];

    public SecondarySkillViewModel Ballistics => this[10];

    public SecondarySkillViewModel EagleEye => this[11];

    public SecondarySkillViewModel Necromancy => this[12];

    public SecondarySkillViewModel Estates => this[13];

    public SecondarySkillViewModel FireMagic => this[14];

    public SecondarySkillViewModel AirMagic => this[15];

    public SecondarySkillViewModel WaterMagic => this[16];

    public SecondarySkillViewModel EarthMagic => this[17];

    public SecondarySkillViewModel Scholar => this[18];

    public SecondarySkillViewModel Tactics => this[19];

    public SecondarySkillViewModel Artillery => this[20];

    public SecondarySkillViewModel Learning => this[21];

    public SecondarySkillViewModel Offence => this[22];

    public SecondarySkillViewModel Armorer => this[23];

    public SecondarySkillViewModel Intelligence => this[24];

    public SecondarySkillViewModel Sorcery => this[25];

    public SecondarySkillViewModel Resistance => this[26];

    public SecondarySkillViewModel FirstAid => this[27];

    public SecondarySkillViewModel Interference => this[28];
    public SecondarySkillViewModel? InterferenceSafe => GetOrDefault(28);

    #endregion
}