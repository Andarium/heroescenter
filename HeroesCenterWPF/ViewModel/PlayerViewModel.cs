namespace HeroesCenter.ViewModel;

public class PlayerViewModel : AbstractIndexedViewModel<PlayerModel>
{
    public PlayerViewModel(GameVersion version, int playerIndex) : base(new PlayerModel(version, playerIndex))
    {
        Resources = new ResourceViewModel(version, playerIndex);
    }

    public byte ActiveHeroIndex => Model.ActiveHeroIndex;
    public byte ActiveTownIndex => Model.ActiveTownIndex;
    public byte Flag => (byte) Model.Index;

    public int HeroOnMap1
    {
        get => Model.GetHeroOnMap(0);
        set => Model.SetHeroOnMap(0, value);
    }

    public int HeroOnMap2
    {
        get => Model.GetHeroOnMap(1);
        set => Model.SetHeroOnMap(1, value);
    }

    public int HeroOnMap3
    {
        get => Model.GetHeroOnMap(2);
        set => Model.SetHeroOnMap(2, value);
    }

    public int HeroOnMap4
    {
        get => Model.GetHeroOnMap(3);
        set => Model.SetHeroOnMap(3, value);
    }

    public int HeroOnMap5
    {
        get => Model.GetHeroOnMap(4);
        set => Model.SetHeroOnMap(4, value);
    }

    public int HeroOnMap6
    {
        get => Model.GetHeroOnMap(5);
        set => Model.SetHeroOnMap(5, value);
    }

    public int HeroOnMap7
    {
        get => Model.GetHeroOnMap(6);
        set => Model.SetHeroOnMap(6, value);
    }

    public int HeroOnMap8
    {
        get => Model.GetHeroOnMap(7);
        set => Model.SetHeroOnMap(7, value);
    }

    public int GetHeroOnMap(int slotIndex)
    {
        return Model.GetHeroOnMap(slotIndex);
    }

    public void SetHeroOnMap(int slotIndex, int heroIndex)
    {
        Model.SetHeroOnMap(slotIndex, heroIndex);
    }

    public ResourceViewModel Resources { get; }

    public void MultiplyGold(decimal multiplier)
    {
        Resources.Gold = (int) (Resources.Gold * multiplier);
    }

    public void MultiplyResources(decimal multiplier)
    {
        Resources.Ore = (int) (Resources.Ore * multiplier);
        Resources.Wood = (int) (Resources.Wood * multiplier);
        Resources.Gems = (int) (Resources.Gems * multiplier);
        Resources.Sulfur = (int) (Resources.Sulfur * multiplier);
        Resources.Mercury = (int) (Resources.Mercury * multiplier);
        Resources.Crystals = (int) (Resources.Crystals * multiplier);
    }

    public int FindIndexOfHeroOnMap(int heroIndex)
    {
        for (var i = 0; i < 8; i++)
        {
            if (Model.GetHeroOnMap(i) == heroIndex)
            {
                return i;
            }
        }

        return -1;
    }

    public int FindFreeSlotIndexOfHeroOnMap()
    {
        return FindIndexOfHeroOnMap(-1);
    }

    public bool AddHeroOnMap(HeroViewModel hero)
    {
        if (FindIndexOfHeroOnMap(hero.Index) >= 0)
        {
            return false;
        }

        var freeSlotIndex = FindFreeSlotIndexOfHeroOnMap();

        if (freeSlotIndex < 0)
        {
            return false;
        }

        SetHeroOnMap(freeSlotIndex, hero.Index);
        hero.Unknown = true;
        hero.PrimarySkills.Flag = Flag;
        return true;
    }
}