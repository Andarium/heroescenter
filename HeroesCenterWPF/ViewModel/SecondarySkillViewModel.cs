namespace HeroesCenter.ViewModel;

public class SecondarySkillViewModel : AbstractIndexedViewModel<SecondarySkillModel>
{
    public SecondarySkillViewModel(SecondarySkillModel model) : base(model)
    {
    }

    /// <summary>
    ///     Возвращает наименование стадии развития навыка
    /// </summary>
    [UsedImplicitly]
    public string? StageName => HeroHelper.Instance.Skills.Stages.ElementAtOrDefault(Stage);

    /// <summary>
    ///     Возвращает наименование навыка
    /// </summary>
    [UsedImplicitly]
    public string? SkillName => HeroHelper.Instance.Skills.Secondary.ElementAtOrDefault(Model.SkillIndex, "N/A");

    /// <summary>
    ///     Возвращает изображение, соответствующее навыку и его степени развития
    /// </summary>
    [UsedImplicitly]
    public ImageSource Image => ImageHelper.GetSecondarySkillImage(Model.SkillIndex, Stage);

    [UsedImplicitly]
    public byte Stage
    {
        get => Model.Stage;
        set
        {
            Model.Stage = value;
            OnPropertyChanged(null);
        }
    }
}