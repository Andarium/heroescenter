namespace HeroesCenter.ViewModel;

public class ArmyViewModel : AbstractSimpleCollectionViewModel<UnitViewModel>
{
    public ArmyViewModel(GameVersion version, int heroIndex) : base(version, CreateArmy(version, heroIndex))
    {
    }

    private static IEnumerable<UnitViewModel> CreateArmy(GameVersion version, int parentIndex)
    {
        return Enumerable.Range(0, 7).Select(x => new UnitViewModel(version, parentIndex, x));
    }
}

/*public class GarrisonArmyViewModel : AbstractSimpleCollectionViewModel<GarrisonUnitViewModel>
{
    public GarrisonArmyViewModel(Int32 townIndex) : base(CreateArmy(townIndex))
    {
    }

    private static IEnumerable<GarrisonUnitViewModel> CreateArmy(Int32 parentIndex)
    {
        return Enumerable.Range(0, 7).Select(x => new GarrisonUnitViewModel(parentIndex, x));
    }
}*/