namespace HeroesCenter.ViewModel;

public class CreatureManager : AbstractMapViewModel<int, CreatureViewModel>
{
    public CreatureManager(Settings settings, GameVersion version) : base(version, GetCreatures(settings, version), model => model.UnitType)
    {
    }

    private static IEnumerable<CreatureViewModel> GetCreatures(Settings settings, GameVersion version)
    {
        return settings.Units[version]
            .GetAllUnits()
            .OrderBy(x => x.UnitType)
            .Select(x => new CreatureViewModel(x, version))
            .ToList();
    }

    public IList<CreatureViewModel> GetTownUnits(TownType town, bool onlyAvailable)
    {
        var result = Values
            .Where(x => x.Town == town && (!onlyAvailable || x.IsAvailable));

        if (town == TownType.Conflux)
        {
            result = result.OrderBy(ConfluxSorter);
        }

        return result.ToList();
    }

    private static int ConfluxSorter(CreatureViewModel unit)
    {
        return ConfluxOrder.IndexOf(unit.UnitType);
    }

    private static readonly List<int> ConfluxOrder = new()
    {
        118, 119, 112, 127, 115, 123, 114, 129, 113, 125, 120, 121, 130, 131, 158
    };
}