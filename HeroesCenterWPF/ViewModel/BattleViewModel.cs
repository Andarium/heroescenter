namespace HeroesCenter.ViewModel;

public class BattleViewModel : AbstractViewModel<BattleModel>
{
    public byte IsMagicGarrison
    {
        get => Model.IsMagicGarrison;
        set => Model.IsMagicGarrison = value;
    }

    public byte IsMagicUsedHeroLeft
    {
        get => Model.IsMagicUsedHeroLeft;
        set => Model.IsMagicUsedHeroLeft = value;
    }

    public byte IsMagicUsedHeroRight
    {
        get => Model.IsMagicUsedHeroRight;
        set => Model.IsMagicUsedHeroRight = value;
    }

    public BattleViewModel(GameVersion version) : base(new BattleModel(version))
    {
    }
}