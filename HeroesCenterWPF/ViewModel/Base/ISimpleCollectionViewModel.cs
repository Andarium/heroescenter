namespace HeroesCenter.ViewModel.Base;

public interface ISimpleCollectionViewModel<out T> : IReadOnlyList<T>
    where T : class, IIndexedViewModel<IIndexedModel>
{
}