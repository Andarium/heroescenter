namespace HeroesCenter.ViewModel.Base;

public interface IMapViewModel<TKey, T> : IReadOnlyDictionary<TKey, T>
    where T : class, IIndexedViewModel<IIndexedModel>
{
}