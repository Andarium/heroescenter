namespace HeroesCenter.ViewModel.Base;

public interface IViewModel : INotifyPropertyChanged
{
    public GameVersion Version { get; }
}

public interface IViewModel<out T> : IViewModel where T : IModel
{
    T Model { get; }
    public string AddressRange => Model.AddressRange;
}

public interface IIndexedViewModel<out T> : IViewModel<T> where T : IIndexedModel
{
    int Index { get; }
}