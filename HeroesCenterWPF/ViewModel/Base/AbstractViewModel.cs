namespace HeroesCenter.ViewModel.Base;

public abstract class AbstractViewModel : IViewModel
{
    private readonly Dictionary<string, HashSet<string>> _propertyChangedProviders;

    protected AbstractViewModel()
    {
        _propertyChangedProviders = PropertyHelper.ResolveDependents(GetType());
    }

    public event PropertyChangedEventHandler? PropertyChanged;

    [NotifyPropertyChangedInvocator]
    protected virtual void OnPropertyChanged([CallerMemberName] string? propertyName = null)
    {
        if (propertyName != null && _propertyChangedProviders.TryGetValue(propertyName, out var dependents))
        {
            foreach (string dependent in dependents)
            {
                RaisePropertyChanged(dependent);
            }
        }

        RaisePropertyChanged(propertyName);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public void RaisePropertyChanged(string? propertyName)
    {
        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }

    protected void RaisePropertyChanged(object? sender, PropertyChangedEventArgs e)
    {
        OnPropertyChanged(e.PropertyName);
    }

    public abstract GameVersion Version { get; }

    protected bool SetField<T>(ref T field, T value, [CallerMemberName] string? propertyName = null)
    {
        if (EqualityComparer<T>.Default.Equals(field, value)) return false;
        field = value;
        OnPropertyChanged(propertyName);
        return true;
    }
}

public abstract class AbstractViewModel<T> : AbstractViewModel, IViewModel<T> where T : IModel
{
    protected AbstractViewModel(T model)
    {
        Model = model;
    }

    public T Model { get; }

    public override GameVersion Version => Model.Version;

    public string StartAddress => Model.StartAddress;
    public string EndAddress => Model.EndAddress;
    public string AddressRange => Model.AddressRange;
}

public abstract class AbstractIndexedViewModel<T> : AbstractViewModel<T>, IIndexedViewModel<T> where T : class, IIndexedModel
{
    protected AbstractIndexedViewModel(T model) : base(model)
    {
    }

    public int Index => Model.Index;

    public byte[] DataBlock
    {
        get => Model.DataBlock;
        set => Model.DataBlock = value;
    }

    public Dictionary<int, byte> GetDataBlockMap()
    {
        var index = 0;
        var dictionary = Model.DataBlock.ToDictionary(item => index++);
        return dictionary;
    }
}