using System.Diagnostics.CodeAnalysis;

namespace HeroesCenter.ViewModel.Base;

public abstract class AbstractMapViewModel<TKey, TValue> : AbstractViewModel, IMapViewModel<TKey, TValue>
    where TKey : notnull
    where TValue : class, IIndexedViewModel<IIndexedModel>
{
    protected AbstractMapViewModel(GameVersion version, IEnumerable<TValue> childViewModels, Func<TValue, TKey> keyGetter)
    {
        Version = version;
        Map = childViewModels.ToDictionary(keyGetter.Invoke);
    }

    protected AbstractMapViewModel(GameVersion version, Func<GameVersion, IEnumerable<TValue>> factory, Func<TValue, TKey> keyGetter) : this(version,
        factory.Invoke(version), keyGetter)
    {
    }

    protected IReadOnlyDictionary<TKey, TValue> Map { get; }

    public bool ContainsKey(TKey key)
    {
        return Map.ContainsKey(key);
    }

    public bool TryGetValue(TKey key, [MaybeNullWhen(false)] out TValue value)
    {
        return Map.TryGetValue(key, out value);
    }

    public TValue this[TKey key] => Get(key);
    public IEnumerable<TKey> Keys => Map.Keys;

    public IEnumerable<TValue> Values => Map.Values;

    public TValue Get(TKey key)
    {
        return GetOrDefault(key) ?? throw new IndexOutOfRangeException($"Key {key} not found");
    }

    public TValue? GetOrDefault(TKey key) => Map.TryGetValue(key, out var value) ? value : default;

    public virtual IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
    {
        return Map.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return ((IEnumerable) Map).GetEnumerator();
    }

    public int Count => Map.Count;
    public sealed override GameVersion Version { get; }
}