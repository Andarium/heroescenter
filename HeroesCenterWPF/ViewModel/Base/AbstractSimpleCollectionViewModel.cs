namespace HeroesCenter.ViewModel.Base;

public abstract class AbstractCollectionViewModel<T1, T2, T3> : AbstractViewModel<T2>, ICollectionViewModel<T1, T3>
    where T1 : class, IIndexedModel
    where T2 : ICollectionModel<T1>
    where T3 : class, IIndexedViewModel<T1>
{
    protected AbstractCollectionViewModel(T2 model, Func<T1, T3> factory) : base(model)
    {
        Collection = model.Select(factory.Invoke).ToList();
    }

    public IReadOnlyList<T3> Collection { get; }

    public virtual T3 this[int i] => Get(i);

    public T3 Get(int i)
    {
        return GetOrDefault(i) ?? throw new IndexOutOfRangeException($"Index {i} out of {Count} elements");
    }

    public T3? GetOrDefault(int i) => Collection.ElementAtOrDefault(i);

    public virtual IEnumerator<T3> GetEnumerator()
    {
        return Collection.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return ((IEnumerable) Collection).GetEnumerator();
    }

    public int Count => Collection.Count;
}

public abstract class AbstractSimpleCollectionViewModel<T> : AbstractViewModel, ISimpleCollectionViewModel<T>
    where T : class, IIndexedViewModel<IIndexedModel>
{
    protected AbstractSimpleCollectionViewModel(GameVersion version, IEnumerable<T> childViewModels)
    {
        Version = version;
        Collection = childViewModels.ToList();
    }

    protected AbstractSimpleCollectionViewModel(GameVersion version, Func<GameVersion, IEnumerable<T>> factory) : this(version, factory.Invoke(version))
    {
    }

    protected IReadOnlyList<T> Collection { get; }

    public T this[int index] => Get(index);

    public T Get(int i)
    {
        return GetOrDefault(i) ?? throw new IndexOutOfRangeException($"Index {i} out of {Count} elements");
    }

    public T? GetOrDefault(int i) => Collection.ElementAtOrDefault(i);

    public virtual IEnumerator<T> GetEnumerator()
    {
        return Collection.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return ((IEnumerable) Collection).GetEnumerator();
    }

    public int Count => Collection.Count;
    public sealed override GameVersion Version { get; }

    public IReadOnlyList<T> Items => Collection;
}