namespace HeroesCenter.ViewModel.Base;

public interface ICollectionViewModel<out T1, out T2> : IReadOnlyList<T2>
    where T1 : class, IIndexedModel
    where T2 : class, IIndexedViewModel<T1>
{
}