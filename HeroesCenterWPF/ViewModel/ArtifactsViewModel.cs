namespace HeroesCenter.ViewModel;

public class ArtifactsViewModel : AbstractSimpleCollectionViewModel<ArtifactViewModel>
{
    public ArtifactsViewModel(GameVersion version, int heroIndex) : base(version, CreateArtifacts(version, heroIndex))
    {
        Seals = new SealsModel(version, heroIndex, this);
        foreach (var model in Collection)
        {
            model.Seals = Seals;
        }

        Backpack = Collection.Skip(19).ToList();
    }

    public SealsModel Seals { get; }
    public IReadOnlyList<ArtifactViewModel> Backpack { get; }

    private static IEnumerable<ArtifactViewModel> CreateArtifacts(GameVersion version, int parentIndex)
    {
        return Enumerable.Range(0, 19 + 64).Select(x => new ArtifactViewModel(version, parentIndex, x));
    }

    public void RemoveAllSeals()
    {
        Seals.RemoveAllSeals();
        foreach (var artifact in Collection.Take(19))
        {
            artifact.RaisePropertyChanged(string.Empty);
        }
    }

    public void SortBackpack()
    {
        var original = Backpack
            .Where(x => !x.IsEmpty)
            .ToList();

        var sorted = original
            .OrderBy(x => x.Category)
            .ThenBy(x => x.Type)
            .Select(x=>new { x.Type, x.Data })
            .ToList();

        for (var i = 0; i < sorted.Count; i++)
        {
            var o = original[i];
            var s = sorted[i];
            o.SetValues(s.Type, s.Data);
        }
    }

    #region Wrappers

    public ArtifactViewModel Head => this[0];

    public ArtifactViewModel Shoulders => this[1];

    public ArtifactViewModel Neck => this[2];

    public ArtifactViewModel RightArm => this[3];

    public ArtifactViewModel LeftArm => this[4];

    public ArtifactViewModel Body => this[5];

    public ArtifactViewModel RightRing => this[6];

    public ArtifactViewModel LeftRing => this[7];

    public ArtifactViewModel Legs => this[8];

    public ArtifactViewModel Slot1 => this[9];

    public ArtifactViewModel Slot2 => this[10];

    public ArtifactViewModel Slot3 => this[11];

    public ArtifactViewModel Slot4 => this[12];

    public ArtifactViewModel Ballista => this[13];

    public ArtifactViewModel Cart => this[14];

    public ArtifactViewModel Tent => this[15];

    public ArtifactViewModel Catapult => this[16];

    public ArtifactViewModel MagicBook => this[17];

    public ArtifactViewModel Slot5 => this[18];

    public ArtifactViewModel Backpack1 => this[19];

    public ArtifactViewModel Backpack2 => this[20];

    public ArtifactViewModel Backpack3 => this[21];

    public ArtifactViewModel Backpack4 => this[22];

    public ArtifactViewModel Backpack5 => this[23];

    #endregion
}