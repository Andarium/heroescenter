namespace HeroesCenter.ViewModel;

public class CreatureViewModel : AbstractIndexedViewModel<CreatureModel>
{
    public CreatureViewModel(UnitSettings unitSettings, GameVersion version) : base(new CreatureModel(version, unitSettings.UnitType))
    {
        _unitSettings = unitSettings;
    }

    public override string ToString()
    {
        return $"UnitType: {UnitType}, Name: {SingleName}, Level: {Level}, Town: {Town}";
    }

    private readonly UnitSettings _unitSettings;

    public int UnitType => _unitSettings.UnitType;
    public bool IsAvailable => _unitSettings.IsAvailable;

    public ImageSource Image => ImageHelper.GetImage(Version, ImageType.Unit, UnitType);

    public string SingleName
    {
        get => Model.SingleName;
        set => Model.SingleName = value;
    }

    public string PluralName
    {
        get => Model.PluralName;
        set => Model.PluralName = value;
    }

    public TownType Town
    {
        get => (TownType) Model.Town;
        set => Model.Town = (int) value;
    }

    public int Level
    {
        get => Model.Level;
        set => Model.Level = value;
    }

    public CreatureFlags Flags
    {
        get => (CreatureFlags) Model.Flags;
        set => Model.Flags = (uint) value;
    }

    public int Wood
    {
        get => Model.Wood;
        set => Model.Wood = value;
    }

    public int Mercury
    {
        get => Model.Mercury;
        set => Model.Mercury = value;
    }

    public int Ore
    {
        get => Model.Ore;
        set => Model.Ore = value;
    }

    public int Sulfur
    {
        get => Model.Sulfur;
        set => Model.Sulfur = value;
    }

    public int Crystal
    {
        get => Model.Crystal;
        set => Model.Crystal = value;
    }

    public int Gems
    {
        get => Model.Gems;
        set => Model.Gems = value;
    }

    public int Gold
    {
        get => Model.Gold;
        set => Model.Gold = value;
    }

    public int FightValue
    {
        get => Model.FightValue;
        set => Model.FightValue = value;
    }

    public int AiValue
    {
        get => Model.AiValue;
        set => Model.AiValue = value;
    }

    public int Growth
    {
        get => Model.Growth;
        set => Model.Growth = value;
    }

    public int Health
    {
        get => Model.Health;
        set => Model.Health = value;
    }

    public int Speed
    {
        get => Model.Speed;
        set => Model.Speed = value;
    }

    public int Attack
    {
        get => Model.Attack;
        set => Model.Attack = value;
    }

    public int Defence
    {
        get => Model.Defence;
        set => Model.Defence = value;
    }

    public int MinDamage
    {
        get => Model.MinDamage;
        set => Model.MinDamage = value;
    }

    public int MaxDamage
    {
        get => Model.MaxDamage;
        set => Model.MaxDamage = value;
    }

    public int Shots
    {
        get => Model.Shots;
        set => Model.Shots = value;
    }

    public int Spells
    {
        get => Model.Spells;
        set => Model.Spells = value;
    }

    #region Traits

    public bool FireImmunity
    {
        get => Flags.HasFlag(CreatureFlags.FireImmunity);
        set => Flags = Flags.SetFlag(CreatureFlags.FireImmunity, value);
    }

    public bool MindImmunity
    {
        get => Flags.HasFlag(CreatureFlags.MindImmunity);
        set => Flags = Flags.SetFlag(CreatureFlags.MindImmunity, value);
    }

    public bool DoubleStrike
    {
        get => Flags.HasFlag(CreatureFlags.DoubleStrike);
        set => Flags = Flags.SetFlag(CreatureFlags.DoubleStrike, value);
    }

    public bool Flight
    {
        get => Flags.HasFlag(CreatureFlags.Flight);
        set => Flags = Flags.SetFlag(CreatureFlags.Flight, value);
    }

    public bool NoRetaliation
    {
        get => Flags.HasFlag(CreatureFlags.NoRetaliation);
        set => Flags = Flags.SetFlag(CreatureFlags.NoRetaliation, value);
    }

    public bool AttackAdjacent
    {
        get => Flags.HasFlag(CreatureFlags.AttackAdjacent);
        set => Flags = Flags.SetFlag(CreatureFlags.AttackAdjacent, value);
    }

    public bool IsDragon
    {
        get => Flags.HasFlag(CreatureFlags.Dragon);
        set => Flags = Flags.SetFlag(CreatureFlags.Dragon, value);
    }

    public bool NoMorale
    {
        get => Flags.HasFlag(CreatureFlags.NoMorale);
        set => Flags = Flags.SetFlag(CreatureFlags.NoMorale, value);
    }

    public bool IsLiving
    {
        get => Flags.HasFlag(CreatureFlags.Living);
        set => Flags = Flags.SetFlag(CreatureFlags.Living, value);
    }

    public bool IsUndead
    {
        get => Flags.HasFlag(CreatureFlags.Undead);
        set => Flags = Flags.SetFlag(CreatureFlags.Undead, value);
    }

    public bool Shooter
    {
        get => Flags.HasFlag(CreatureFlags.Shooter);
        set => Flags = Flags.SetFlag(CreatureFlags.Shooter, value);
    }

    public bool Breath
    {
        get => Flags.HasFlag(CreatureFlags.Breath);
        set => Flags = Flags.SetFlag(CreatureFlags.Breath, value);
    }

    public bool DoubleWide
    {
        get => Flags.HasFlag(CreatureFlags.DoubleWide);
        set => Flags = Flags.SetFlag(CreatureFlags.DoubleWide, value);
    }

    public bool NoMeleePenalty
    {
        get => Flags.HasFlag(CreatureFlags.NoMeleePenalty);
        set => Flags = Flags.SetFlag(CreatureFlags.NoMeleePenalty, value);
    }

    public string FlagsString => Flags.ToString();

    #endregion
}