namespace HeroesCenter.ViewModel;

public class UIViewModel : AbstractViewModel<UIModel>
{
    public UIViewModel(GameVersion version) : base(new UIModel(version))
    {
    }

    public byte ViewX => Model.ViewX;
    public byte ViewY => Model.ViewY;
    public byte ViewZ => Convert(Model.ViewZ);

    public byte CursorCellX => Model.CursorCellX;
    public byte CursorCellY => Model.CursorCellY;
    public byte CursorCellZ => Model.CursorCellZ;

    private static byte Convert(byte input)
    {
        return (input == 0 ? 0 : 1).ToByte();
    }
}