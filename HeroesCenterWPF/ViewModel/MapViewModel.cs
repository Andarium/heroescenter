namespace HeroesCenter.ViewModel;

public class MapViewModel : AbstractViewModel<MapModel>
{
    public MapViewModel(GameVersion version) : base(new MapModel(version))
    {
    }

    [DependsOn(nameof(DayIndex))]
    public short CurrentDay
    {
        get => Model.CurrentDay;
        set
        {
            Model.CurrentDay = value;
            OnPropertyChanged();
        }
    }

    [DependsOn(nameof(DayIndex))]
    public short CurrentWeek
    {
        get => Model.CurrentWeek;
        set
        {
            Model.CurrentWeek = value;
            OnPropertyChanged();
        }
    }

    [DependsOn(nameof(DayIndex))]
    public short CurrentMonth
    {
        get => Model.CurrentMonth;
        set
        {
            Model.CurrentMonth = value;
            OnPropertyChanged();
        }
    }

    [DependsOn(nameof(CurrentDay))]
    [DependsOn(nameof(CurrentWeek))]
    [DependsOn(nameof(CurrentMonth))]
    public short DayIndex
    {
        get => (short) ((CurrentMonth - 1) * 28 + (CurrentWeek - 1) * 7 + CurrentDay - 1);
        set => SetDayIndex(value);
    }

    private void SetDayIndex(short value)
    {
        Model.CurrentMonth = (short) (value / 28 + 1);
        value %= 28;
        Model.CurrentWeek = (short) (value / 7 + 1);
        value %= 7;
        Model.CurrentDay = (short) (value + 1);
        OnPropertyChanged(nameof(DayIndex));
    }

    /// <summary>
    ///     Возвращает максимальный уровень героев на текущей карте
    /// </summary>
    public byte LevelCap
    {
        get => Model.LevelCap;
        set => Model.LevelCap = value;
    }
}