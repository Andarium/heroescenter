namespace HeroesCenter.ViewModel;

public class HeroManager : AbstractSimpleCollectionViewModel<HeroViewModel>
{
    public event Action<string?> CurrentHeroPropertyChanged = delegate { };

    private Flags _currentFlags = Flags.None;

    public HeroManager(GameVersion version) : base(version, CreateHeroes)
    {
        CollectionView = (ListCollectionView) CollectionViewSource.GetDefaultView(Collection);
        CollectionView.Filter = FlagFilter;
        CollectionView.CurrentChanged += CurrentHeroChanged;

        foreach (var hero in Collection)
        {
            hero.PropertyChanged += OnHeroPropertyChanged;
        }
    }

    private void OnHeroPropertyChanged(object? sender, PropertyChangedEventArgs e)
    {
        if (sender == CurrentHero)
        {
            CurrentHeroPropertyChanged.Invoke(e.PropertyName);
        }
    }

    private static IEnumerable<HeroViewModel> CreateHeroes(GameVersion version)
    {
        int count = version.GetAllHeroesCount();
        return Enumerable.Range(0, count).Select(x => new HeroViewModel(version, x));
    }

    public HeroViewModel? CurrentHero => CollectionView.CurrentItem as HeroViewModel;

    public int CurrentHeroIndex => CurrentHero?.Index ?? -1;

    public int CurrentHeroPortraitIndex => CurrentHero?.Portrait ?? -1;

    public ListCollectionView CollectionView { get; }

    public void ChangeVisibility(Flags flags, bool force = false)
    {
        if (flags == _currentFlags && !force)
        {
            return;
        }

        _currentFlags = flags;

        CollectionView.Refresh();
    }

    private bool FlagFilter(object input)
    {
        if (_currentFlags == Flags.All)
        {
            return true;
        }

        if (input is HeroViewModel viewModel)
        {
            byte heroFlag = viewModel.PrimarySkills.Flag;
            if (_currentFlags == Flags.FlagsOnly)
            {
                return heroFlag <= 7;
            }

            return heroFlag == (byte) _currentFlags;
        }

        return false;
    }

    private void CurrentHeroChanged(object? sender, EventArgs e)
    {
        OnPropertyChanged(nameof(CurrentHero));
    }

    public override IEnumerator<HeroViewModel> GetEnumerator()
    {
        return CollectionView.Cast<HeroViewModel>().GetEnumerator();
    }
}