namespace HeroesCenter.ViewModel;

public sealed class SpellBookViewModel : AbstractSimpleCollectionViewModel<SpellViewModel>
{
    public SpellBookViewModel(GameVersion version, int heroIndex) : base(version, CreateArtifacts(version, heroIndex))
    {
    }

    private static IEnumerable<SpellViewModel> CreateArtifacts(GameVersion version, int parentIndex)
    {
        return Enumerable.Range(0, HeroStats.SpellsLearned.Size).Select(x => new SpellViewModel(version, parentIndex, x));
    }
}