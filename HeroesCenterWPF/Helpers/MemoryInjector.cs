using static HeroesCenter.Native.NativeMethods;

namespace HeroesCenter.Helpers;

public static class MemoryInjector
{
    // private static IntPtr _offenseStartAddress;
    // private static IntPtr _offenseBodyAddress;
    // private static IntPtr _offenseEndAddress;

    //004E4551

    // 83 78 04 16 75 22 original

    // Heroes3.exe + D9166 intelligence




    // Heroes3.exe + E4408 archery
    // Heroes3.exe + E4551 offense
    // Heroes3.exe + E45B1 defense
    // Heroes3.exe + E4984 resistance
    // Heroes3.exe + E4B51 intelligence
    // Heroes3.exe + E4F06 logistic
    // Heroes3.exe + E5B49 sorcery

    // privileges
    private const int PROCESS_CREATE_THREAD = 0x0002;
    private const int PROCESS_QUERY_INFORMATION = 0x0400;
    private const int PROCESS_VM_OPERATION = 0x0008;
    private const int PROCESS_VM_WRITE = 0x0020;
    private const int PROCESS_VM_READ = 0x0010;

    private const int DesiredAccess =
        PROCESS_CREATE_THREAD | PROCESS_QUERY_INFORMATION | PROCESS_VM_OPERATION | PROCESS_VM_WRITE | PROCESS_VM_READ | 0x001F0FFF;

    // used for memory allocation
    private const uint MEM_COMMIT = 0x00001000;

    private const uint MEM_RESERVE = 0x00002000;

    // private const UInt32 PAGE_READWRITE = 4;
    private const uint PAGE_EXECUTE_READWRITE = 0x40;

    private sealed class CheckBlock
    {
        public readonly IntPtr Start;
        public readonly IntPtr Body;
        public readonly IntPtr End;
        public readonly IntPtr External;
        // public readonly bool IsAllocated;
        public readonly byte[] DefaultComparison;
        public readonly byte Register;

        public CheckBlock(IntPtr procHandle, IntPtr start, IntPtr external)
        {
            Start = start;
            Body = start + 6;
            End = GetEndAddress(procHandle, start);
            External = external;
            DefaultComparison = GetDefaultComparison(procHandle, start, external);
            Register = GetRegister(procHandle, Body);
            // IsAllocated = GetOrAllocCheatBlock(procHandle, start, out End, out External, out DefaultComparison);
        }
    }

    public static void main(byte heroIndex)
    {
        var p = MemoryWorker.Process;

        if (p == null)
        {
            return;
        }

        var pid = p.Id;
        var procHandle = OpenProcess(DesiredAccess, false, pid);


        var baseAddress = p.MainModule!.BaseAddress;


        var isAllocated = GetOrAllocCheatBlock0(procHandle, baseAddress + 0xE4408, out IntPtr external); // archery

        var genOffset = 22;


        // Heroes3.exe + E4408 archery
        // Heroes3.exe + E4551 offense
        // Heroes3.exe + E45B1 defense
        // Heroes3.exe + E4984 resistance
        // Heroes3.exe + E4B51 intelligence
        // Heroes3.exe + E4F06 logistic
        // Heroes3.exe + E5B49 sorcery
        SetBlock(0xE4408, 0); // archery
        SetBlock(0xE4551, 1); // offense
        SetBlock(0xE45B1, 2); // armory
        SetBlock(0xE4984, 3); // resistance
        SetBlock(0xE4B51, 4); // intelligence
        SetBlock(0xE4F06, 5); // logistic
        SetBlock(0xE5B49, 6); // sorcery


        MessageBox.Show($"{(isAllocated? "New" : "Old")}: 0x{external:X}");

        CloseHandle(procHandle);

        void SetBlock(int startOffset, int index)
        {
            var block = new CheckBlock(procHandle, baseAddress + startOffset, external + index * genOffset);

            var externalCode = GenerateExternalCode(block, heroIndex);
            var replaceCode = GenerateReplaceCode(block);

            WriteProcessMemory(procHandle, block.Start, replaceCode, new IntPtr(replaceCode.Length), out _);
            WriteProcessMemory(procHandle, block.External, externalCode, new IntPtr(externalCode.Length), out _);
        }
    }


    private static bool GetOrAllocCheatBlock0(IntPtr procHandle, IntPtr startAddress, out IntPtr external)
    {
        var expected = new byte[] { 0x83, 0x78 };
        var buffer = new byte[5];
        ReadProcessMemory(procHandle, startAddress, buffer, new IntPtr(buffer.Length), out _);
        bool isAllocated;


        const uint allocatedSize = 256;
        if (IsSame(buffer.Take(expected.Length).ToArray(), expected))
        {
            external = VirtualAllocEx(procHandle, IntPtr.Zero, allocatedSize, MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE);
            isAllocated = true;
        }
        else
        {
            var offset = BitConverter.ToInt32(buffer.Skip(1).Take(4).ToArray());
            external = startAddress + offset + 5;
            // var empty = Enumerable.Repeat((byte) 0, (int) allocatedSize).ToArray();
            // WriteProcessMemory(procHandle, external, empty, new IntPtr(empty.Length), out _);
            isAllocated = false;
        }

        return isAllocated;
    }


    /*private static Boolean GetOrAllocCheatBlock(IntPtr procHandle, IntPtr startAddress, out IntPtr endAddress, out IntPtr external, out Byte[] defaultComparison)
    {
        var expected = new Byte[] { 0x83, 0x78, 0x04, 0x16, 0x75, 0x28 };
        var buffer = new Byte[expected.Length];
        ReadProcessMemory(procHandle, startAddress, buffer, new IntPtr(expected.Length), out _);
        bool isAllocated;

        if (IsSame(buffer, expected))
        {
            const UInt32 allocatedSize = 1024;
            external = VirtualAllocEx(procHandle, IntPtr.Zero, allocatedSize, MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE);
            isAllocated = true;
        }
        else
        {
            var offset = BitConverter.ToInt32(buffer.Skip(1).Take(4).ToArray());
            external = startAddress + offset + 5;
            isAllocated = false;
            ReadProcessMemory(procHandle, external, buffer, new IntPtr(4), out _);
        }

        defaultComparison = buffer.Take(4).ToArray();
        ReadProcessMemory(procHandle, startAddress - 1, buffer, new IntPtr(1), out _);
        endAddress = startAddress + buffer[0];
        return isAllocated;
    }*/

    private static IntPtr GetEndAddress(IntPtr procHandle, IntPtr startAddress)
    {
        var buffer = new byte[1];
        ReadProcessMemory(procHandle, startAddress - 1, buffer, new IntPtr(1), out _);
        return startAddress + buffer[0];
    }

    private static byte[] GetDefaultComparison(IntPtr procHandle, IntPtr startAddress, IntPtr external)
    {
        var expected = new byte[] { 0x83, 0x78 };
        var buffer = new byte[4];
        ReadProcessMemory(procHandle, startAddress, buffer, new IntPtr(buffer.Length), out _);

        if (!IsSame(buffer.Take(2).ToArray(), expected))
        {
            ReadProcessMemory(procHandle, external, buffer, new IntPtr(buffer.Length), out _);
        }

        return buffer;
    }

    private static byte GetRegister(IntPtr procHandle, IntPtr body)
    {
        var buffer = new byte[3];
        ReadProcessMemory(procHandle, body, buffer, new IntPtr(buffer.Length), out _);
        var originalRegister = buffer[2];

        return originalRegister switch
        {
            0x41 => 0x79, // ecx
            0x53 => 0x7B, // ebx
            0x56 => 0x76, // esi
            0x57 => 0x77, // edi
            _ => originalRegister
        };
    }

    private static bool IsSame(byte[] first, byte[] second)
    {
        if (first.Length != second.Length)
        {
            return false;
        }

        for (int i = 0; i < first.Length; i++)
        {
            if (first[i] != second[i])
            {
                return false;
            }
        }

        return true;
    }

    private static byte[] GenerateReplaceCode(CheckBlock block)
    {
        var result = new List<byte>();

        result.AddRange(JmpRelCode(block.Start, block.External));

        while (result.Count < 6)
        {
            result.Add(0x90);
        }

        return result.ToArray();
    }

    private static byte[] GenerateExternalCode(CheckBlock block, byte heroIndex)
    {
        var result = new List<byte>();

        // result.AddRange(JmpNotEqualCode(0x13)); // skip exiting if equal
        // result.AddRange(JmpCode(end)); // exiting
        result.AddRange(block.DefaultComparison); // 4 bytes
        result.AddRange(JmpEqualCode(5 + 4 + 2)); // skip exiting if equal, offset next comparison

        // var newComparison = defaultComparison.ToArray();
        // newComparison[^1] = heroIndex;
        var newComparison = new byte[] { 0x83, block.Register, 0x1A, heroIndex };
        result.AddRange(newComparison);
        result.AddRange(JmpEqualCode(5)); // skip exiting if equal
        result.AddRange(JmpRelCode(block.External + result.Count, block.End)); // exiting
        result.AddRange(JmpRelCode(block.External + result.Count, block.Body)); // continue default algorithm

        return result.ToArray();
    }

    /*// 5 bytes
    private static byte[] MovCode(IntPtr dest)
    {
        var result = new Byte[5];

        result[0] = 0xB8;
        var addr = dest.ToInt32().ToByteArray();

        for (int i = 0; i < 4; i++)
        {
            result[i + 1] = addr[i];
        }

        return result;
    }*/

    // 7 bytes
    /*private static byte[] JmpCode(IntPtr dest)
    {
        var result = new Byte[7];

        result[0] = 0xEA;
        var addr = dest.ToInt32().ToByteArray();

        for (int i = 0; i < 4; i++)
        {
            result[i + 1] = addr[i];
        }

        return result;
    }*/

    private static byte[] JmpRelCode(IntPtr source, IntPtr dest)
    {
        var result = new byte[5];

        result[0] = 0xE9;

        source += 5;

        var offset = dest.ToInt32() - source.ToInt32();

        var offsetArray = offset.ToByteArray();

        // var addr = dest.ToInt32().ToByteArray();

        for (int i = 0; i < 4; i++)
        {
            result[i + 1] = offsetArray[i];
        }

        return result;
    }

    private static byte[] JmpEqualCode(byte offset) => new byte[] { 0x74, offset };
    private static byte[] JmpNotEqualCode(byte offset) => new byte[] { 0x75, offset };
}