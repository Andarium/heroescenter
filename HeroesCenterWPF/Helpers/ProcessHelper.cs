namespace HeroesCenter.Helpers;

internal static class ProcessHelper
{
    [DllImport("user32.dll")]
    private static extern IntPtr GetForegroundWindow();

    [DllImport("user32.dll", SetLastError = true)]
    private static extern uint GetWindowThreadProcessId(IntPtr hWnd, out uint lpdwProcessId);

    public static int GetForegroundProcessId()
    {
        var hWnd = GetForegroundWindow();
        GetWindowThreadProcessId(hWnd, out var processId);
        return Convert.ToInt32(processId);
    }

    public static Process GetForegroundProcess()
    {
        return Process.GetProcessById(GetForegroundProcessId());
    }

    public static Process[] GetProcesses(bool foregroundFirst = false)
    {
        var result = ProcessNamesHelper
            .Filter(Process.GetProcesses())
            .OrderBy(x => x.ProcessName);

        if (foregroundFirst)
        {
            var foregroundId = GetForegroundProcessId();
            result = result.ThenByDescending(x => x.Id == foregroundId);
        }

        return result.ToArray();
    }

    public static string[] GetProcessesNames(bool foregroundFirst = false)
    {
        return GetProcesses(foregroundFirst).Select(x => $"{x.ProcessName} ({x.Id})").ToArray();
    }
}