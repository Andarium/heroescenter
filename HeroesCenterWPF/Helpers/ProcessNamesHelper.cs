using System.Diagnostics.CodeAnalysis;
using static HeroesCenter.Core.GameVersion;

namespace HeroesCenter.Helpers;

public static class ProcessNamesHelper
{
    private static readonly Dictionary<GameVersion, string[]> VersionNames = new()
    {
        { ShadowOfDeath, new[] { "Heroes3" } },
        { InTheWakeOfGods, new[] { "h3era", "h3wog", "wog" } },
        { HornOfTheAbyss, new[] { "h3hota" } }
    };

    private static readonly string[] PossibleNames = VersionNames
        .SelectMany(x => x.Value)
        .Select(x => x.ToLowerInvariant())
        .ToArray();

    private static readonly IReadOnlyList<string> Exclude = new List<string>
    {
        "vshost",
        "opera",
        "chrome",
        "firefox",
        "wpf",
        "string",
        "converter",
        "heroescenter",
        "maped"
    };

    public static bool TryGetVersion(string processName, [NotNullWhen(true)] out GameVersion? version)
    {
        processName = processName.ToLowerInvariant();

        foreach (string exclude in Exclude)
        {
            if (processName.Contains(exclude, StringComparison.InvariantCultureIgnoreCase))
            {
                version = null;
                return false;
            }
        }

        foreach ((GameVersion gameVersion, string[] value) in VersionNames)
        {
            foreach (string versionName in value)
            {
                if (processName.Contains(versionName, StringComparison.InvariantCultureIgnoreCase))
                {
                    version = gameVersion;
                    return true;
                }
            }
        }

        version = null;
        return false;
    }

    public static string[] Filter(IEnumerable<string> processNames)
    {
        return processNames.Where(x => TryGetVersion(x, out _)).ToArray();
    }

    public static Process[] Filter(IEnumerable<Process> processNames)
    {
        return processNames.Where(x => TryGetVersion(x.ProcessName, out _)).ToArray();
    }
}