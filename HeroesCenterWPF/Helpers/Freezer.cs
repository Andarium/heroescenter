using System.Timers;

namespace HeroesCenter.Helpers;

public sealed class Freezer : IDisposable
{
    public const double TimerInterval = 25;

    private static Freezer? _instance;
    private readonly Timer _timer;
    private readonly List<ValueAction> _toRemove = new();

    private Dictionary<string, ValueAction> Actions { get; }

    private Freezer()
    {
        _timer = new Timer(TimerInterval);
        _timer.Elapsed += OnTimerElapsed;
        _timer.Start();

        Actions = new Dictionary<string, ValueAction>();
    }

    public static Freezer Instance => _instance ??= new Freezer();

    private void OnTimerElapsed(object? sender, ElapsedEventArgs e)
    {
        _toRemove.Clear();
        foreach (ValueAction action in Actions.Values)
        {
            if (!action.IsActionTime(_timer.Interval))
            {
                continue;
            }

            if (!action.PerformAction())
            {
                _toRemove.Add(action);
            }
        }

        foreach (var action in _toRemove)
        {
            Actions.Remove(action.ActionName);
        }
    }

    /// <summary>
    /// </summary>
    /// <param name="action"></param>
    /// <returns>Успешно ли добавление</returns>
    public bool AddAction(ValueAction action)
    {
        if (HasAction(action.ActionName))
        {
            Debug.WriteLine($"ActionName с именем '{action.ActionName}' уже существует.");
            return false;
        }

        Actions.Add(action.ActionName, action);
        return true;
    }

    public void RemoveAction(ValueAction action)
    {
        Actions.Remove(action.ActionName);
    }

    public void RemoveAction(string actionKey)
    {
        Actions.Remove(actionKey);
    }

    public bool HasAction(string actionKey)
    {
        return Actions.ContainsKey(actionKey);
    }

    public void Dispose()
    {
        _timer.Dispose();
    }
}