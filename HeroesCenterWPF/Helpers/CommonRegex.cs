namespace HeroesCenter.Helpers;

public static class CommonRegex
{
    public static readonly Regex EverythingButDigits = new("[^0-9]+", RegexOptions.Compiled);
    public static readonly Regex EverythingButDigitsAndMinus = new("[^0-9-]+", RegexOptions.Compiled);
}