namespace HeroesCenter.Helpers;

public static class JsonLoader
{
    public static T LoadSettings<T>(string jsonPath)
    {
        if (string.IsNullOrWhiteSpace(jsonPath))
        {
            throw new ArgumentException("Value cannot be null or whitespace.", nameof(jsonPath));
        }

        var settings = CustomJsonSerializer.Deserialize<T>(jsonPath.LoadText());

        if (settings == null)
        {
            throw new NullReferenceException($"Null {typeof(T).Name} settings. Path: {jsonPath}!");
        }

        return settings;
    }

    public static Dictionary<GameVersion, List<ArtifactInfo>> LoadArtifacts()
    {
        var stream = ResourceHelper.Text.Artifacts.GetStream();
        var text = new StreamReader(stream).ReadToEnd();

        var s = new JsonSerializerSettings
        {
            Converters = new List<JsonConverter> { new BooleanConverter() }
        };
        var map = JsonConvert.DeserializeObject<Dictionary<GameVersion, List<ArtifactInfo>>>(text, s)!;

        foreach (var pair in map)
        {
            foreach (ArtifactInfo info in pair.Value)
            {
                info.Setup(pair.Key);
            }
        }

        var sod = map[GameVersion.ShadowOfDeath];

        var scroll = sod.First(x => x.Type == 1);
        sod.Remove(scroll);

        for (var i = 0; i <= 70; i++)
        {
            sod.Add(new ArtifactInfo(scroll, i));
        }

        var wog = map[GameVersion.InTheWakeOfGods];
        var hota = map[GameVersion.HornOfTheAbyss];

        static object Selector(ArtifactInfo x) => x.Type;
        wog.AddRangeUnique(sod, Selector);
        wog.Sort();
        hota.AddRangeUnique(sod, Selector);
        hota.Sort();

        return map;
    }
}