namespace HeroesCenter.Helpers;

public static class ResourceHelper
{
    //Константа для формирования путей
    public const string UriPack = "pack://application:,,,/HeroesCenter;component";

    public static class Skills
    {
        public const string Primary = "/Resources/Skills/Primary/";
        public const string Secondary = "/Resources/Skills/Secondary/";

        public static readonly IReadOnlyList<string> PrimaryCollection = new[]
        {
            "attack",
            "defence",
            "spellpower",
            "knowledge",
            "experience",
            "mana",
            "movement"
        };
    }

    public static class Images
    {
        public const string Portraits = "/Resources/Portraits/";
        public const string Specializations = "/Resources/Specializations/";
        public const string Units = "/Resources/Units/";
        public const string Artifacts = "/Resources/Artifacts/";
        public const string Scrolls = "/Resources/Artifacts/Scrolls/";
        public const string Flags = "/Resources/Flags/";
        public const string Towns = "/Resources/Towns/";
        public const string Spells = "/Resources/Spells/";
    }

    public static class Text
    {
        public const string HeroSkills = "/Resources/Text/Skills.xml";
        public const string HeroClasses = "/Resources/Text/Classes.xml";
        public const string Artifacts = "/Resources/Text/Artifacts.json";
        public const string Spells = "/Resources/Text/Spells.xml";
    }

    public static class Settings
    {
        public const string PortraitMap = "/Resources/Settings/Portraits.json";
        public const string MemoryWorkerMap = "/Resources/Settings/MemoryWorkers.json";
        public const string Specializations = "/Resources/Settings/Specializations.json";
        public const string Units = "/Resources/Settings/Units.json";
    }

    #region Helpers-Extensions for paths/Uri

    public static string AbsolutePath(this string path)
    {
        return path.StartsWith(UriPack, StringComparison.Ordinal)
            ? path
            : string.Concat(UriPack, path);
    }

    public static Uri AbsoluteUri(this string path)
    {
        return path.StartsWith(UriPack, StringComparison.Ordinal)
            ? new Uri(path, UriKind.Absolute)
            : new Uri(string.Concat(UriPack, path), UriKind.Absolute);
    }

    //Выдает поток для доступа к ресурсам на основе относительного пути. Используется в загрузке xml
    public static Stream? GetStreamOrNull(this string relativePath)
    {
        try
        {
            var resStream = Application.GetResourceStream(relativePath.AbsoluteUri());
            return resStream?.Stream;
        }
        catch (Exception)
        {
            return null;
        }
    }

    public static Stream GetStream(this string relativePath)
    {
        return GetStreamOrNull(relativePath)!;
    }

    public static string LoadText(this string relativePath)
    {
        var uri = relativePath.AbsoluteUri();
        var resStream = Application.GetResourceStream(uri);

        if (resStream == null)
        {
            throw new ArgumentException(relativePath);
        }

        return new StreamReader(resStream.Stream).ReadToEnd();
    }

    #endregion
}