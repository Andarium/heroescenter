namespace HeroesCenter.Helpers.Keyboard;

[StructLayout(LayoutKind.Sequential)]
public struct LowLevelKeyboardInputEvent
{
    /// <summary>
    ///     A virtual-key code. The code must be a value in the range 1 to 254.
    /// </summary>
    public readonly int VirtualCode;

    /// <summary>
    ///     A hardware scan code for the key.
    /// </summary>
    public readonly int HardwareScanCode;

    /// <summary>
    ///     The extended-key flag, event-injected Flags, context code, and transition-state flag. This member is specified as follows. An application can use the following values to test the keystroke Flags. Testing LLKHF_INJECTED (bit 4) will tell you whether the event was injected. If it was, then testing LLKHF_LOWER_IL_INJECTED (bit 1) will tell you whether or
    ///     not the event was injected from a process running at lower integrity level.
    /// </summary>
    public readonly int Flags;

    /// <summary>
    ///     The time stamp stamp for this message, equivalent to what GetMessageTime would return for this message.
    /// </summary>
    public readonly int TimeStamp;

    /// <summary>
    ///     Additional information associated with the message.
    /// </summary>
    private readonly IntPtr AdditionalInformation;

    public Key KeyCode => KeyInterop.KeyFromVirtualKey(VirtualCode);

    public bool Equals(LowLevelKeyboardInputEvent other)
    {
        return VirtualCode == other.VirtualCode && HardwareScanCode == other.HardwareScanCode && Flags == other.Flags && TimeStamp == other.TimeStamp;
    }

    public override bool Equals(object? obj)
    {
        return obj is LowLevelKeyboardInputEvent other && Equals(other);
    }

    public override int GetHashCode()
    {
        unchecked
        {
            int hashCode = VirtualCode;
            hashCode = (hashCode * 397) ^ HardwareScanCode;
            hashCode = (hashCode * 397) ^ Flags;
            hashCode = (hashCode * 397) ^ TimeStamp;
            return hashCode;
        }
    }

    public static bool operator ==(LowLevelKeyboardInputEvent left, LowLevelKeyboardInputEvent right)
    {
        return left.Equals(right);
    }

    public static bool operator !=(LowLevelKeyboardInputEvent left, LowLevelKeyboardInputEvent right)
    {
        return !(left == right);
    }
}