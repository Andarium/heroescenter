namespace HeroesCenter.Helpers.Keyboard;

public enum KeyboardState
{
    KeyDown = 0x0100,
    KeyUp = 0x0101,
    KeyPress = 0x5001,
    SysKeyDown = 0x0104,
    SysKeyUp = 0x0105
}