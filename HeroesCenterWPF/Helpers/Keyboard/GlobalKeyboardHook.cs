namespace HeroesCenter.Helpers.Keyboard;

//Based on https://gist.github.com/Stasonix
public sealed class GlobalKeyboardHook : IDisposable
{
    private static readonly HashSet<int> PressedVirtualCodes = new();

    // ReSharper disable once InconsistentNaming
    public const int WH_KEYBOARD_LL = 13;
    private const int KfAltdown = 0x2000;
    public const int LlkhfAltdown = KfAltdown >> 8;
    private NativeMethods.HookProc? _hookProc;
    private IntPtr _user32LibraryHandle;

    private IntPtr _windowsHookHandle;

    public event EventHandler<GlobalKeyboardHookEventArgs>? KeyboardPressed;
    private readonly HashSet<Key> _registeredKeys;

    public GlobalKeyboardHook(IEnumerable<Key> registerKeys)
    {
        _registeredKeys = new HashSet<Key>(registerKeys);
        _windowsHookHandle = IntPtr.Zero;
        _user32LibraryHandle = IntPtr.Zero;
        _hookProc += LowLevelKeyboardProc; // we must keep alive _hookProc, because GC is not aware about SetWindowsHookEx behaviour.

        _user32LibraryHandle = NativeMethods.LoadLibrary("User32");
        if (_user32LibraryHandle == IntPtr.Zero)
        {
            int errorCode = Marshal.GetLastWin32Error();
            throw new Win32Exception(errorCode, $"Failed to load library 'User32.dll'. Error {errorCode}: {new Win32Exception(errorCode).Message}.");
        }

        _windowsHookHandle = NativeMethods.SetWindowsHookEx(WH_KEYBOARD_LL, _hookProc, _user32LibraryHandle, 0);
        if (_windowsHookHandle == IntPtr.Zero)
        {
            int errorCode = Marshal.GetLastWin32Error();
            string processName = Process.GetCurrentProcess().ProcessName;
            string message = new Win32Exception(errorCode).Message;
            throw new Win32Exception(errorCode, $"Failed to adjust keyboard hooks for '{processName}'. Error {errorCode}: {message}.");
        }
    }

    public void Dispose()
    {
        Dispose(true);
        GC.SuppressFinalize(this);
    }

    private void Dispose(bool disposing)
    {
        int errorCode = Marshal.GetLastWin32Error();
        if (disposing)
        {
            // because we can unhook only in the same thread, not in garbage collector thread
            if (_windowsHookHandle != IntPtr.Zero)
            {
                if (NativeMethods.UnhookWindowsHookEx(_windowsHookHandle))
                {
                    _windowsHookHandle = IntPtr.Zero;
                    _hookProc -= LowLevelKeyboardProc;
                }
                else
                {
                    Console.WriteLine($"Failed to remove keyboard hooks for '{Process.GetCurrentProcess().ProcessName}'. Error {errorCode}");
                }
            }
        }

        if (_user32LibraryHandle != IntPtr.Zero)
        {
            if (NativeMethods.FreeLibrary(_user32LibraryHandle))
            {
                _user32LibraryHandle = IntPtr.Zero;
            }
            else
            {
                Console.WriteLine($"Failed to unload library 'User32.dll'. Error {errorCode}");
            }
        }
    }

    ~GlobalKeyboardHook()
    {
        Dispose(false);
    }

    public IntPtr LowLevelKeyboardProc(int nCode, IntPtr wParam, IntPtr lParam)
    {
        var fEatKeyStroke = false;

        int wparamTyped = wParam.ToInt32();

        if (Enum.IsDefined(typeof(KeyboardState), wparamTyped))
        {
            var type = (KeyboardState) wparamTyped;
            var p = (LowLevelKeyboardInputEvent) Marshal.PtrToStructure(lParam, typeof(LowLevelKeyboardInputEvent))!;

            if (_registeredKeys.Contains(p.KeyCode))
            {
                var eventArguments = new GlobalKeyboardHookEventArgs(p, type);

                if (type == KeyboardState.KeyDown || type == KeyboardState.SysKeyDown)
                {
                    if (PressedVirtualCodes.Add(p.VirtualCode))
                    {
                        eventArguments = new GlobalKeyboardHookEventArgs(p, KeyboardState.KeyPress);
                        KeyboardPressed?.Invoke(this, eventArguments);
                    }
                }
                else if (type == KeyboardState.KeyUp || type == KeyboardState.SysKeyUp)
                {
                    PressedVirtualCodes.Remove(p.VirtualCode);
                }

                fEatKeyStroke = eventArguments.Handled;
            }
        }

        return fEatKeyStroke ? (IntPtr) 1 : NativeMethods.CallNextHookEx(IntPtr.Zero, nCode, wParam, lParam);
    }

    public static bool IsKeyDown(Key key)
    {
        int code = KeyInterop.VirtualKeyFromKey(key);
        return PressedVirtualCodes.Contains(code);
    }

    public static bool IsAnyKeyDown(params Key[] keys)
    {
        return keys.Any(IsKeyDown);
    }
}