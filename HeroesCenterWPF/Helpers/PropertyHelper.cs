namespace HeroesCenter.Helpers;

public static class PropertyHelper
{
    private static readonly Dictionary<Type, Dictionary<string, HashSet<string>>> Resolved = new();

    public static Dictionary<string, HashSet<string>> ResolveDependents(Type containingType)
    {
        if (Resolved.TryGetValue(containingType, out var result))
        {
            return result;
        }

        result = new Dictionary<string, HashSet<string>>();

        var inverted = containingType
            .GetProperties()
            .Where(x => x.GetCustomAttributes(typeof(DependsOnAttribute)).Any())
            .Select(x =>
                new KeyValuePair<string, IEnumerable<string>>(
                    x.Name,
                    x.GetCustomAttributes(typeof(DependsOnAttribute)).Cast<DependsOnAttribute>().Select(z => z.Name)));

        foreach (var pair in inverted)
        {
            foreach (string provider in pair.Value)
            {
                if (result.TryGetValue(provider, out var temp))
                {
                    temp.Add(pair.Key);
                }
                else
                {
                    result[provider] = new HashSet<string> { pair.Key };
                }
            }
        }

        Resolved[containingType] = result;
        return result;
    }
}