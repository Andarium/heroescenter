namespace HeroesCenter.Helpers;

public class TimeCounter : INotifyPropertyChanged
{
    private static TimeCounter? _instance;
    private int _counter;

    private TimeCounter()
    {
        T1 = new Stopwatch();
        T2 = new Stopwatch();
        T3 = new Stopwatch();
        _counter = 0;
        Messages = new List<string>();
    }

    public static TimeCounter Instance
    {
        get => _instance ??= new TimeCounter();
        set => _instance = value;
    }

    public Stopwatch T1 { get; }
    public Stopwatch T2 { get; }
    public Stopwatch T3 { get; }

    public int Counter
    {
        get => _counter;
        set => SetField(ref _counter, value);
    }

    public IList<string> Messages { get; }

    public void TimerChanged()
    {
        OnPropertyChanged(nameof(T1));
        OnPropertyChanged(nameof(T2));
        OnPropertyChanged(nameof(T3));
    }

    public event PropertyChangedEventHandler? PropertyChanged;

    protected virtual void OnPropertyChanged([CallerMemberName] string? propertyName = null)
    {
        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }

    protected bool SetField<T>(ref T field, T value, [CallerMemberName] string? propertyName = null)
    {
        if (EqualityComparer<T>.Default.Equals(field, value)) return false;
        field = value;
        OnPropertyChanged(propertyName);
        return true;
    }
}