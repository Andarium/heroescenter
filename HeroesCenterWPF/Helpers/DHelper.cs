namespace HeroesCenter.Helpers;

public static class DHelper
{
    public static DependencyProperty Register<TProperty, TClass>(string name)
    {
        return DependencyProperty.Register(name, typeof(TProperty), typeof(TClass), new PropertyMetadata(default(TProperty)));
    }

    public static DependencyProperty Register<TProperty, TClass>(string name, object? defaultValue)
    {
        return DependencyProperty.Register(name, typeof(TProperty), typeof(TClass), new PropertyMetadata(defaultValue));
    }

    public static DependencyProperty Register<TProperty, TClass>(string name, PropertyChangedCallback callback)
    {
        return DependencyProperty.Register(name, typeof(TProperty), typeof(TClass), new PropertyMetadata(callback));
    }
    public static DependencyProperty Register<TProperty, TClass>(string name, object? defaultValue, PropertyChangedCallback callback)
    {
        return DependencyProperty.Register(name, typeof(TProperty), typeof(TClass), new PropertyMetadata(defaultValue, callback));
    }
}