namespace HeroesCenter.Helpers;

public static class MouseHelper
{
    private const int WmNclbuttondown = 0xA1;
    private static readonly IntPtr HtCaption = new(0x2);
    public static int TitleHeight = 30, BorderHeight = 15;

    public static void DragWindow(IInputElement control, IntPtr handle, MouseEventArgs e)
    {
        double z = e.GetPosition(control).Y;
        if (z < BorderHeight || z > TitleHeight + BorderHeight) return;
        //ReleaseCapture();
        NativeMethods.SendMessage(handle, WmNclbuttondown, HtCaption, IntPtr.Zero);
    }
}