using static HeroesCenter.Data.ImageType;
using static HeroesCenter.Helpers.ResourceHelper.Images;

namespace HeroesCenter.Helpers;

public static class ImageHelper
{
    private const string PngExtension = ".png";
    private const string UnitPrefix = "unit_";
    private const string ArtPrefix = "art_";
    private const string ScrollPrefix = "scroll_";
    private const string FlagPrefix = "flag_";
    private const string SpecializationPrefix = "spec_";
    private const string SecondarySkillPrefix = "secskill_";
    private const string SpellPrefix = "spell_";
    private const string OldPostfix = "_old";
    private const string Delimiter = "_";

    private static readonly Dictionary<GameVersion, string> VersionSubFolder = new()
    {
        { GameVersion.ShadowOfDeath, "" },
        { GameVersion.InTheWakeOfGods, "wog/" },
        { GameVersion.HornOfTheAbyss, "hota/" },
    };

    static ImageHelper()
    {
        ColoredBackgrounds = Colorize(GetImage("/Resources/BlueTile.png"));
        ColoredInventory = Colorize(GetImage("/Resources/HeroInventory.png"));
        DefaultFlag = GetImage("/Resources/Flags/flag_255.png");
        ArtSlotImage = GetImage("/Resources/ArtSlot.png");
        DefaultBackground = ColoredBackgrounds.ElementAt(8);
        DefaultHeroInventory = ColoredInventory.ElementAt(8);
        UnitRemovalImage = GetImage(GameVersion.ShadowOfDeath, Unit, 122);
        NullImage = BitmapSource.Create(1, 1, 96, 96, PixelFormats.Bgra32, null, new byte[] { 0, 0, 0, 0 }, 4);
        LockImage = GetArtImage(145, 0, GameVersion.ShadowOfDeath);
    }

    public static readonly IList<BitmapSource> ColoredBackgrounds;
    public static readonly IList<BitmapSource> ColoredInventory;
    public static readonly BitmapSource DefaultBackground;
    public static readonly BitmapSource DefaultHeroInventory;
    public static readonly BitmapSource DefaultFlag;
    public static readonly BitmapSource UnitRemovalImage;
    public static readonly BitmapSource NullImage;
    public static readonly BitmapSource LockImage;
    public static readonly BitmapSource ArtSlotImage;

    private static readonly ConcurrentDictionary<GameVersion, ImageType, int, BitmapSource> Images = new();

    private static BitmapSource GetImage(string path, bool assert = true)
    {
        var result = CachedImages.GetImageOrNull(path.AbsolutePath());

        if (result == null)
        {
            if (assert)
            {
                throw new NullReferenceException($"Can't load image from {path.AbsolutePath()}");
            }

            result = NullImage;
        }

        return result;
    }

    public static BitmapSource GetImage(GameVersion version, ImageType type, int index, bool assert = true)
    {
        if (Images.TryGetValue(version, type, index, out BitmapSource? result))
        {
            return result;
        }

        string path;
        switch (type)
        {
            case PortraitBig:
            case PortraitSmall:
                string suffix = type == PortraitBig ? "_b" : "_s";
                path = Portraits + Settings.Instance.Portraits[version, index] + suffix + PngExtension;
                break;
            case Unit:
                var cs = Settings.Instance.Units[version, index];
                path = $"{Units}{cs.SpriteVersion}/{UnitPrefix}{cs.SpriteKey}{PngExtension}";
                break;
            case Spell:
                path = $"{Spells}{SpellPrefix}{index}{PngExtension}";
                break;
            case UnitTownBack:
            case PrimarySkill:
            case SecondarySkill:
            case Specialization:
            case Artifact:
            case Flag:
            default:
                throw new ArgumentOutOfRangeException(nameof(type), type, null);
        }

        BitmapSource image = GetImage(path, assert);
        Images[version, type, index] = image;
        return image;
    }

    public static BitmapSource GetPrimarySkillImage(int index, bool useNewImages = false)
    {
        string path = string.Concat(
            ResourceHelper.Skills.Primary,
            ResourceHelper.Skills.PrimaryCollection[index],
            useNewImages || index == 1 || index == 4 || index == 6
                ? string.Empty
                : OldPostfix,
            PngExtension);
        return GetImage(path);
    }

    public static BitmapSource GetSecondarySkillImage(int index, int stage)
    {
        string path = string.Concat(
            ResourceHelper.Skills.Secondary,
            SecondarySkillPrefix,
            index,
            Delimiter,
            MathEx.Clamp(stage, 0, 3),
            PngExtension);
        return GetImage(path);
    }

    public static BitmapSource GetSpecializationImage(int index)
    {
        string path = string.Concat(
            Specializations,
            SpecializationPrefix,
            index,
            PngExtension);
        return GetImage(path);
    }

    public static BitmapSource GetArtImage(int index, int data, GameVersion version)
    {
        if (index < 0)
        {
            return NullImage;
        }

        string path;
        if (index == 1 && data >= 0)
        {
            path = $"{Scrolls}{ScrollPrefix}{data}{PngExtension}";
        }
        else
        {
            path = $"{Artifacts}{VersionSubFolder[version]}{ArtPrefix}{index:D3}{PngExtension}";
        }

        return GetImage(path);
    }

    public static BitmapSource GetFlagImage(int index)
    {
        string path = string.Concat(
            ResourceHelper.Images.Flags,
            FlagPrefix,
            index,
            PngExtension);
        return GetImage(path);
    }

    public static BitmapSource GetTownImage(TownType town)
    {
        string path = string.Concat(
            Towns,
            town.ToString(),
            PngExtension);
        return GetImage(path);
    }

    public static BitmapSource GetBackgroundTile(byte? id)
    {
        var count = ColoredBackgrounds.Count;

        if (id == null || id >= count)
        {
            return ColoredBackgrounds[count - 1];
        }

        return ColoredBackgrounds[id.Value];
    }

    public static BitmapSource GetHeroInventory(byte? id)
    {
        var count = ColoredInventory.Count;

        if (id == null || id >= count)
        {
            return ColoredInventory[count - 1];
        }

        return ColoredInventory[id.Value];
    }

    public static IList<BitmapSource> Colorize(BitmapSource blueImage)
    {
        if (blueImage.Format.BitsPerPixel != 32 && blueImage.Format.BitsPerPixel != 24)
        {
            throw new ArgumentException($"Bad image format: bits per pixel {blueImage.Format.BitsPerPixel}");
        }

        int stride = blueImage.PixelWidth * blueImage.Format.BitsPerPixel;
        //Для WPF stride вычисляется так, выравнивание нужно лишь для GDI
        int bytesPerPixel = blueImage.Format.BitsPerPixel / 8;

        var pixelArray = new byte[9][];

        for (var i = 0; i < 9; i++)
        {
            pixelArray[i] = new byte[stride * blueImage.PixelHeight];
            blueImage.CopyPixels(pixelArray[i], stride, 0);
        }

        for (var i = 0; i < pixelArray[1].Length; i += bytesPerPixel)
        {
            byte b = pixelArray[1][i];
            byte g = pixelArray[1][i + 1];
            byte r = pixelArray[1][i + 2];
            //var a = pixelArray[1][i + 3];


            if (b <= r || b <= g)
            {
                continue;
            }

            //Красный
            //pixelArray[0][i + 3] = a;
            pixelArray[0][i + 2] = b;
            pixelArray[0][i + 1] = pixelArray[0][i] = (byte) (r >> 1);

            //Коричневый
            //pixelArray[2][i + 3] = a;
            pixelArray[2][i + 2] = b;
            pixelArray[2][i + 1] = (byte) (b / 4 * 3);
            pixelArray[2][i] = (byte) (b >> 1);

            //Зеленый
            //pixelArray[3][i + 3] = a;
            pixelArray[3][i + 2] = (byte) (r >> 1);
            pixelArray[3][i + 1] = (byte) (b / 4 * 3);
            pixelArray[3][i] = (byte) (r >> 1);

            //Оранжевый
            //pixelArray[4][i + 3] = a;
            pixelArray[4][i + 2] = (byte) (b * 4 / 3);
            pixelArray[4][i + 1] = (byte) (b / 3 * 2);
            pixelArray[4][i] = (byte) (r >> 1);

            //Фиолетовый
            //pixelArray[5][i + 3] = a;
            pixelArray[5][i + 2] = (byte) (b / 4 * 3);
            pixelArray[5][i + 1] = (byte) (r / 4 * 3);
            pixelArray[5][i] = b;

            //Бирюзовый
            //pixelArray[6][i + 3] = a;
            pixelArray[6][i + 2] = r;
            pixelArray[6][i + 1] = b;
            pixelArray[6][i] = b;

            //Розовый
            //pixelArray[7][i + 3] = a;
            pixelArray[7][i + 2] = (byte) (b * 4 / 3);
            pixelArray[7][i + 1] = (byte) (b / 3 * 2);
            pixelArray[7][i] = (byte) (b / 4 * 3);

            //Серый
            //pixelArray[8][i + 3] = a;
            pixelArray[8][i + 2] =
                pixelArray[8][i + 1] = pixelArray[8][i] = (byte) (b * 0.114f + g * 0.587f + r * 0.299f);
            //x = 0.299 R + 0.587 G + 0.114 B
        }

        var outputImages = new List<BitmapSource>(9);
        outputImages.AddRange(
            pixelArray
                .Select(bytes =>
                    BitmapSource.Create(
                        blueImage.PixelWidth,
                        blueImage.PixelHeight,
                        blueImage.DpiX,
                        blueImage.DpiY,
                        blueImage.Format,
                        blueImage.Palette,
                        bytes,
                        stride)));
        return outputImages;
    }
}