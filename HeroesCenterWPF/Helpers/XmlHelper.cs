

// ReSharper disable PossibleNullReferenceException

namespace HeroesCenter.Helpers;

public static class XmlHelper
{
    public static void LoadSkills(
        out IReadOnlyList<string> primary,
        out IReadOnlyList<string> secondary,
        out IReadOnlyList<string> stages)
    {
        XDocument doc = XDocument.Load(ResourceHelper.Text.HeroSkills.GetStream());
        primary = doc
            .Descendants("PrimarySkills")
            .First()
            .Descendants()
            .Select(x => x.Value)
            .ToList();

        secondary = doc
            .Descendants("SecondarySkills")
            .First()
            .Descendants()
            .Select(x => x.Value)
            .ToList();

        stages = doc
            .Descendants("SkillStages")
            .First()
            .Descendants()
            .Select(x => x.Value)
            .ToList();
    }

    public static IReadOnlyList<string> LoadClasses()
    {
        return XDocument
            .Load(ResourceHelper.Text.HeroClasses.GetStream())
            .Descendants("Entry")
            .Select(x => x.Value)
            .ToList();
    }


    public static IReadOnlyDictionary<int, SpellInfo> LoadSpells()
    {
        const string path = ResourceHelper.Text.Spells;
        Stream? serialized = path.GetStream();

        if (serialized == null)
        {
            throw new NullReferenceException($"Can't load stream {path}");
        }

        return CustomXmlSerializer.DeserializeSpells(serialized);
    }
}