namespace HeroesCenter.Helpers;

public static class AssemblyInfoHelper
{
    static AssemblyInfoHelper()
    {
        var assembly = Assembly.GetExecutingAssembly();
        AssemblyName assemblyName = assembly.GetName();
        Version = assemblyName.Version?.ToString() ?? string.Empty;
        Name = assemblyName.Name ?? string.Empty;
        Company = GetAttribute<AssemblyCompanyAttribute>().Company;
        Title = GetAttribute<AssemblyTitleAttribute>().Title;
        FullName = $"{Title} v.{Version}";

        T GetAttribute<T>()
        {
            var attribute = Attribute.GetCustomAttribute(assembly, typeof(T), false);
            return (T) Convert.ChangeType(attribute, typeof(T))!;
        }
    }

    public static readonly string Version;
    public static readonly string Name;
    public static readonly string Title;
    public static readonly string Company;
    public static readonly string FullName;
}