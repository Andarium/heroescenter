namespace HeroesCenter.Helpers;

public static class Converter
{
    public static TOutput ConvertValue<TOutput, TInput>(TInput value) where TInput : IConvertible
    {
        return (TOutput)Convert.ChangeType(value, typeof(TOutput));
    }
}