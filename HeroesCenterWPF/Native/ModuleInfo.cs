namespace HeroesCenter.Native;

public record ModuleInfo(IntPtr BaseAddress, IntPtr EntryPoint, uint Size, string FullName)
{
    public string ModuleName => Path.GetFileName(FullName);

    public override string ToString()
    {
        return $"{BaseAddress:X8}: {ModuleName}";
    }
}

[StructLayout(LayoutKind.Sequential)]
public readonly record struct ModuleInfoNative
{
    public readonly IntPtr lpBaseOfDll;
    public readonly uint SizeOfImage;
    public readonly IntPtr EntryPoint;
}