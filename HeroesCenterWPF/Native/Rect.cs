namespace HeroesCenter.Native;

[StructLayout(LayoutKind.Sequential, Pack = 0)]
public readonly struct Rect
{
    public readonly int left;
    public readonly int top;
    public readonly int right;
    public readonly int bottom;

    public override bool Equals(object? obj)
    {
        return obj is Rect other && Equals(other);
    }

    public bool Equals(Rect other)
    {
        return left == other.left &&
               top == other.top &&
               right == other.right &&
               bottom == other.bottom;
    }

    public override int GetHashCode()
    {
        unchecked
        {
            var hashCode = left;
            hashCode = hashCode * 397 ^ top;
            hashCode = hashCode * 397 ^ right;
            hashCode = hashCode * 397 ^ bottom;
            return hashCode;
        }
    }

    public static bool operator ==(Rect rect1, Rect rect2)
    {
        return rect1.Equals(rect2);
    }

    public static bool operator !=(Rect rect1, Rect rect2)
    {
        return !(rect1 == rect2);
    }
}