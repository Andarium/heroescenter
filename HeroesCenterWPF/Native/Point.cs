namespace HeroesCenter.Native;

[StructLayout(LayoutKind.Sequential)]
public readonly struct Point
{
    public Point(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    public readonly int x;
    public readonly int y;

    public override bool Equals(object? obj)
    {
        return obj is Point other && Equals(other);
    }

    public override int GetHashCode()
    {
        unchecked
        {
            return x * 397 ^ y;
        }
    }

    public static bool operator ==(Point point1, Point point2)
    {
        return point1.x == point2.x && point1.y == point2.y;
    }

    public static bool operator !=(Point point1, Point point2)
    {
        return !(point1 == point2);
    }

    public bool Equals(Point other)
    {
        return x == other.x && y == other.y;
    }
}