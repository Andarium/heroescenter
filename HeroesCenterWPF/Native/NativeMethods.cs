namespace HeroesCenter.Native;

internal static class NativeMethods
{
    public delegate IntPtr HookProc(int nCode, IntPtr wParam, IntPtr lParam);

    [DllImport("kernel32.dll", CharSet = CharSet.Unicode)]
    public static extern IntPtr LoadLibrary(string lpFileName);

    [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
    public static extern bool FreeLibrary(IntPtr hModule);

    /// <summary>
    ///     The SetWindowsHookEx function installs an application-defined hook procedure into a hook chain.
    ///     You would install a hook procedure to monitor the system for certain types of events. These events are
    ///     associated either with a specific thread or with all threads in the same desktop as the calling thread.
    /// </summary>
    /// <param name="idHook">hook type</param>
    /// <param name="lpfn">hook procedure</param>
    /// <param name="hMod">handle to application instance</param>
    /// <param name="dwThreadId">thread identifier</param>
    /// <returns>If the function succeeds, the return value is the handle to the hook procedure.</returns>
    [DllImport("USER32", SetLastError = true)]
    public static extern IntPtr SetWindowsHookEx(int idHook, HookProc lpfn, IntPtr hMod, int dwThreadId);

    /// <summary>
    ///     The CallNextHookEx function passes the hook information to the next hook procedure in the current hook chain.
    ///     A hook procedure can call this function either before or after processing the hook information.
    /// </summary>
    /// <param name="hHook">handle to current hook</param>
    /// <param name="code">hook code passed to hook procedure</param>
    /// <param name="wParam">value passed to hook procedure</param>
    /// <param name="lParam">value passed to hook procedure</param>
    /// <returns>If the function succeeds, the return value is true.</returns>
    [DllImport("USER32", SetLastError = true)]
    public static extern IntPtr CallNextHookEx(IntPtr hHook, int code, IntPtr wParam, IntPtr lParam);

    [DllImport("user32")]
    internal static extern bool GetMonitorInfo(IntPtr hMonitor, MonitorInfo lpmi);

    [DllImport("user32")]
    internal static extern IntPtr MonitorFromWindow(IntPtr hwnd, int flags);

    [DllImport("kernel32.dll")]
    internal static extern IntPtr OpenProcess(
        int dwDesiredAccess,
        bool bInheritHandle,
        int dwProcessId);

    [DllImport("kernel32.dll", SetLastError = true)]
    [return: MarshalAs(UnmanagedType.Bool)]
    internal static extern bool CloseHandle(IntPtr hObject);

    [DllImport("kernel32.dll", SetLastError = true)]
    internal static extern bool ReadProcessMemory(
        IntPtr hProcess,
        IntPtr lpBaseAddress,
        [Out] byte[] lpBuffer,
        IntPtr dwSize,
        out int lpNumberOfBytesRead);

    [DllImport("kernel32.dll", SetLastError = true)]
    internal static extern bool WriteProcessMemory(
        IntPtr hProcess,
        IntPtr lpBaseAddress,
        byte[] lpBuffer,
        IntPtr nSize,
        out int lpNumberOfBytesWritten);

    [DllImport("user32.dll")]
    internal static extern IntPtr SendMessage(IntPtr hWnd, int msg, IntPtr wParam, IntPtr lParam);

    [DllImport("user32.dll")]
    internal static extern bool ReleaseCapture();

    /// <summary>
    ///     The UnhookWindowsHookEx function removes a hook procedure installed in a hook chain by the SetWindowsHookEx
    ///     function.
    /// </summary>
    /// <param name="hHook">handle to hook procedure</param>
    /// <returns>If the function succeeds, the return value is true.</returns>
    [DllImport("USER32", SetLastError = true)]
    internal static extern bool UnhookWindowsHookEx(IntPtr hHook);

    [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
    public static extern bool SetWindowPos(
        HandleRef hWnd,
        HandleRef hWndInsertAfter,
        int x,
        int y,
        int cx,
        int cy,
        int flags = 0x0010 | 0x0004 | 0x0200 | 0x0400);

    [DllImport("kernel32.dll", SetLastError = true, ExactSpelling = true)]
    public static extern IntPtr VirtualAllocEx(IntPtr hProcess, IntPtr lpAddress, uint dwSize, uint flAllocationType, uint flProtect);

    /// <summary>TimeBeginPeriod(). See the Windows API documentation for details.</summary>
    [DllImport("winmm.dll", EntryPoint = "timeBeginPeriod", SetLastError = true)]
    public static extern uint TimeBeginPeriod(uint uMilliseconds);

    /// <summary>TimeEndPeriod(). See the Windows API documentation for details.</summary>
    [DllImport("winmm.dll", EntryPoint = "timeEndPeriod", SetLastError = true)]
    public static extern uint TimeEndPeriod(uint uMilliseconds);


    [DllImport("psapi.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
    public static extern uint GetModuleFileNameEx(IntPtr hProcess, IntPtr hModule, [Out] StringBuilder lpBaseName, uint nSize);

    [DllImport("psapi.dll", SetLastError = true)]
    public static extern bool GetModuleInformation(IntPtr hProcess, IntPtr hModule, out ModuleInfoNative lpModInfo, uint cb);

    public enum ListModules : uint
    {
        LIST_MODULES_DEFAULT = 0x0,
        LIST_MODULES_32BIT = 0x01,
        LIST_MODULES_64BIT = 0x02,
        LIST_MODULES_ALL = 0x03
    }

    // see: https://docs.microsoft.com/en-us/windows/win32/api/psapi/nf-psapi-enumprocessmodulesex
    [DllImport("psapi.dll", SetLastError = true)]
    public static extern bool EnumProcessModulesEx(
        IntPtr hProcess,
        [MarshalAs(UnmanagedType.LPArray, ArraySubType = UnmanagedType.U4)] [In] [Out]
        IntPtr[] lphModule,
        uint cb,
        [MarshalAs(UnmanagedType.U4)] out uint lpcbNeeded,
        ListModules dwFilterFlag
    );

    private static List<IntPtr> GetProcessModulesHandles(IntPtr handle)
    {
        uint arraySize = 256;
        var processMods = new IntPtr[arraySize];
        var pointerSize = (uint) IntPtr.Size;
        uint arrayBytesSize = arraySize * pointerSize;
        uint bytesCopied;

        // Loop until all modules are listed
        // See: https://docs.microsoft.com/en-us/windows/win32/api/psapi/nf-psapi-enumprocessmodules#:~:text=If%20lpcbNeeded%20is%20greater%20than%20cb%2C%20increase%20the%20size%20of%20the%20array%20and%20call%20EnumProcessModules%20again.
        // Stops if:
        //   - EnumProcessModulesEx return 0 (call failed)
        //   - All modules are listed
        //   - The next size of the list is greater than uint.MaxValue
        while (EnumProcessModulesEx(handle, processMods, arrayBytesSize, out bytesCopied, ListModules.LIST_MODULES_ALL) &&
               arrayBytesSize == bytesCopied && arraySize <= uint.MaxValue - 128)
        {
            arraySize += 128;
            processMods = new IntPtr[arraySize];
            arrayBytesSize = arraySize * pointerSize;
        }

        var moduleCount = (int) (bytesCopied / pointerSize);
        return processMods.Take(moduleCount).ToList();
    }

    public static ModuleInfo? FindProcessModule(IntPtr handle, string moduleName)
    {
        var modulesHandles = GetProcessModulesHandles(handle);

        var moduleInfoSize = (uint) Marshal.SizeOf<ModuleInfoNative>();
        var stringBuilder = new StringBuilder(256);

        foreach (var moduleHandle in modulesHandles)
        {
            var success = GetModuleInformation(handle, moduleHandle, out var info, moduleInfoSize);
            if (!success)
            {
                continue;
            }

            stringBuilder.Clear();
            var tmp = GetModuleFileNameEx(handle, moduleHandle, stringBuilder, (uint) stringBuilder.Capacity);
            if (tmp == 0)
            {
                continue;
            }

            var modulePath = stringBuilder.ToString();

            if (moduleName == Path.GetFileName(modulePath))
            {
                return new ModuleInfo(info.lpBaseOfDll, info.EntryPoint, info.SizeOfImage, modulePath);
            }
        }

        return null;
    }
}