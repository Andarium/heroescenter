namespace HeroesCenter.Native;

[StructLayout(LayoutKind.Sequential)]
public struct MinMaxInfo : IEquatable<MinMaxInfo>
{
    public Point ptReserved;
    public Point ptMaxSize;
    public Point ptMaxPosition;
    public Point ptMinTrackSize;
    public Point ptMaxTrackSize;

    public override bool Equals(object? obj)
    {
        return obj is MinMaxInfo other && Equals(other);
    }

    public override int GetHashCode()
    {
        return ptReserved.GetHashCode();
    }

    public static bool operator ==(MinMaxInfo info1, MinMaxInfo info2)
    {
        return info1.Equals(info2);
    }

    public static bool operator !=(MinMaxInfo info1, MinMaxInfo info2)
    {
        return !(info1 == info2);
    }

    public bool Equals(MinMaxInfo other)
    {
        return ptReserved == other.ptReserved &&
               ptMaxSize == other.ptMaxSize &&
               ptMaxPosition == other.ptMaxPosition &&
               ptMinTrackSize == other.ptMinTrackSize &&
               ptMaxTrackSize == other.ptMaxTrackSize;
    }
}