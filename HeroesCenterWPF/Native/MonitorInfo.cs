namespace HeroesCenter.Native;

[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
public class MonitorInfo
{
    public int cbSize = Marshal.SizeOf(typeof(MinMaxInfo));
    public int dwFlags = 0;
    public Rect rcMonitor = new();
    public Rect rcWork = new();
}