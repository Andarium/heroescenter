using System.Diagnostics.CodeAnalysis;

namespace HeroesCenter.Extensions;

public static class EnumerableExtension
{
    public static TSource? ElementAtOrDefault<TSource>(this IReadOnlyCollection<TSource>? source, byte? index, TSource? @default = default)
    {
        if (source == null || index == null || index.Value >= source.Count)
        {
            return @default;
        }

        return source.ElementAt(index.Value);
    }

    public static TSource? ElementAtOrDefault<TSource>(this IReadOnlyCollection<TSource>? source, int? index, TSource? @default = default)
    {
        if (source == null || index == null || index.Value >= source.Count || index.Value < 0)
        {
            return @default;
        }

        return source.ElementAt(index.Value);
    }

    public static TValue? ElementAtOrDefault<TKey, TValue>(this IDictionary<TKey, TValue>? source, TKey? key, TValue? @default = default)
        where TValue : notnull
    {
        if (source == null || key == null || !source.ContainsKey(key))
        {
            return @default;
        }

        return source[key];
    }

    public static TSource ElementAtOrLast<TSource>(this IEnumerable<TSource> source, int index)
    {
        if (source == null)
        {
            throw new ArgumentNullException(nameof(source));
        }

        if (index < 0)
        {
            throw new IndexOutOfRangeException($"Negative index {index}");
        }

        if (source is IList<TSource> list)
        {
            if (index >= list.Count)
            {
                if(list.Count == 0)
                {
                    throw new IndexOutOfRangeException("Empty collection");
                }

                index = list.Count - 1;
            }

            return list[index];
        }

        if (!TryGetElementOrLast(source, index, out var element))
        {
            throw new IndexOutOfRangeException("Empty collection");
        }

        return element;
    }

    public static TSource? ElementAtOrLastOrDefault<TSource>(this IEnumerable<TSource> source, int index)
    {
        if (source == null)
        {
            throw new ArgumentNullException(nameof(source));
        }

        if (index < 0)
        {
            return default;
        }


        if (source is IList<TSource> list)
        {
            return index < list.Count ? list[index] : default;
        }

        return TryGetElementOrLast(source, index, out var element) ? element : default;
    }

    public static IList<T> AddRangeUnique<T, TKey>(this IList<T> source, IList<T> add, Func<T, TKey> selector)
    {
        if (source == null)
        {
            throw new ArgumentNullException(nameof(source));
        }

        if (selector == null)
        {
            throw new ArgumentNullException(nameof(selector));
        }

        var sourceKeys = new HashSet<TKey>(source.Select(selector));
        var addKeys = add.Select(selector).ToArray();

        for (var i = 0; i < addKeys.Length; i++)
        {
            if (!sourceKeys.Contains(addKeys[i]))
            {
                source.Add(add[i]);
            }
        }

        return source;
    }

    public static IList<TKey> GetKeysOfDiff<TKey, TValue>(this IDictionary<TKey, TValue> input, IDictionary<TKey, TValue> other) where TValue : IComparable
    {
        return input
            .Where(x => x.Value.CompareTo(other[x.Key]) != 0)
            .Select(x => x.Key)
            .ToList();
    }

    private static bool TryGetElement<TSource>(IEnumerable<TSource> source, int index, [MaybeNullWhen(false)] out TSource element)
    {
        Debug.Assert(source != null);

        if (index >= 0)
        {
            using IEnumerator<TSource> e = source.GetEnumerator();
            while (e.MoveNext())
            {
                if (index == 0)
                {
                    element = e.Current;
                    return true;
                }

                index--;
            }
        }

        element = default;
        return false;
    }

    private static bool TryGetElementOrLast<TSource>(
        IEnumerable<TSource> source,
        int index,
        [MaybeNullWhen(false)] out TSource element
    )
    {
        Debug.Assert(source != null);
        var isEmpty = true;
        TSource last = default!;
        if (index >= 0)
        {
            using IEnumerator<TSource> e = source.GetEnumerator();
            while (e.MoveNext())
            {
                element = e.Current;
                isEmpty = false;
                if (index == 0)
                {
                    return true;
                }

                index--;
            }
        }

        if (!isEmpty)
        {
            element = last;
            return true;
        }

        element = default;
        return false;
    }
}