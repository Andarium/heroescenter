namespace HeroesCenter.Extensions;

public static class StringExtensions
{
    public const char EndChar = '\0';
    public const string EndString = "\0";

    private static readonly StringBuilder StringBuilder = new();

    public static string ByteArrayToString(this IEnumerable<byte> input)
    {
        StringBuilder.Clear();

        foreach (byte b in input)
        {
            if (b == 0)
            {
                break;
            }

            if (b > 122)
            {
                StringBuilder.Append((char) (b + 848));
            }
            else
            {
                StringBuilder.Append((char) b);
            }
        }

        return StringBuilder.ToString();
    }

    /// <summary>
    ///     Перевод строковой переменной в массив байт
    /// </summary>
    /// <param name="input">Строка, которую нужно перевести в массив байт</param>
    /// <param name="limitLength"></param>
    public static byte[] ToByteArray(this string input, int? limitLength = null)
    {
        var length = Math.Min(input.Length, limitLength ?? int.MaxValue);
        var byteArray = new byte[length + 1]; // +1 for EndChar
        for (var i = 0; i < input.Length && i < length; i++)
        {
            byteArray[i] = input[i] > 970
                ? (byte) (input[i] - 848)
                : (byte) input[i];
        }

        byteArray[length] = (byte) EndChar;

        return byteArray;
    }

    public static bool Contains(this string source, string value, StringComparison comp)
    {
        return source.IndexOf(value, comp) >= 0;
    }

    public static bool Contains(this IEnumerable<string> source, string value, StringComparison comp)
    {
        return source.Any(x => x.Equals(value, comp));
    }

    public static T ToEnum<T>(this string value)
    {
        return (T) Enum.Parse(typeof(T), value, true);
    }

    public static string OrEmpty(this string? input)
    {
        return input ?? string.Empty;
    }

    public static string ToStringOrEmpty(this object? input)
    {
        if (input == null)
        {
            return string.Empty;
        }

        return input.ToString() ?? string.Empty;
    }
}