namespace HeroesCenter.Extensions;

public static class CoreExtensions
{
    public static T Clamp<T>(this T val, T min, T max) where T : IComparable<T>
    {
        if (val.CompareTo(min) < 0) return min;
        if (val.CompareTo(max) > 0) return max;
        return val;
    }

    public static short Clamp(this short val, short min, short max)
    {
        if (val.CompareTo(min) < 0) return min;
        if (val.CompareTo(max) > 0) return max;
        return val;
    }

    public static ulong ToUInt64(this object value)
    {
        // Helper function to silently convert the value to UInt64 from the other base types for enum without throwing an exception.
        // This is need since the Convert functions do overflow checks.
        TypeCode typeCode = Convert.GetTypeCode(value);
        ulong result;

        switch (typeCode)
        {
            case TypeCode.SByte:
            case TypeCode.Int16:
            case TypeCode.Int32:
            case TypeCode.Int64:
                result = (ulong) Convert.ToInt64(value, CultureInfo.InvariantCulture);
                break;

            case TypeCode.Byte:
            case TypeCode.UInt16:
            case TypeCode.UInt32:
            case TypeCode.UInt64:
                result = Convert.ToUInt64(value, CultureInfo.InvariantCulture);
                break;

            default:
                // All unsigned types will be directly cast
                throw new InvalidOperationException("InvalidOperation_UnknownEnumType");
        }

        return result;
    }

    public static byte ToByte(this int input)
    {
        return (byte) input;
    }

    public static void ReloadDataContext(this FrameworkElement? element)
    {
        if (element == null)
        {
            return;
        }

        var currentContext = element.DataContext;

        if (currentContext != null)
        {
            element.DataContext = null;
            element.DataContext = currentContext;
        }
    }
}