namespace HeroesCenter.Extensions;

public static class ToByteArrayExtension
{
    /// <summary>
    ///     Перевод структурной переменной в массив байт
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="input">Переменная структурного типа, которую нужно перевести в массив байт</param>
    /// <returns></returns>
    public static byte[] ToByteArray<T>(this T input) where T : struct
    {
        if (typeof(T) == typeof(byte))
        {
            return new[] { (byte) (input as object) };
        }

        MethodInfo? method = typeof(BitConverter).GetMethod("GetBytes", new[] { input.GetType() });
        if (method == null)
        {
            throw new ArgumentException($"{typeof(T).Name} cannot be converted to Byte array");
        }

        object? result = method.Invoke(null, new object[] { input });

        if (result == null)
        {
            throw new NullReferenceException($"{typeof(T).Name} cannot be converted to Byte array");
        }

        return (byte[]) result;
    }

    /*public static Byte[] ToByteArray<T>(this T[] someArray) where T : struct
    {
        var result = new Byte[someArray.Length*System.Runtime.InteropServices.Marshal.SizeOf(typeof (T))];
        Buffer.BlockCopy(someArray, 0, result, 0, result.Length);
        return result;
    }*/

    /// <summary>
    ///     Перевод IEnumerable переменных структурного типа в массив байт
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="someCollection">IEnumerable переменных структурного типа, который нужно перевести в массив байт</param>
    public static byte[] ToByteArray<T>(this IEnumerable<T> someCollection) where T : struct
    {
        T[] someArray = someCollection.ToArray();
        var result = new byte[someArray.Length * Marshal.SizeOf(typeof(T))];
        Buffer.BlockCopy(someArray, 0, result, 0, result.Length);
        return result;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static int ToInt32(this byte[] input) => BitConverter.ToInt32(input, 0);

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static short ToInt16(this byte[] input) => BitConverter.ToInt16(input, 0);
}