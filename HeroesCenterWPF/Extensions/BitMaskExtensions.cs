namespace HeroesCenter.Extensions;

public static class BitMaskExtensions
{
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool HasBit(this ulong value, int bitIndex)
    {
        return value.HasFlag(1UL << bitIndex);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool HasBit(this long value, int bitIndex)
    {
        return value.HasFlag(1L << bitIndex);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool HasBit(this int value, int bitIndex)
    {
        return value.HasFlag(1 << bitIndex);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool HasBit(this byte value, int bitIndex)
    {
        return value.HasFlag(1 << bitIndex);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool HasFlag(this ulong value, ulong flag)
    {
        return (value & flag) == flag;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool HasFlag(this long value, long flag)
    {
        return (value & flag) == flag;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool HasFlag(this int value, int flag)
    {
        return (value & flag) == flag;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool HasFlag(this byte value, int flag)
    {
        return (value & flag) == flag;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static int BitCount(this int x)
    {
        x -= (x >> 1) & 0x55555555;
        x = ((x >> 2) & 0x33333333) + (x & 0x33333333);
        x = ((x >> 4) + x) & 0x0f0f0f0f;
        x += x >> 8;
        x += x >> 16;
        return x & 0x0000003f;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static int FloorLog2(this int x)
    {
        x |= x >> 1;
        x |= x >> 2;
        x |= x >> 4;
        x |= x >> 8;
        x |= x >> 16;

        return BitCount(x) - 1;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static HashSet<int> GetBits(this int x)
    {
        var result = new HashSet<int>();
        var bitIndex = 0;
        while (x > 0)
        {
            if (x.HasBit(bitIndex))
            {
                // remove bit
                x &= ~(1 << bitIndex);
                result.Add(bitIndex);
            }

            bitIndex++;
        }

        return result;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static ulong SetBit(this ulong input, int index)
    {
        return input | (1UL << index);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static long SetBit(this long input, int index)
    {
        return input | (1L << index);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static ulong UnsetBit(this ulong input, int index)
    {
        return input & ~(1UL << index);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static long UnsetBit(this long input, int index)
    {
        return input & ~(1L << index);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static byte GetByte(this long input, int index)
    {
        var shift = index * 8;
        return (byte) ((input >> shift) & 0xFF);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static long SetByte(this long input, int index, byte value)
    {
        var shift = index * 8;
        input &= ~(0xFFL << shift); // clear index byte
        var mask = (long) value;
        mask <<= shift;
        return input | mask; // set index byte
    }
}