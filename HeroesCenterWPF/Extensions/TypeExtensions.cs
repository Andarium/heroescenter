namespace HeroesCenter.Extensions;

public static class TypeExtensions
{
    //a thread-safe way to hold default instances created at run-time
    private static readonly ConcurrentDictionary<Type, object?> TypeDefaults = new();

    public static object? GetDefaultValue(this Type type)
    {
        return type.IsValueType
            ? TypeDefaults.GetOrAdd(type, Activator.CreateInstance)
            : null;
    }
}