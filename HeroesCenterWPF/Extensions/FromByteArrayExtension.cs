namespace HeroesCenter.Extensions;

public static class FromByteArrayExtension
{
    public static int[]? ToInt32Array(this IEnumerable<byte> input)
    {
        var byteArray = input.ToArray();
        if (byteArray.Length % 4 != 0 || byteArray.Length < 4) return null;
        var tempArray = new int[byteArray.Length / 4];
        Buffer.BlockCopy(byteArray, 0, tempArray, 0, byteArray.Length);
        return tempArray;
    }
}