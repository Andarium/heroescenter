using System.Diagnostics.CodeAnalysis;

namespace HeroesCenter.Extensions;

public static class EnumerationExtension
{
    public static List<KeyValuePair<Enum, string>>? GetValueDescriptions(Type enumType)
    {
        if (!enumType.IsEnum)
        {
            return null;
        }

        string ConvertEnum(Enum input)
        {
            string s = input.ToString();
            s = s.Replace("_", " ");

            s = Regex.Replace(s, "\\p{Lu}(?=\\p{Ll})", match => " " + match.Value);
            s = s.ToLowerInvariant().Trim();
            s = Regex.Replace(s, "[ ]+", " ");
            s = Regex.Replace(s, "^\\w", match => match.Value.ToUpperInvariant());
            return s;
        }

        List<KeyValuePair<Enum, string>> attributes = Enum
            .GetValues(enumType)
            .Cast<Enum>()
            .OrderBy(x => GetOrder(enumType, x.ToString()))
            .ToDictionary(x => x, ConvertEnum)
            .ToList();

        return attributes.ToList();
    }

    private static int GetOrder(Type type, string field)
    {
        var fieldInfo = type.GetField(field);

        if (fieldInfo == null)
        {
            throw new InvalidOperationException($"No such field {field} in {type.AssemblyQualifiedName}");
        }

        OrderAttribute[] attributes = fieldInfo.GetCustomAttributes(typeof(OrderAttribute), false).Cast<OrderAttribute>().ToArray();
        return attributes.Length > 0
            ? attributes.First().Value
            : int.MaxValue;
    }

    public static T AddFlag<T>(this Enum value, [DisallowNull] T flag)
    {
        Type underlyingType = Enum.GetUnderlyingType(value.GetType());

        dynamic valueAsInt = Convert.ChangeType(value, underlyingType);
        dynamic flagAsInt = Convert.ChangeType(flag, underlyingType);
        valueAsInt |= flagAsInt;

        return (T) valueAsInt;
    }

    public static T RemoveFlag<T>(this Enum value, [DisallowNull] T flag)
    {
        Type underlyingType = Enum.GetUnderlyingType(value.GetType());

        dynamic valueAsInt = Convert.ChangeType(value, underlyingType);
        dynamic flagAsInt = Convert.ChangeType(flag, underlyingType);
        valueAsInt &= ~flagAsInt;

        return (T) valueAsInt;
    }

    public static List<T> GetFlags<T>(this Enum value)
    {
        IEnumerable<T> allValues = Enum.GetValues(typeof(T)).Cast<T>();

        return allValues
            .Where(x =>
                x is Enum e &&
                value.HasFlag(e))
            .ToList();
    }

    public static bool HasAnyOfFlags(this Enum input, IEnumerable<Enum> flags)
    {
        return flags.Any(input.HasFlag);
    }

    public static T SwitchFlag<T>(this Enum value, T flag) where T : struct, IComparable, IConvertible, IFormattable
    {
        Type underlyingType = Enum.GetUnderlyingType(value.GetType());

        dynamic valueAsInt = Convert.ChangeType(value, underlyingType);
        dynamic flagAsInt = Convert.ChangeType(flag, underlyingType);

        bool hasFlag = (valueAsInt & flagAsInt) == flagAsInt;

        if (hasFlag)
        {
            valueAsInt &= ~flagAsInt;
        }
        else
        {
            valueAsInt |= flagAsInt;
        }

        valueAsInt |= flagAsInt;

        return (T) valueAsInt;
    }

    public static T SetFlag<T>(this Enum value, T flag, bool set) where T : struct, IComparable, IConvertible, IFormattable
    {
        Type underlyingType = Enum.GetUnderlyingType(value.GetType());

        dynamic valueAsInt = Convert.ChangeType(value, underlyingType);
        dynamic flagAsInt = Convert.ChangeType(flag, underlyingType);

        if (set)
        {
            valueAsInt |= flagAsInt;
        }
        else
        {
            valueAsInt &= ~flagAsInt;
        }

        return (T) valueAsInt;
    }

    public static bool HasFlag(this Enum variable, Enum value)
    {
        // check if from the same type.
        if (variable.GetType() != value.GetType())
        {
            throw new ArgumentException("The checked flag is not from the same type as the checked variable.");
        }

        var num = Convert.ToUInt64(value);
        var num2 = Convert.ToUInt64(variable);
        return (num2 & num) == num;
    }

    public static bool HasEnumFlag<T>(this T variable, T value) where T : unmanaged, Enum
    {
        unsafe
        {
            return sizeof(T) switch
            {
                1 => (*(byte*) &variable & *(byte*) &value) == *(byte*) &value,
                2 => (*(ushort*) &variable & *(ushort*) &value) == *(ushort*) &value,
                4 => (*(uint*) &variable & *(uint*) &value) == *(uint*) &value,
                8 => (*(ulong*) &variable & *(ulong*) &value) == *(ulong*) &value,
                _ => throw new Exception("Size does not match a known Enum backing type.")
            };
        }
    }

    public static bool IntersectsFlag<T>(this T variable, T value) where T : unmanaged, Enum
    {
        unsafe
        {
            return sizeof(T) switch
            {
                1 => (*(byte*) &variable & *(byte*) &value) > 0,
                2 => (*(ushort*) &variable & *(ushort*) &value) > 0,
                4 => (*(uint*) &variable & *(uint*) &value) > 0,
                8 => (*(ulong*) &variable & *(ulong*) &value) > 0,
                _ => throw new Exception("Size does not match a known Enum backing type.")
            };
        }
    }

    public static bool EqualsAnyOf<T>(this T variable, params T[] values)
    {
        return variable != null && values.Any(x => Equals(x, values));
    }
}