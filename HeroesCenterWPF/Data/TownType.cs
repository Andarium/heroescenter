namespace HeroesCenter.Data;

public enum TownType
{
    Castle      = 0,
    Rampart     = 1,
    Tower       = 2,
    Inferno     = 3,
    Necropolis  = 4,
    Dungeon     = 5,
    Stronghold  = 6,
    Fortress    = 7,
    Conflux     = 8,
    Cove        = 9,
    None   = -1
}