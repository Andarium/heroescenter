namespace HeroesCenter.Data;

public class UnitInfo
{
    public int Index { get; set; } = -1;
    public bool Available { get; set; }
}