namespace HeroesCenter.Data.Stats;

public static class HeroStats
{
    public static readonly MemoryVariable
        X = new(2, 0),
        Y = new(2, 2),
        Z = new(2, 4),
        Unknown_IsAvailable = new (1, 6),
        // 17 bytes
        ManaPoints = new(2, 24),
        Specialization = new(1, 26),
        // 7 bytes
        Flag = new(1, 34),
        Name = new(12, 35),
        // 1 byte
        Class = new(1, 48),
        // 3 bytes
        PortraitId = new(1, 52),
        // 20 bytes
        DailyMovement = new(4, 73), // Очки движения, которые выдаются в начале дня. Пересчитываются при заходе в экран героя
        Movement = new(4, 77),
        Experience = new(4, 81),
        Level = new(2, 85),
        // 58 bytes
        Army = new(56, 145),
        SecondarySkills = new(28, 201),
        SecondarySkillsRegistry = new(28, 229), // Реестр навыков (наличие и порядок), SOD/WOG
        SecondarySkillsCount = new(1, 257), // Ограничитель изучения новых навыков внутриигровым способом, без WoG не дает изучать больше 8
        // 6 bytes
        Unknown_NwcTheOne = new(1, 264), // nwctheone
        // 4 bytes
        TeleportUseCount = new(1, 269),
        // 12 bytes
        MoraleTotal = new(1, 282),
        LuckTotal = new(1, 283),
        // 17 bytes
        ArtifactsOnHero = new(19 * 8, 301), // Артефакты на герое
        // 1 byte
        Seals = new(14, 454),
        ArtifactsInBackpack = new(64 * 8, 468), // Артефакты в рюкзаке, отображаются первые 5
        // 22 bytes
        SpellsLearned = new(70, 1002),
        SpellsInBook = new(70, 1072),
        Attack = new(1, 1142),
        Defence = new(1, 1143),
        SpellPower = new(1, 1144),
        Knowledge = new(1, 1145),
        SecondarySkillsRegistryPointer = new(29, 1146, 8, 0); // Реестр навыков (наличие и порядок), HOTA

    public static int SummarySize
    {
        get
        {
            return AllVariables
                .Select(x => x.Size)
                .Sum();
        }
    }

    public static MemoryVariable[] AllVariables
    {
        get
        {
            return typeof(HeroStats)
                .GetFields()
                .Select(field => field.GetValue(typeof(HeroStats)))
                .Cast<MemoryVariable>()
                .ToArray();
        }
    }

    public static Dictionary<string, MemoryVariable> AllVariablesWithNames
    {
        get
        {
            return typeof(HeroStats)
                .GetFields()
                .ToDictionary(x => x.Name, y => (MemoryVariable) y.GetValue(typeof(HeroStats))!);
        }
    }
}