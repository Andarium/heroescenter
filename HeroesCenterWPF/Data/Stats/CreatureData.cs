namespace HeroesCenter.Data.Stats;

public static class CreatureData
{
    public const int CreatureCount = 197; // WOG
    public const int Size = 116;

    public static readonly MemoryVariable
        Town =          new(04, 000),
        Level =         new(04, 004),
        DefUnitName =   new(08, 020, 0), // Pointer, 4 letters, local name in def-file? (pike, "halb, zelt, monk, etc), space for 8 letters
        DefFileName =   new(12, 020, 0), // Pointer, def-file name, space for 12 letters
        Flags =         new(04, 016),
        SingleName =    new(24, 020, 0), // Pointer, space for 24 letters
        PluralName =    new(24, 024, 0), // Pointer, space for 24 letters
        // Unknown 4 bytes (PluralName2 for ru localization?)
        Wood =          new(04, 032),
        Mercury =       new(04, 036),
        Ore =           new(04, 040),
        Sulfur =        new(04, 044),
        Crystal =       new(04, 048),
        Gems =          new(04, 052),
        Gold =          new(04, 056),
        FightValue =    new(04, 060),
        AiValue =       new(04, 064),
        Growth =        new(04, 068),
        HordeGrowth =   new(04, 072),
        Health =        new(04, 076),
        Speed =         new(04, 080),
        Attack =        new(04, 084),
        Defence =       new(04, 088),
        MinDamage =     new(04, 092),
        MaxDamage =     new(04, 096),
        Shots =         new(04, 100),
        Spells =        new(04, 104),
        AdvMapLow =     new(04, 108), // CRTRAITS.txt
        AdvMapHigh =    new(04, 112);
}