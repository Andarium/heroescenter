namespace HeroesCenter.Data.Stats;

public static class TownStats
{
    // http://heroescommunity.com/viewthread.php3?TID=18817&pagenumber=3
    public static readonly MemoryVariable
        Index = new(1, 0),
        Owner = new(1, 1),
        BuildUsed = new(1, 2),
        TownType = new(1, 4),
        HeroGuest = new(1, 10),
        HeroHost = new(1, 14),
        CreatureRemains = new(14, 20),
        GarrisonArmy = new(56, 224),
        FirstCircleSpells = new(6 * 4, 68),
        SecondCircleSpells = new(5 * 4, 92),
        // Skipped 1 * 4 bytes
        ThirdCircleSpells = new(4 * 4, 116),

        // Skipped 2 * 4 bytes
        FourthCircleSpells = new(3 * 4, 140),

        // Skipped 3 * 4 bytes
        FifthCircleSpells = new(2 * 4, 164),

        // Skipped 4 * 4 bytes
        FirstCircleSpellsCount = new(1, 188), // max 6
        SecondCircleSpellsCount = new(1, 189), // max 5
        ThirdCircleSpellsCount = new(1, 190), // max 4
        FourthCircleSpellsCount = new(1, 191), // max 3
        FifthCircleSpellsCount = new(1, 192); // max 2
}