namespace HeroesCenter.Data.Stats;

public static class PlayerData
{
    public static readonly MemoryVariable
        Index = new(1, 0),
        HeroesOnMapCount = new(1, 1),
        SelectedHero = new(1, 4),
        HeroesOnMap = new(8 * 4, 8),
        TownCount = new(1, 62),
        SelectedTown = new(1, 63),
        Towns = new(4, 64),
        Wood = new(4, 156),
        Mercury = new(4, 160),
        Ore = new(4, 164),
        Sulfur = new(4, 168),
        Crystals = new(4, 172),
        Gems = new(4, 176),
        Gold = new(4, 180);
}