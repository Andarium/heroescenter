namespace HeroesCenter.Data.Stats;

/// <summary>
///     Класс этот вместо блокнота/заметки по переменным в данных карты (размер и смещение)
/// </summary>
public static class MapData
{
    public static readonly MemoryVariable CurrentDay = new(0x2, 0);
    public static readonly MemoryVariable CurrentWeek = new(0x2, 2);
    public static readonly MemoryVariable CurrentMonth = new(0x2, 4);
    public static readonly MemoryVariable LevelCap = new(0x1, 569);
}