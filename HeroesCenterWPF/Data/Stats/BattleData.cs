namespace HeroesCenter.Data.Stats;

public static class BattleData
{
    public static readonly MemoryVariable IsMagicGarrison = new(0x1, 0);
    public static readonly MemoryVariable IsMagicUsedHeroLeft = new(0x1, 240);
    public static readonly MemoryVariable IsMagicUsedHeroRight = new(0x1, 244);
}