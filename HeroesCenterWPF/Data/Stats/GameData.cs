namespace HeroesCenter.Data.Stats;

/// <summary>
///     Класс этот вместо блокнота/заметки по переменным в данных игры (размер и смещение)
/// </summary>
public static class GameData
{
    public static readonly MemoryVariable ActivePlayer = new(0x1, 0);
    public static readonly MemoryVariable IsUnderWorld = new(0x1, -0x385A8);
}