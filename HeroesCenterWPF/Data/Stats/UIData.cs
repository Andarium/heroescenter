namespace HeroesCenter.Data.Stats;

public static class UIData
{
    public static readonly MemoryVariable
        ViewX = new(1, 0),
        ViewY = new(1, 2),
        ViewZ = new(1, 3),
        CursorCellX = new(1, 4),
        CursorCellY = new(1, 6),
        CursorCellZ = new(1, 7);
}