namespace HeroesCenter.Data;

public class SpellInfo
{
    public string Name { get; }
    public int Type { get; }
    public int Level { get; }
    public MagicSchool School { get; }

    public SpellInfo(int type, int level, string name, MagicSchool school)
    {
        Name = name;
        Type = type;
        Level = level;
        School = school;
    }

    public SpellInfo()
    {
        Name = string.Empty;
        Type = -1;
        Level = -1;
        School = MagicSchool.All;
    }

    public override string ToString()
    {
        return $"{Name}, Level {Level}, {School}";
    }
}