using static HeroesCenter.Data.ArtifactCategory;

namespace HeroesCenter.Data;

public static class SlotHelper
{
    private static readonly IReadOnlyList<string> SlotNames = new[]
    {
        "Head",
        "Shoulders",
        "Neck",
        "Right Arm",
        "Left Arm",
        "Body",
        "Right Ring",
        "Left Ring",
        "Legs",
        "Slot #1",
        "Slot #2",
        "Slot #3",
        "Slot #4",
        "Ballista",
        "Cart",
        "Tent",
        "Catapult",
        "Magic Book",
        "Slot #5",
        "Backpack #1",
        "Backpack #2",
        "Backpack #3",
        "Backpack #4",
        "Backpack #5",
        "None"
    };

    private static readonly IReadOnlyList<ArtifactCategory> SlotCategories = new[]
    {
        Head,
        Shoulders,
        Neck,
        RightArm,
        LeftArm,
        Body,
        Rings,
        Rings,
        Legs,
        Slots,
        Slots,
        Slots,
        Slots,
        Ballista,
        Cart,
        Tent,
        Catapult,
        MagicBook,
        Slots,
        Backpack
    };

    public static string GetSlotName(int slotIndex)
    {
        return SlotNames.ElementAtOrLast(slotIndex);
    }

    public static ArtifactCategory GetSlotCategory(int slotIndex)
    {
        return SlotCategories.ElementAtOrLast(slotIndex);
    }
}