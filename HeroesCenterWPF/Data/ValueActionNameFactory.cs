namespace HeroesCenter.Data;

public static class ValueActionNameFactory
{
    private static int _index;

    public static string CreateDamperActionName()
    {
        return $"Damper Action #{_index++}";
    }

    public static string CreateMovementDamperActionName(int heroIndex)
    {
        return $"MovementDamper for Hero {heroIndex}";
    }

    public static string CreateMovementFreezeActionName(int heroIndex)
    {
        return $"MovementFreeze for Hero {heroIndex}";
    }
}