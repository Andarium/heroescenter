namespace HeroesCenter.Data;

public class UnitSettings
{
    public readonly int UnitType;
    public readonly string SpriteKey;
    public readonly GameVersion SpriteVersion;
    public readonly bool IsAvailable;

    public UnitSettings(int unitType, string spriteKey, GameVersion spriteVersion, bool isAvailable)
    {
        UnitType = unitType;
        SpriteKey = spriteKey;
        SpriteVersion = spriteVersion;
        IsAvailable = isAvailable;
    }
}