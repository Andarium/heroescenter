namespace HeroesCenter.Data;

public static class ArtifactHelper
{
    private static readonly Dictionary<GameVersion, List<ArtifactInfo>> VersionMap = JsonLoader.LoadArtifacts();

    private static readonly ArtifactInfo NullInfo = new()
    {
        Type = -1,
    };

    public static IList<ArtifactInfo> GetArtifactsByCategory(ArtifactCategory category, GameVersion version, bool availableOnly = true)
    {
        var versionArtifacts = VersionMap[version];

        var result = versionArtifacts.Where(x => category.HasFlag(x.Category));

        if (availableOnly)
        {
            result = result.Where(x => x.Available);
        }

        return result.ToList();
    }

    public static ArtifactInfo GetArtifactInfo(int type, int data, GameVersion gameVersion)
    {
        if (type < 0)
        {
            return NullInfo;
        }

        var versionArtifacts = VersionMap[gameVersion];
        if (type == 1)
        {
            return versionArtifacts.First(x => x.Type == type && x.Data == data);
        }

        return versionArtifacts.First(x => x.Type == type);
    }

    public static ArtifactInfo GetArtifactInfo(int type, GameVersion gameVersion)
    {
        if (type < 0)
        {
            return NullInfo;
        }

        var versionArtifacts = VersionMap[gameVersion];
        return versionArtifacts.FirstOrDefault(x => x.Type == type) ?? new ArtifactInfo();
    }
}