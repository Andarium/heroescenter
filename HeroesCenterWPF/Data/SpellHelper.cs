namespace HeroesCenter.Data;

public static class SpellHelper
{
    private static readonly IReadOnlyDictionary<int, SpellInfo> AllSpells = XmlHelper.LoadSpells();

    public static string GetName(int type)
    {
        return AllSpells.TryGetValue(type, out SpellInfo? spell) ? spell.Name : "None";
    }
}