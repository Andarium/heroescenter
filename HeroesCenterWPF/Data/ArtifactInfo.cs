namespace HeroesCenter.Data;

public class ArtifactInfo : IComparable<ArtifactInfo>
{
    public ArtifactInfo()
    {
        Name = string.Empty;
        DisplayName = Name;
        Type = -1;
        Available = false;
        Category = ArtifactCategory.None;
        Data = -1;
        Image = ImageHelper.NullImage;
    }

    public ArtifactInfo(ArtifactInfo original, int newData) : this()
    {
        Name = original.Name;
        Type = original.Type;
        Available = original.Available;
        Category = original.Category;
        Data = newData;
        Setup(original.Version);
    }

    [JsonProperty("a", Order = 2)]
    public bool Available { get; set; }

    [JsonIgnore]
    public GameVersion Version { get; private set; }

    [JsonProperty("n", Order = 3)]
    public string Name { get; private set; }

    [JsonIgnore]
    public string DisplayName { get; private set; }

    [JsonIgnore]
    public int Data { get; }

    [JsonProperty("i", Order = 0)]
    public int Type { get; set; }

    [JsonProperty("c", Order = 1)]
    public ArtifactCategory Category { get; set; }

    [UsedImplicitly]
    [JsonIgnore]
    public BitmapSource Image { get; private set; }

    public void Setup(GameVersion version)
    {
        Version = version;
        if (Type == 1 && Data >= 0)
        {
            DisplayName = Name = $"{Name} ({SpellHelper.GetName(Data)})";
        }
        else
        {
            DisplayName = $"{Name} #{Type}";
        }

        Image = ImageHelper.GetArtImage(Type, Data, Version);
    }

    public int CompareTo(ArtifactInfo? other)
    {
        if (other == null)
        {
            return 1;
        }

        var result = Type.CompareTo(other.Type);

        if (result == 0)
        {
            result = Data.CompareTo(other.Data);
        }

        return result;
    }

    public override string ToString()
    {
        return $"{Name} (#{Type})";
    }
}