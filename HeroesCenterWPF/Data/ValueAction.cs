namespace HeroesCenter.Data;

public class ValueAction
{
    protected readonly Func<bool>? Action;
    public readonly string ActionName;
    protected readonly double Interval;

    private double _elapsedTime;

    /// <summary>
    ///     Конструктор для ValueAction
    /// </summary>
    /// <param name="actionName">Имя действия</param>
    /// <param name="interval">Интервал выполнения действия</param>
    public ValueAction(string? actionName, double interval)
    {
        ActionName = actionName ?? string.Empty;
        Interval = interval;
    }

    /// <param name="actionName">Имя действия</param>
    /// <param name="interval">Интервал выполнения действия</param>
    /// <param name="action">Функция, возвращающая true, если операция была успешна</param>
    public ValueAction(string actionName, double interval, Func<bool> action) : this(actionName, interval)
    {
        Action = action;
    }

    public bool IsActionTime(double timePassed)
    {
        _elapsedTime += timePassed;

        if (_elapsedTime >= Interval)
        {
            _elapsedTime %= Interval;
            return true;
        }

        return false;
    }

    protected bool Equals(ValueAction other)
    {
        return string.Equals(ActionName, other.ActionName) && Interval.Equals(other.Interval) && Equals(Action, other.Action);
    }

    public override int GetHashCode()
    {
        unchecked
        {
            int hashCode = ActionName.GetHashCode();
            hashCode = (hashCode * 397) ^ Interval.GetHashCode();
            hashCode = (hashCode * 397) ^ (Action != null ? Action.GetHashCode() : 0);
            return hashCode;
        }
    }

    /*public override Int32 GetHashCode()
    {
        return Action.GetHashCode() * 7 + Interval.GetHashCode() + ActionName.GetHashCode() * 3;
    }*/

    public override bool Equals(object? obj)
    {
        if (obj is null)
        {
            return false;
        }

        if (ReferenceEquals(this, obj))
        {
            return true;
        }

        if (obj.GetType() != GetType())
        {
            return false;
        }

        return Equals((ValueAction) obj);
    }

    public virtual bool PerformAction()
    {
        return Action?.Invoke() == true;
    }
}

public class DamperAction : ValueAction
{
    private readonly Func<int> _getter;
    private readonly Func<bool> _isAvailable;
    private readonly decimal _power;
    private readonly Action<int> _setter;
    private int _prevValue;

    public DamperAction(double interval, string actionName, decimal power, Func<int> getter, Action<int> setter, Func<bool> isAvailable) : base(actionName, interval)
    {
        _power = power;
        _getter = getter;
        _setter = setter;
        _isAvailable = isAvailable;
        _prevValue = getter.Invoke();
    }

    public override bool PerformAction()
    {
        if (!_isAvailable.Invoke())
        {
            return false;
        }

        int currentValue = _getter.Invoke();
        if (_prevValue > currentValue)
        {

            var from = currentValue;
            decimal midValue = _prevValue * _power + currentValue * (1 - _power);
            _prevValue = (int) midValue;
            _setter.Invoke(_prevValue);
        }
        else if (_prevValue < currentValue)
        {
            _prevValue = currentValue;
            _setter.Invoke(currentValue);
        }

        return true;
    }
}