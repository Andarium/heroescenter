namespace HeroesCenter.Data;

public class PrimarySkillsImages : INotifyPropertyChanged
{
    private bool _useNewImages;

    public bool UseNewImages
    {
        get => _useNewImages;
        set
        {
            if (value == _useNewImages) return;
            _useNewImages = value;
            OnPropertyChanged();
            OnPropertyChanged(nameof(Attack));
            OnPropertyChanged(nameof(SpellPower));
            OnPropertyChanged(nameof(Knowledge));
            OnPropertyChanged(nameof(ManaPoints));
        }
    }

    public BitmapSource Attack => ImageHelper.GetPrimarySkillImage(0, UseNewImages);
    public BitmapSource Defence => ImageHelper.GetPrimarySkillImage(1);
    public BitmapSource SpellPower => ImageHelper.GetPrimarySkillImage(2, UseNewImages);
    public BitmapSource Knowledge => ImageHelper.GetPrimarySkillImage(3, UseNewImages);
    public BitmapSource Experience => ImageHelper.GetPrimarySkillImage(4);
    public BitmapSource ManaPoints => ImageHelper.GetPrimarySkillImage(5, UseNewImages);
    public BitmapSource MovementPoints => ImageHelper.GetPrimarySkillImage(6);

    public event PropertyChangedEventHandler? PropertyChanged;

    [NotifyPropertyChangedInvocator]
    protected virtual void OnPropertyChanged([CallerMemberName] string? propertyName = null)
    {
        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }
}