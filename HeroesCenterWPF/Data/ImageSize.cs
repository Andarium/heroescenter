namespace HeroesCenter.Data;

public enum ImageSize
{
    Big,
    Small
}