namespace HeroesCenter.Data;

public partial class MemoryWorker
{
    private const int PointerSize = 4;

    public byte[] GetDataDirect(int dataLength, IntPtr address)
    {
        var buffer = new byte[dataLength];
        // Читаем необходимое содержимое
        NativeMethods.ReadProcessMemory(_processHandle, address, buffer, new IntPtr(dataLength), out _);
        return buffer;
    }

    /// <summary>
    ///     Получает данные из процесса в виде массива байт
    /// </summary>
    /// <param name="dataLength">Размер данных, которые нужно получить, в байтах</param>
    /// <param name="addressOffset">Смещение в блоке данных игры (внутри блока героев, например)</param>
    public byte[] GetData(int dataLength, int addressOffset)
    {
        IntPtr address = IntPtr.Add(DataPtr, addressOffset);
        return GetDataDirect(dataLength, address);
    }

    public byte[] GetData(int size, int offset, int index)
    {
        return GetData(size, offset + BlockSize * index);
    }

    public byte[] GetData(MemoryVariable variable, int index = 0)
    {
        // Normal variable
        if (!variable.IsPointer)
        {
            return GetData(variable.Size, variable.Offset + BlockSize * index);
        }

        // Pointer variable
        byte[] result = GetData(PointerSize, variable.Offset + BlockSize * index);

        var offsets = variable.PointerOffsets;
        int address = BitConverter.ToInt32(result) + offsets[0];
        for (var i = 1; i < offsets.Count; i++)
        {
            result = GetDataDirect(PointerSize, new IntPtr(address));
            address = BitConverter.ToInt32(result) + offsets[i];
        }

        return GetDataDirect(variable.Size, new IntPtr(address));
    }
    private void SetDataDirect(byte[] input, IntPtr address)
    {
        NativeMethods.WriteProcessMemory(_processHandle, address, input, new IntPtr(input.Length), out _);
    }

    /// <summary>
    ///     Задает указанные данные в процессе по смещению
    /// </summary>
    /// <param name="input">Данные, которые необходимо задать</param>
    /// <param name="addressOffset">Смещение в блоке данных игры</param>
    /// <param name="index">индекс блока, если есть</param>
    public void SetData(byte[] input, int addressOffset, int index = 0)
    {
        addressOffset += BlockSize * index;
        IntPtr address = IntPtr.Add(DataPtr, addressOffset); //Добавили смещения для нужных данных
        SetDataDirect(input, address);
    }

    public void SetData<T>(T input, int addressOffset, int index = 0) where T : struct
    {
        SetData(input.ToByteArray(), addressOffset + BlockSize * index);
    }

    public void SetData<T>(T input, MemoryVariable variable, int index = 0) where T : struct
    {
        SetData(input.ToByteArray(), variable, index);
    }

    public void SetData(string input, MemoryVariable variable, int index = 0)
    {
        SetData(input.ToByteArray(), variable, index);
    }

    public void SetData(byte[] input, MemoryVariable variable, int index = 0)
    {
        // Normal variable
        if (!variable.IsPointer)
        {
            SetData(input, variable.Offset + BlockSize * index);
            return;
        }

        // Pointer variable
        byte[] result = GetData(PointerSize, variable.Offset + BlockSize * index);

        var offsets = variable.PointerOffsets;
        int address = BitConverter.ToInt32(result) + offsets[0];
        for (var i = 1; i < offsets.Count; i++)
        {
            result = GetDataDirect(PointerSize, new IntPtr(address));
            address = BitConverter.ToInt32(result) + offsets[i];
        }

        SetDataDirect(input, new IntPtr(address));
    }
}