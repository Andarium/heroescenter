namespace HeroesCenter.Data;

public class CachedImages
{
    private static CachedImages? _instance;
    private readonly IDictionary<string, BitmapSource> _images;

    private CachedImages()
    {
        _images = new ConcurrentDictionary<string, BitmapSource>();
    }

    public static CachedImages Instance => _instance ??= new CachedImages();

    public static BitmapSource? GetImageOrNull(string path)
    {
        if (Instance._images.TryGetValue(path, out BitmapSource? result))
        {
            return result;
        }

        var img = new BitmapImage();

        Stream? stream = path.GetStreamOrNull();

        if (stream == null)
        {
            return null;
        }

        img.BeginInit();
        img.CacheOption = BitmapCacheOption.OnLoad;
        img.CreateOptions = BitmapCreateOptions.PreservePixelFormat;
        img.StreamSource = stream;
        img.EndInit();

        return Instance._images[path] = img;
    }
}