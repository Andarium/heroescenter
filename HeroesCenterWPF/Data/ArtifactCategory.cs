namespace HeroesCenter.Data;

[Flags]
public enum ArtifactCategory
{
    None        = 0,
    Head        = 1 << 0,
    Shoulders   = 1 << 1,
    Neck        = 1 << 2,
    RightArm    = 1 << 3,
    LeftArm     = 1 << 4,
    Body        = 1 << 5,
    Rings       = 1 << 6,
    Legs        = 1 << 7,
    Slots       = 1 << 8,
    Ballista    = 1 << 9,
    Cart        = 1 << 10,
    Tent        = 1 << 11,
    Catapult    = 1 << 12,
    MagicBook   = 1 << 13,
    Backpack    = 1 << 14,
    Scroll      = 1 << 15,
    Other       = Backpack | Cart | Tent | Ballista | Catapult | MagicBook
}