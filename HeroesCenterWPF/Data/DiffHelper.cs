namespace HeroesCenter.Data;

public sealed class DiffHelper
{
    private static byte[]? _snapshot;

    public static void RememberHero()
    {
        if (GameViewModel.Instance is { CurrentHero: { } hero } game)
        {
            var worker = MemoryWorker.GetInstance(game.Version, MemoryWorkerType.Hero);
            _snapshot = worker.GetData(worker.BlockSize, 0, hero.Index);
        }
    }

    public static void DiffHero()
    {
        if (_snapshot == null)
        {
            return;
        }

        if (GameViewModel.Instance is { CurrentHero: { } hero } game)
        {
            var worker = MemoryWorker.GetInstance(game.Version, MemoryWorkerType.Hero);
            var newSnapshot = worker.GetData(worker.BlockSize, 0, hero.Index);
            D(_snapshot, newSnapshot, worker.BlockSize);
        }
    }

    private static void D(byte[] oldData, byte[] newData, int size)
    {
        List<Tuple<string, int>> F(KeyValuePair<string, MemoryVariable> pair)
        {
            var range = Enumerable.Range(pair.Value.Offset, pair.Value.Size);

            return range.Select
            (
                (x, index) => new Tuple<string, int>
                (
                    $"{pair.Key}[{index}]", x
                )
            ).ToList();
        }

        var all = HeroStats.AllVariablesWithNames
            .SelectMany(F)
            // .OrderBy(x => x.Item2)
            .ToList();

        StringBuilder sb = new();

        for (var i = 0; i < size; i++)
        {
            var oldValue = oldData[i];
            var newValue = newData[i];
            if (oldValue != newValue /*&& all.All(x => x.Item2 != i)*/)
            {
                var z = all.FirstOrDefault(x=>x.Item2 == i);
                sb.Append($"{i}: {oldValue} => {newValue}");
                if (z != null)
                {
                    sb.AppendLine($" // {z.Item1}");
                }
                else
                {
                    sb.AppendLine();
                }
            }
        }

        File.WriteAllText("E:\\z.txt", sb.ToString());
        SimpleDialog.Show(null, sb.ToString());
    }
}