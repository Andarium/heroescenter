namespace HeroesCenter.Data;

public class SkillsInfo
{
    protected internal SkillsInfo()
    {
        XmlHelper.LoadSkills(out Primary, out Secondary, out Stages);
    }

    [JsonProperty("Primary")]
    public readonly IReadOnlyList<string> Primary;

    [JsonProperty("Secondary")]
    public readonly IReadOnlyList<string> Secondary;

    [JsonProperty("Stages")]
    public readonly IReadOnlyList<string> Stages;

    [JsonIgnore] public string Attack => Primary[0];
    [JsonIgnore] public string Defence => Primary[1];
    [JsonIgnore] public string SpellPower => Primary[2];
    [JsonIgnore] public string Knowledge => Primary[3];
    [JsonIgnore] public string Experience => Primary[4];
    [JsonIgnore] public string ManaPoints => Primary[5];
    [JsonIgnore] public string MovementPoints => Primary[6];
    [JsonIgnore] public string Specialization => Primary[8];
}