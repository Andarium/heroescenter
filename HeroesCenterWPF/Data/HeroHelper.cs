namespace HeroesCenter.Data;

public class HeroHelper
{
    private static HeroHelper? _instance;

    private HeroHelper()
    {
        Images = new PrimarySkillsImages();
        Skills = new SkillsInfo();
        Classes = XmlHelper.LoadClasses();
    }

    public static HeroHelper Instance => _instance ??= new HeroHelper();
    public IReadOnlyList<string> Classes { get; }
    public SkillsInfo Skills { get; }
    public PrimarySkillsImages Images { get; }

    public bool UseNewImages
    {
        get => Images.UseNewImages;
        set => Images.UseNewImages = value;
    }
}