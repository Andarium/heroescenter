namespace HeroesCenter.Data;

public enum MemoryWorkerType
{
    Game,
    Player,
    Hero,
    Town,
    Creatures,
    UI,
    Map,
    Battle
}