namespace HeroesCenter.Data;

public static class ValueActionFactory
{
    public static ValueAction CreateMovementFreezeAction(
        GameViewModel gameViewModel,
        int value,
        double interval = Freezer.TimerInterval
    )
    {
        HeroViewModel? hero = gameViewModel.CurrentHero;
        interval = Math.Max(interval, Freezer.TimerInterval);

        var action = new ValueAction(ValueActionNameFactory.CreateMovementFreezeActionName(gameViewModel.CurrentHeroIndex),
            interval, () =>
            {
                if (hero == null || !MemoryWorker.IsGameRunning())
                {
                    return false;
                }

                hero.PrimarySkills.MovementPoints = value;
                return true;
            });
        return action;
    }

    public static ValueAction CreateMovementDamperAction(
        string actionName,
        GameViewModel gameViewModel,
        decimal power,
        double interval = Freezer.TimerInterval
    )
    {
        return CreateMovementDamperAction(actionName, gameViewModel, gameViewModel.CurrentHeroIndex, power, interval);
    }

    public static ValueAction CreateMovementDamperAction(
        string actionName,
        GameViewModel gameViewModel,
        int heroIndex, decimal power,
        double interval = Freezer.TimerInterval
    )
    {
        HeroViewModel? hero = gameViewModel.HeroManager.GetOrDefault(heroIndex);

        bool IsAvailable()
        {
            return hero != null && !MemoryWorker.IsGameRunning();
        }

        int Getter()
        {
            return hero!.PrimarySkills.MovementPoints;
        }

        void Setter(int value)
        {
            hero!.PrimarySkills.MovementPoints = value;
        }

        return new DamperAction(interval, actionName, power, Getter, Setter, IsAvailable);
    }

    public static ValueAction CreateDamperAction(
        Func<int> getter,
        Action<int> setter,
        int threshold,
        string? actionName = null,
        double interval = Freezer.TimerInterval
    )
    {
        var prevValue = 0;
        interval = Math.Max(interval, Freezer.TimerInterval);

        var action = new ValueAction(actionName ?? ValueActionNameFactory.CreateDamperActionName(),
            interval, () =>
            {
                int tempValue = getter();
                if (prevValue > tempValue + threshold)
                {
                    setter(prevValue);
                }
                else
                {
                    prevValue = tempValue;
                }

                return true;
            });
        return action;
    }
}