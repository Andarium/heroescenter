namespace HeroesCenter.Data;

public class SpecializationData
{
    public readonly int Index;
    public readonly int Key;
    public readonly string Name;

    public SpecializationData(int index, int key, string name)
    {
        Index = index;
        Key = key;
        Name = name;
    }
}