namespace HeroesCenter.Data;

[Flags]
public enum MagicSchool
{
    Air = 1 << 0,
    Earth = 1 << 1,
    Fire = 1 << 2,
    Water = 1 << 3,
    All = Air | Earth | Fire | Water
}