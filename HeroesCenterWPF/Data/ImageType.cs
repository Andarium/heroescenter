namespace HeroesCenter.Data;

public enum ImageType
{
    PortraitBig,
    PortraitSmall,
    Unit,
    UnitTownBack,
    PrimarySkill,
    SecondarySkill,
    Specialization,
    Artifact,
    Flag,
    Spell
}