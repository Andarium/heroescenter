namespace HeroesCenter.Data;

public class MemoryVariable
{
    public readonly bool IsPointer;
    public readonly int Offset;
    public readonly int Size;

    private readonly ReadOnlyCollection<int> _pointerOffsets;

    public IReadOnlyList<int> PointerOffsets => _pointerOffsets;

    public MemoryVariable(int size, int offset, params int[] pointerOffsets)
    {
        Offset = offset;
        Size = size;
        _pointerOffsets = new ReadOnlyCollection<int>(pointerOffsets);
        IsPointer = _pointerOffsets.Count != 0;
    }

    public MemoryVariable CopyPointer(int deltaFinalOffset = 0, int? overrideSize = null)
    {
        int size = overrideSize ?? Size;

        if (!IsPointer)
        {
            return new MemoryVariable(size, Offset + deltaFinalOffset);
        }

        var offsets = _pointerOffsets.ToArray();
        offsets[^1] += deltaFinalOffset;
        return new MemoryVariable(size, Offset, offsets);
    }
}