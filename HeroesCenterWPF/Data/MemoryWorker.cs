using System.Diagnostics.CodeAnalysis;

namespace HeroesCenter.Data;

public partial class MemoryWorker
{
    protected const int DesiredAccess = 0x001F0FFF; //Константа желаемого доступа в процессе

    private static readonly ConcurrentDictionary<GameVersion, MemoryWorkerType, MemoryWorker> Instances = new();
    private static readonly int ProgramProcessId = Process.GetCurrentProcess().Id;

    private IntPtr _processHandle;

    protected readonly byte[] TempBuffer = new byte[4];

    protected MemoryWorker(MemoryWorkerSettings settings)
    {
        _settings = settings;
        Recalculate();
    }

    public static bool IsGameRunning()
    {
        if (_currentProcess == null)
        {
            return false;
        }

        _currentProcess.Refresh();
        return !_currentProcess.HasExited;
    }

    public static bool IsGameRunning(out bool isGameForeground, out bool isProgramForeground)
    {
        isGameForeground = false;
        isProgramForeground = false;
        if (_currentProcess == null)
        {
            return false;
        }

        _currentProcess.Refresh();
        var foregroundProcessId = ProcessHelper.GetForegroundProcessId();
        isGameForeground = _currentProcess.Id == foregroundProcessId;
        isProgramForeground = ProgramProcessId == foregroundProcessId;
        return !_currentProcess.HasExited;
    }

    public static string? ProcessName => _currentProcess?.ProcessName;
    public static Process? Process => _currentProcess;
    public static int ProcessId => _currentProcess?.Id ?? 0;
    private static Process? _currentProcess;
    private static GameVersion? _currentGameVersion = null;
    private static Action? _onExit;
    private readonly MemoryWorkerSettings _settings;

    public int BlockSize => _settings.BlockSize;

    public IntPtr DataPtr { get; private set; }

    public static bool SetProcess(int processId, [NotNullWhen(true)] out GameVersion? version, Action? onExit = null)
    {
        if (processId != 0 && ProcessId != processId)
        {
            try
            {
                var newProcess = Process.GetProcessById(processId);
                UnsetProcess();
                _currentProcess = newProcess;
                if (!ProcessNamesHelper.TryGetVersion(_currentProcess.ProcessName, out version))
                {
                    throw new ArgumentException($"Can't recognize game version {_currentProcess.ProcessName}");
                }

                _currentProcess.EnableRaisingEvents = true;
                _currentProcess.Exited += OnProcessExit;
                _onExit = onExit;
                _currentGameVersion = version;
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        version = null;
        return false;
    }

    private static void OnProcessExit(object? sender, EventArgs e)
    {
        UnsetProcess();
        Application.Current.Dispatcher.Invoke(() => _onExit?.Invoke());
    }

    public static void UnsetProcess()
    {
        if (_currentProcess != null)
        {
            _currentProcess.Exited -= OnProcessExit;
        }

        _currentProcess = null;
        _currentGameVersion = null;
    }

    public static void RaiseRecalculate()
    {
        if (_currentGameVersion == null || _currentProcess == null)
        {
            return;
        }

        foreach (var worker in Instances[_currentGameVersion.Value].Values)
        {
            worker.Recalculate();
        }
    }

    protected void Recalculate()
    {
        NativeMethods.CloseHandle(_processHandle);

        if (_currentProcess == null)
        {
            return;
        }

        //Получаем handle на процесс с нужным доступом
        _processHandle = OpenProcess();

        IntPtr? moduleBaseAddress;

        string moduleName = _settings.ModuleName;
        if (string.IsNullOrWhiteSpace(moduleName) || moduleName.Equals("main", StringComparison.InvariantCultureIgnoreCase))
        {
            moduleBaseAddress = _currentProcess.MainModule?.BaseAddress;
            if (moduleBaseAddress == null)
            {
                throw new InvalidOperationException($"Main module for process {_currentProcess.ProcessName}:{_currentProcess.Id} is null");
            }
        }
        else
        {
            var module = NativeMethods.FindProcessModule(_processHandle, moduleName);
            moduleBaseAddress = module?.BaseAddress;

            if (moduleBaseAddress == null)
            {
                throw new InvalidOperationException($"Module {moduleName} for process {_currentProcess.ProcessName}:{_currentProcess.Id} not found");
            }
        }

        IntPtr resultPointer = IntPtr.Add(moduleBaseAddress.Value, _settings.ModuleOffset);

        foreach (int pointerOffset in _settings.Pointers)
        {
            NativeMethods.ReadProcessMemory(_processHandle, resultPointer, TempBuffer, new IntPtr(4), out _);
            resultPointer = new IntPtr(BitConverter.ToInt32(TempBuffer, 0));
            resultPointer = IntPtr.Add(resultPointer, pointerOffset);
        }

        DataPtr = resultPointer;
    }

    public static MemoryWorker GetInstance(GameVersion version, MemoryWorkerType type)
    {
        if (Instances.TryGetValue(version, type, out MemoryWorker? worker))
        {
            return worker;
        }

        worker = new MemoryWorker(Settings.Instance.MemoryWorkers[version, type]);
        return Instances[version, type] = worker;
    }

    protected static IntPtr OpenProcess()
    {
        return NativeMethods.OpenProcess(DesiredAccess, false, ProcessId);
    }

    public string GetBlockStartAddress(int index, int offset = 0)
    {
        return $"{DataPtr.ToInt64() + index * BlockSize + offset:X8}";
    }

    public string GetBlockEndAddress(int index, int offset = 0)
    {
        return $"{DataPtr.ToInt64() + (index + 1) * BlockSize - 1 + offset:X8}";
    }
}